<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/paciente_nuevo.css" type="text/css" rel="stylesheet" />
<link href="css/validacion_campos.css"type="text/css" rel="stylesheet" />
<title>ODONTOLOGO NUEVO</title>
<meta name="viewport" content="width=device-width, user-scalable=no , initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">

<script src="js/jquery.js"></script>
<style>
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
<!--GEOLOCALIZACION-->
<script>
function obtener()
{
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.watchPosition(mostrar_posicion,gestion_errores,parametros);
	
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;	
}
function gestion_errores(error)
{
	if(error.code==1)
	{
		alert("debes permitir el uso de la geolocalizacion en tu navegador");
	}
}
</script>
<!--AJAX-->
<script>
function ciudades()
{
	var DEPT=$('#departamento').val();
	$.ajax(
	{
		url:'../presentacion/consulta_ciudades.php',
		data:
		{
			DEPT: DEPT,
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#ciudad").attr('disabled','disabled');
		},
		success: function(data)
		{
			$('#ciudad').html(data);
			$("#ciudad").removeAttr('disabled');
		}
	})
}
$(document).ready(function()
{
	$('#departamento').change(function()
	{
		ciudades();
	});
	    
});
</script>
</head>
<?php
include('../datos/conex_copia.php');
if($id_usu!=''&&$id_usu!='0')
{
?>
<body onload="obtener()">
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			<center>
					<a href="http://aplicacionesarc.com/3M/"  class="btn_continuar">
						<img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
					</a>
				</center>
			</center>
			</span>
				
			<?php }else{?>
<table align="right">
    	<tr>
            <th>Bienvenid@  <?php echo $usua?></th>
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" ><span style="color:#000;">
        </a></td>
         <td><a href="../presentacion/consultar_encuesta_realizadas.php">
        <img src="../presentacion/imagenes/encuesta.png" width="56" height="57" id="cambiar" title="ENCUESTAS REALIZADAS" ><span style="color:#000;">
        </a></td>
        <td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" ><span style="color:#000;">
        </a></td>
         <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" ><span style="color:#000;">
        </a></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
<center><img src="../presentacion/imagenes/logo2.png" width="34%" height="17%" /></center>
<form id="tranferencias" name="tranferencias" action="../logica/registrar_paciente.php" method="post" style="width:100%;" enctype="multipart/form-data" accept-charset="utf-8">
	<div class="form-group" style="display:none">
        <input id="LAT" name="LAT" type="text" class="form-control" autocomplete="off"/>
        <input id="lon" name="lon" type="text" class="form-control" autocomplete="off"/>
    </div>
	<header>
        <div class="container" id="div_header">
        	<center><h1>ODONTOLOGO NUEVO NO EFECTIVO</h1></center>
        </div>
    </header>
      <input name="usuario" value="<?php echo $usua; ?>" type="hidden">
<input name="id_usuario" value="<?php echo $id_usu; ?>" type="hidden">
    
    <div class="form-group">
        <label for="cliente">Nombre</label><span class="asterisco">*</span>
        <br>
        <input name="nombre" id="nombre" class="form-control" type="text" placeholder="Nombre" required="required" />
    </div>
<div class="form-group">
        <label for="cliente">Apellido</label><span class="asterisco">*</span>
        <br>
        <input name="apellido" id="apellido" class="form-control" type="text" placeholder="Apellidos" required="required"/>
    </div>
    <div class="form-group">
        <label for="cliente">Direcci&oacute;n Residencial</label><span class="asterisco">*</span>
        <br>
        <input name="direccion" id="direccion" class="form-control" type="text" placeholder="Direccion" required="required"/>
    </div>
    <div class="form-group">
        <label for="cliente">Departamento</label><span class="asterisco">*</span>
        <br>
        <select name="departamento" id="departamento" class="form-control" required>
        	<option value="">ELIJA...</option>
        	<?php
			$consulta_departamento=mysqli_query($conex,"SELECT * FROM 3m_departamento WHERE NOMBRE_DEPARTAMENTO!='' ORDER BY NOMBRE_DEPARTAMENTO ASC");
			echo mysqli_error($conex);
			while($datos=mysqli_fetch_array($consulta_departamento))
			{
			?>
        		<option><?php echo $datos['NOMBRE_DEPARTAMENTO'] ?></option>
            <?php
			}
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">Ciudad</label><span class="asterisco">*</span>
        <br>
        <select name="ciudad" id="ciudad" class="form-control" required disabled="disabled">
        </select>
    </div>
   
    <div class="form-group">
            <center>
            	<input name="btn_cliente_no_efectivo" type="submit" id="btn_cliente_no_efectivo" value="GUARDAR" class="form-control btn" >
            </center>
    </div>
    <br />
</form>
<?php } ?>
</body>
</html>


<?php
}
?>