<?php
$ID_CLIENTE;
$id_usuario;
$DISTRIBUIDOR;
require_once('../datos/conex_copia.php');
header('Content-Type: text/html; charset=UTF-8');
mysqli_set_charset($conex,"utf8"); 
$fecha= date("Y-m-d");

$body1 = "";
$body2 = "";
$body3 = "";
$body4 = "";

$consulta_TOT=mysqli_query($conex,"SELECT SUM(dp.TOTAL_PEDIDO) AS 'TOTAL' 
FROM 3m_detalle_pedido AS dp
INNER JOIN 3m_pedido AS p ON p.ID_PEDIDO=dp.ID_PEDIDO_FK_2
WHERE dp.ID_PEDIDO_FK_2='$ID_PEDIDO'");
	
	$nreg=mysqli_num_rows($consulta_TOT);
	while($consulta_total=mysqli_fetch_array($consulta_TOT))
	{
		$total=$consulta_total['TOTAL'];
	}

$consulta_cliente = mysqli_query($conex,"SELECT cli.NOMBRE_CLIENTE, cli.APELLIDO_CLIENTE, cli.TIPO_IDENTIFICACION, cli.IDENTIFICACION_CLIENTE, cli.CELULAR_CLIENTE, cli.TELEFONO_CLIENTE, cli.DIRECCION_CLIENTE, cli.CIUDAD_CLIENTE,cli.EMAIL_CLIENTE, p.FORMA_PAGO, p.TIPO_PEDIDO, dp.RANGO_ENTREGA, dp.OBSERVACIONES, u.USER
 FROM 3m_detalle_pedido AS dp
INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=dp.ID_CLIENTE_FK_2
INNER JOIN 3m_categoria AS c ON dp.ID_CATEGORIA_FK_2=c.STOCK
INNER JOIN 3m_pedido AS p ON p.ID_PEDIDO=dp.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS u ON dp.ID_USUARIO_FK_2=u.ID_USUARIO
WHERE dp.ID_PEDIDO_FK_2='$ID_PEDIDO'");

echo mysqli_error($conex);
$nrego=mysqli_num_rows($consulta_cliente);

if($nrego > 0){
while ($fila1 = mysqli_fetch_array($consulta_cliente))
  { 
    	$CLIENTE      			= $fila1['NOMBRE_CLIENTE'].' '.$fila1['APELLIDO_CLIENTE'];
  		$TIPO_IDENTIFICACION	= $fila1['TIPO_IDENTIFICACION'].' '.$fila1['IDENTIFICACION_CLIENTE'];
		$CELULAR_CLIENTE 		= $fila1['CELULAR_CLIENTE'];
		$TELEFONO_CLIENTE 		= $fila1['TELEFONO_CLIENTE'];
		$DIRECCION_CLIENTE  	= $fila1['DIRECCION_CLIENTE'];
		$CIUDAD_CLIENTE 		= $fila1['CIUDAD_CLIENTE'];
		$FORMA_PAGO 			= $fila1['FORMA_PAGO'];
		$tipo_pedido        	= $fila1['TIPO_PEDIDO'];
		$RANGO_ENTREGA 			= $fila1['RANGO_ENTREGA'];
		$OBSERVACIONES			= $fila1['OBSERVACIONES'];
		$USER 		  			= $fila1['USER'];
        $CORREO_CLIENTE     	= $fila1["EMAIL_CLIENTE"];
		
  }
 
 
$body1 = "
<b>Distribuidor:</b> ".$DISTRIBUIDOR."

<h3>Datos Del Cliente</h3>
<table width='100%'>
  <tr>
  <th width='24%' style='text-align:left'>
  DOCUMENTO DE IDENTIFICACI&Oacute;N:
  </th>
        <td width='30%' >
       	".$TIPO_IDENTIFICACION."
        </td>
  <th width='14%' style='text-align:left'>
  NOMBRE CLIENTE:
  </th>
        <td width='32%' >
      	".$CLIENTE."
        </td>
  </tr>
     <tr>
     <th style='text-align:left'>
     TELEFONO CELULAR:
     </th>
        <td>
		".$CELULAR_CLIENTE."
        </td>
         <th style='text-align:left'>TELEFONO FIJO:</th>
        <td>
      	".$TELEFONO_CLIENTE."
        </td>
  </tr>
  <tr>
        <th style='text-align:left'>CORREO ELECTRONICO:</th>
        <td colspan='3'>".$CORREO_CLIENTE."</td>
</tr>
         <tr>
        <th style='text-align:left'>DIRECCION CLIENTE:</th>
 <td>
     	".$DIRECCION_CLIENTE."
		</td>
 
        <th style='text-align:left'>CIUDAD:</th>
        <td>
		".$CIUDAD_CLIENTE."
		</td>
        </tr>
        <tr>
           <th style='text-align:left'>FORMA DE PAGO:</th>
      <td>
      ".$FORMA_PAGO."
      </td>
      <th style='text-align:left'>TIPO PEDIDO:</th>
      <td>
      ".$tipo_pedido."
      </td>
    </tr>
    <tr>
           <th style='text-align:left'>HORARIO ENTREGA:</th>
      <td>
      ".$RANGO_ENTREGA."
      </td>
      <th style='text-align:left'>USUARIO:</th>
      <td>
      ".$USER."
      </td>
    </tr>
    <tr>
      <th colspan='1' style='text-align:left'>OBSERVACIONES:</th>
      <td colspan='3'>
      ".$OBSERVACIONES."
	  </td>
	  </tr>
   </table><br /><br />";
    
	
	
$body2 = "<h3>Detalle Del Pedido</h3>
<table style='width:99%;border:1px solid #000; margin:auto auto;' rules='all'>
	<tr>
		<th>NOMBRE PRODUCTO</th>
		<th>CANTIDAD</th>
		<th>VALOR</th>

	</tr>";
	
}


	$consulta_pedi=mysqli_query($conex,"SELECT C.ID_CATEGORIA, B.ID_CATEGORIA_FK_2, B.TOTAL_PEDIDO, B.CANTIDAD_PRODUCTO,C.DESCRIPCION
 FROM 3m_detalle_pedido AS B
INNER JOIN 3m_categoria AS C ON B.ID_CATEGORIA_FK_2=C.STOCK
INNER JOIN 3m_pedido AS p ON p.ID_PEDIDO=B.ID_PEDIDO_FK_2
WHERE B.ID_PEDIDO_FK_2='$ID_PEDIDO'");


if (empty($consulta_pedi)){
	echo "El pedido esta vacio";
	}else{				

				while($dato=mysqli_fetch_array($consulta_pedi))
				{
	
	$body3.= "	
					<tr>
					<td>".$dato['DESCRIPCION']."</td>
					<td>".$dato['CANTIDAD_PRODUCTO']."</td>
					<td>".$dato['TOTAL_PEDIDO']."</td>
		
					</tr>"; 
				
				}

	$body4 = "
	<tr>
			<th colspan='2'>TOTAL COMPRA:</th>
					<td colspan='4'>
						".$total."
					</td>
  </tr>
            
</table>";
        $body5="<br> <b>POR FAVOR CONFIRMAR CORREO RECIBIDO A:  programacion@peoplecontact.cc</b>";
	}




$BODY_TODO = $body1.$body2.$body3.$body4.$body5;
echo $BODY_TODO;


if($tipo_pedido=='Transferencia'){
	
	if($DISTRIBUIDOR=='Paola Linares-Dental 83 Portafolio 3M ESPE')
	{//1 por problemas en el correo se envia del dental se envia desde el correo de programacion@peoplecontact.cc alli se reenvia a jenny.ramirez@dental83.com,paola.linares@dental83.com
           
			$to= "jenny.ramirez@dental83.com".", ";
            $to.="paola.linares@dental83.com".", ";
			$to.= "solicitudpedidos3m@gmail.com";
	}
	else if($DISTRIBUIDOR=='Claudia Esperanza Beltran-Dentales Padilla')
	{//2
            $to = "ventas1@dentalespadilla.com".", ";
            $to.="ventasalmacen@dentalespadilla.com";
	}
	else if($DISTRIBUIDOR=='Yina Moncayo-Dental Palermo')
	{// 3
            $to = "contabilidadpalermo3@gmail.com".", ";
            $to = "solicitudpedidospalermo@gmail.com";
	}
	else if($DISTRIBUIDOR=='Gina Santamaria-Dental Nader')
	{//4
            $to = "comercial@dentalnader.com".", ";
            $to.="facturacion@dentalnader.com".", ";
            $to.="mercadeo@dentalnader.com".", ";
			$to.="gerencia@dentalnader.com";
	}
	else if($DISTRIBUIDOR=='Bracket Ltda-Andres Beltran')
	{//5
             $to = "gerencia@bracket.com.co".", ";
			 $to.="solicitudpedidos3mbrackets@gmail.com";
	}
	else if($DISTRIBUIDOR=='Dental Nader Villavicencio-Jhovanny Parra')
	{//6
             $to = "villavicencio@dentalnader.com".", ";
             $to.="facturacion@dentalnader.com".", ";
             $to.="mercadeo@dentalnader.com".", ";
			 $to.="gerencia@dentalnader.com";
	}
	else if($DISTRIBUIDOR=='Sandra Patricia Lopez-Casa Dental Sede Sur')
	{//7
            $to = "sandrap.lopez@casadentalltda.com".", ";
            $to.= "femafo16@gmail.com".", ";
            $to.= "patricia.velasquez@casadentalltda.com";
	 
	}
	else if($DISTRIBUIDOR=='Luz Elena Sanchez-Casa Dental Sede Norte')
	{//8
             $to = "d.ramirez@casadentalltda.com".", ";
             $to.= "n.argote@casadentalltda.com" .", ";
             $to.= "patricia.velasquez@casadentalltda.com".", ";
			 $to.= "luzelena.sanchez@casadentalltda.com".", ";
			 $to.= "auxventas2@casadentalsas.com";
	
	}
	else if($DISTRIBUIDOR=='Adriana Meneses-Casa Odontologica Importadora Y Comercializadora SAS')
	{//9
            $to = "asesoradrico@hotmail.com".", ";
			$to.="solicitudpedidos3mnorte@gmail.com";
	}
	else if($DISTRIBUIDOR=='Alex Basurto-Dental ORBIDENTAL')
	{//10
            $to = "orbidental@yahoo.com";
	}
	else if($DISTRIBUIDOR=='Marleny Pinto-Dental MEGADENTALES')
	{//11
            $to = "ventas2@megadentales.com";
	}
	else if($DISTRIBUIDOR=='Dental ALDENTAL')
	{//12
            $to = "dentaladmon@une.net.co".", ";
            $to.= "dental-compras@une.net.co".", ";
            $to.= "almadent@une.net.co".", ";
			$to.= "solicitudpedido3mmedellin@gmail.com";
	}
	else if($DISTRIBUIDOR=='Paula Andrea Restrepo-Dentales Maria T')
	{//13
            $to = "dentalesmariate@gmail.com";
	}
	else if($DISTRIBUIDOR=='Maryory Pareja-Dentales HV SAS')
	{//14
            $to = "dentaleshv@hotmail.com";
	}
	else if($DISTRIBUIDOR=='Juan Carlos Sosa-Dental DENTALES ANTIOQUIA')
	{//15
            $to = "info@dentalesantioquia.com.co";
	}
	else if($DISTRIBUIDOR=='Juan David Gonzales-Dental Market')
	{//16
             $to = "gerencia@dentalesmarket.com".", ";
             $to.="info@dentalesmarket.com".", ";
             $to.="dcomercial@dentalesmarket.com";
	 }
	else if($DISTRIBUIDOR=='Alicia Torres-Dental Alfor')
	{//18
             $to = "compras@dentalalfor.com";
	}
	else if($DISTRIBUIDOR=='Faiber Laverde-Dentales Y Dentales')
	{//19
            $to = "adm.dentalesydentales@gmail.com";
	}
	
	else if($DISTRIBUIDOR=='Angela Munioz-Dental ALDENTAL')
	{//20
            $to = "dental1ventas2@aldentalsa.com";
	}
	
	else if($DISTRIBUIDOR=='Jhovany Manrique-Dental MEGADENTALES')
	{//21
            $to = "ventas2@megadentales.com";
	}
	
	else if($DISTRIBUIDOR=='Carlos Henao-Dental Nader')
	{//22
            $to = "comercial@dentalnader.com".", ";
            $to.="facturacion@dentalnader.com".", ";
            $to.="mercadeo@dentalnader.com".", ";
			$to.="gerencia@dentalnader.com";
	}
	else if($DISTRIBUIDOR=='Faride Maestre-Alfa Representaciones dentales')
	{//23
             $to = "almacen@adental.com.co".", ";
			 $to.= "faride@adental.com.co";
	}
	else if($DISTRIBUIDOR=='Luz Karime Zanabria-Vertice S.A.S')
	{//24
            $to = "vertice.luzkarime@gmail.com".", ";
            $to.="vertice.milena@gmail.com";
	}
        else if($DISTRIBUIDOR=='Adriana Nino-Dental Nader Bucaramanga')
	{//26
            $to ="bucaramanga@dentalnader.com".", ";
			$to.="gerencia@dentalnader.com".", ";
			$to.="gerencia@dentalnader.com".", ";
			$to.="facturacion@dentalnader.com".", ";
			$to.="mercadeo@dentalnader.com";
	}
         else if($DISTRIBUIDOR=='Angie Hernandez-Asedis LTDA')
	{//27
            $to ="ventasasedis@hotmail.com";
	}
         else if($DISTRIBUIDOR=='Angela Jimenez-Home dental intl')
	{//28
            $to ="angela@homedentalintl.com";
	}
	 else if($DISTRIBUIDOR=='Omar Alberto Rueda-Drosan Lineas Especializadas')
	{//29
			$to ="solicitudpedidos3m2@gmail.com".", ";
            $to.="gerencia@drosanlineasespecializadas.com".", ";
            $to.="drosan.le@gmail.com";
	}
	 else if($DISTRIBUIDOR=='Ana Bueno-Elegir Soluciones')
	{//30
			$to ="asistente.gerencia@elegirsoluciones.com.co";
	}
	else if($DISTRIBUIDOR=='Marcela Cepeda-Dentales Boyaca')
	{//31
			$to ="dentalesboyacamarca@gmail.com";
			/*$to.="chelitacep@gmail.com".", ";
			$to.="solicitudpedidoboyaca3m@gmail.com";*/
	}
	else if($DISTRIBUIDOR=='Eddy Janeth Escobar-KBPO Venta Profesional')
	{//32
			$to ="eddyescobar@kbpo.co".", ";
			$to.="solicitudpedidos3m3@gmail.com";
	}
	else if($DISTRIBUIDOR=='Daniel Vega-Casa Dental')
	{//33
		$to = "d.vega@casadentalltda.com".", ";
		$to.="people.marketingdesarrollo@gmail.com";
		
	}
	else if($DISTRIBUIDOR=='Juan Diego Salazar-Dual Dental')
	{//34
		$to = "dualdental@yahoo.com";
	}
	else if($DISTRIBUIDOR=='Suministros Medicos Y Odontologicos SYM')
	{//35
		$to = "smsuministrossas@gmail.com";
	}
	else if($DISTRIBUIDOR=='Distribuciones y Suministros AYD')
	{//36
		$to = "tatatovar0304@yahoo.es";
	}
	else if($DISTRIBUIDOR=='Proclinica SAS')
	{//37
		$to = "solicitudpedido3mproclinica@gmail.com";
	}
	else if($DISTRIBUIDOR=='Macrodental del Oriente')
	{//38
		$to = "macrodentaldeloriente@gmail.com";
	}
	else if($DISTRIBUIDOR=='Dentales y Acrilicos')
	{//39
		$to = "proveedoresdya@gmail.com";
	}
	else if($DISTRIBUIDOR=='Mundo dental')
	{//40
		$to = "ventas@mundodental.co";
	}
	else if($DISTRIBUIDOR=='Dental Caldas')
	{//41
		$to = "solicitudpedido3mcaldas@gmail.com";
	}
	else if($DISTRIBUIDOR=='Mundo Dental de la Costa')
	{//42
		$to = "solicitudpedidos3mbarranquilla@gmail.com";
	}
	else if($DISTRIBUIDOR=='Proclinics')
	{//43
		$to = "proclinics.sas@gmail.com";
	}
	else if($DISTRIBUIDOR=='Salud Dental')
	{//44
		$to = "solicitudpedido3msantamarta@gmail.com";
	}
	else if($DISTRIBUIDOR=='Surtidental de la Costa')
	{//45
		$to = "solicitudpedido3mcartegena1@gmail.com";
	}
	else if($DISTRIBUIDOR=='Dentalmedico')
	{//46
		$to = "solicitudpedido3mcartagena2@gmail.com";
	}
	else if($DISTRIBUIDOR=='Comercializadora Biodental SAS')
	{//47
		$to = "solicitudpedido3mpasto@peoplecontact.cc".", ";
		$to.= "pedidospasto3m@gmail.com";
	}
	else if($DISTRIBUIDOR=='Medico Dentales')
	{//48
		$to = "medicodentales@gmail.com".", ";
		$to.= "pedidospasto3m@gmail.com";
	}
	else if($DISTRIBUIDOR=='Casa Dental Pasto')
	{//49
		$to = "solicitudpedidoscali@peoplecontact.cc".", ";
		$to.= "patricia.velasquez@casadentalltda.com";
	}
	else if($DISTRIBUIDOR=='Dental del Sur')
	{//50
		$to = "disdentaldelsur@gmail.com".", ";
		$to.= "pedidospasto3m@gmail.com";
	}
	else if($DISTRIBUIDOR=='Distribuciones hospitalarias Janer')
	{//51
		$to = "pedidospasto3m@gmail.com";
	}
	else if($DISTRIBUIDOR=='Dental 7 de agosto-Paola Dueri')
	{//52
		$to = "pedidos@dental7deagosto.com".", ";
		$to.="7deagosto@dental7deagosto.com";
	}
	else if($DISTRIBUIDOR=='Distribuidora Dental Pardo SAS - Laura Mina')
	{//53
		$to = "info@dentalrestrepo.com";
	}
	else if($DISTRIBUIDOR=='Jose Alejo Rodriguez Barragan - Dental Jar')
	{//54
		$to = "jardental20@gmail.com";//poner correo creado ya que es hotmail
	}
	else if($DISTRIBUIDOR=='Equidentales')
	{//55
		$to = "solicitudpedidos3marmenia@gmail.com";//poner correo creado ya que es hotmail
	}
	else if($DISTRIBUIDOR=='Juan Araque - Ultra estetica dental')
	{//56
		$to = "ultraesteticadental@gmail.com";
	}
	else if($DISTRIBUIDOR=='Edgar - Rossydent')
	{//57
		$to = "solicitudespedido3mbucaramanga@gmail.com";//poner correo creado ya que es hotmail
	}
	else if($DISTRIBUIDOR=='ODONTOSOLUCIONES-MARIA ISABEL VILLABONA')

	{//58
		$to = "solicitudespedido3mbarranca@gmail.com";//poner correo creado ya que es hotmail
	}
	else if($DISTRIBUIDOR=='DOTADENTAL-LUZ STELLA SANTOS CORDERO')
	{//59
		$to = "dotadentalsas@gmail.com";
	}
	else if($DISTRIBUIDOR=='DENTIORIENTE-ASTRID KATERINE MANRIQUE')
	{//60
		$to = "dentioriente@gmail.com";
	}
	else if($DISTRIBUIDOR=='TEETH-ANDRES TANGARIFE')
	{//61
		$to = "iocrovi27@gmail.com";
	}
	else if($DISTRIBUIDOR=='Emilse Amado - Dentales H&M')
	{//62
		$to = "emiamadoc@yahoo.es";
	}
		else if($DISTRIBUIDOR=='Lineas Medicas Odontologicas')
	{//63
		$to = "lineasmedicasodontologicas@hotmail.com";
	}
	else{
            $to = "programacion@peoplecontact.cc";
	}
	
	$subject = "SOLITUD DE PEDIDO ".$fecha;


        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <programacion@peoplecontact.cc>' . "\r\n";
        $headers .= 'Cc: programacion@peoplecontact.cc,seguimiento3m@peoplecontact.cc,coordinador3m@peoplecontact.cc' . "\r\n";

        mail($to,$subject,$BODY_TODO,$headers);
	
}

else if($tipo_pedido=='Sugerido')
{

        $to = "seguimiento3m@peoplecontact.cc";
	$subject = "PEDIDO SUGERIDO ".$fecha;


        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <programacion@peoplecontact.cc>' . "\r\n";
        $headers .= 'Cc: coordinador3m@peoplecontact.cc,programacion@peoplecontact.cc' . "\r\n";

        mail($to,$subject,$BODY_TODO,$headers);
	
}







?>