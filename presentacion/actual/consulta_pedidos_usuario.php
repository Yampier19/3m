<?php
	include("../logica/session.php");
    header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 


<title>CONSULTA PEDIDO</title>

<script>
$(document).ready(function() {
    $('#pedidos').DataTable();
} );
</script>

</script>
<script language=javascript> 
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}
</script>
<style>
.tit
{
	padding:5px;
	text-transform:uppercase;
	font-weight:bold;
	text-align:left;
}
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
</head>
<?PHP
include('../datos/conex.php');
if(isset($pedid))
{
	$ID_CLIENTE=base64_decode($pedid);
}
else
{
	$ID_CLIENTE=$ID_CLIENTE;
}
 echo mysql_error($conex);

?>
<body>
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			<center>
					<a href="http://aplicacionesarc.com/3M/"  class="btn_continuar">
						<img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
					</a>
				</center>
			</center>
			</span>
				
			<?php }else{?>
<div class="container-fluid">
<table align="right" style="width:100%;">
    	<tr> 	
          <th colspan="6">Bienvenid@  <?php echo $usua?></th> 
          </tr>
          <tr>
            <input name="usua" style="display:none;" type="text" id="usua" value="<?php echo $usua; ?>" readonly/>      
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" >
        </a></td>

        </a></td>
        <td><a href="../presentacion/kpi_visitador.php">
        <img src="../presentacion/imagenes/kpi.png" width="56" height="57" id="cambiar" title="KPI'S" >
         <center> <font style="font-size:10px;" >KPI'S</font></center>
        </a></td>


        <td><a href="../presentacion/consultar_gestiones_visitadores.php">
        <img src="../presentacion/imagenes/gestiones.png" width="56" height="57" id="cambiar" title="GESTIONES" >
         <center> <font style="font-size:10px;" >Gestiones</font></center>
        </a></td>
        
        <td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Pedidos</font></center>
        </a></td>
          <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PR&Oacute;XIMA VISITA" >
         <center> <font style="font-size:10px;" >Pr&oacute;xima <br />Visita</font></center>
        </a></td>
        <td><a href="../presentacion/consulta_productos_visita.php?">
        <img src="../presentacion/imagenes/productos.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Productos</font></center>
        </a></td>
         <td><a href="../presentacion/reporte_clientes_nuevos.php">
        <img src="../presentacion/imagenes/excel.png" width="48" height="51" id="cambiar" title="REPORTE EXCEL" ><br />
       <center> <font style="font-size:10px;" >Clientes <br />Nuevos</font></center>
        </a></td>
        </tr>
    </table>
    </div> 
	<br />
    <br />
  <div class="col-md-12">
<form id="cambio_contacto" name="cambio_contacto" method="post" action="#" enctype="multipart/form-data" class="letra">
<fieldset style="margin:auto auto; width:90%;">
<div class="col-md-12">
      <div class="col-md-5">
                     <label for="mes">ESTADO PEDIDO</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="ESTADO">
			<option value="">Seleccione</option>
            <option value="NUEVO">NUEVO</option>
            <option value="DEVUELTO">DEVUELTO</option>
            <option value="ENTREGADO">ENTREGADO</option>
            <option value="ENTREGA PARCIAL">ENTREGA PARCIAL</option>
            <option value="EN PROCESO">EN PROCESO</option>
            <option value="DESPACHADO">DESPACHADO</option>
                        </select>
                </div>

      <div class="col-md-5">
             <label for="cliente">FECHA GESTI&Oacute;N:</label><span class="asterisco">*</span><br />
                <input type="date" class="form-control" name="fecha" id="fecha" max="<?php echo date("Y-m-d");?>"/>
        </div>    
      <div class="col-md-2">
           <label for="cliente"></label><br /><br />
    <button title="Consultar" name="consulta"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>
      </div>
</div>
     
  </fieldset>
    
</form>
<div class="table table-responsive">
    <div class="col-md-6">
        <?php
        $mes = date("m");
        $year = date("Y");
         if($mes ==01){ $nombre_mes ='Enero';}
         else if($mes ==02){$nombre_mes ='Febrero';}
         else if($mes ==03){$nombre_mes ='Marzo';}
         else if($mes ==04){$nombre_mes ='Abril';}
         else if($mes ==05){$nombre_mes ='Mayo';}
         else if($mes ==06){$nombre_mes ='Junio';}
         else if($mes ==07){$nombre_mes ='Julio';}
         else if($mes ==08){$nombre_mes ='Agosto';}
         else if($mes ==09){$nombre_mes ='Septiembre';}
         else if($mes ==10){$nombre_mes ='Octubre';}
         else if($mes ==11){$nombre_mes ='Noviembre';}
         else if($mes ==12){$nombre_mes ='Diciembre';}

         
        
        
        $consulta_suma_pedidos = mysql_query("SELECT IF(SUM(TOTAL_PEDIDO)IS NULL,0,SUM(TOTAL_PEDIDO))  AS SUMA FROM 3m_pedido WHERE MONTH(FECHA_PEDIDO)='".$mes."' AND YEAR(FECHA_PEDIDO)='".$year."' AND TIPO_PEDIDO <> 'Sugerido' AND ESTADO_PEDIDO<>'RECHAZADO' AND ESTADO_PEDIDO<>'ELIMINADO' AND ESTADO_PEDIDO<>'DEVUELTO' AND ID_USUARIO_FK=".$id_usu,$conex);
          while($dato_pedidos=mysql_fetch_array($consulta_suma_pedidos))
        {
           $valor_pedidos = $dato_pedidos["SUMA"];
        }
         
         ?>
        <h4>Transferencias <b> <?php echo $nombre_mes; ?>:</b>
             <span class="label label-danger">
                 <?php if($valor_pedidos==0){ echo "$ ". $valor_pedidos; }else{   echo "$ ".number_format($valor_pedidos,0,',','.');} ?></span></h4>
  
        
    </div>

 <p>
   <?php 


if(isset($_POST["consulta"])){ 
	$fecha	    	=$_POST["fecha"];
	$estado  		= $_POST["ESTADO"];
		if(empty($fecha) && empty($estado)){
			?>
        <script>
		alert('seleccione una obcion de busqueda');
		</script>
            <?php
			$consulta=mysql_query("
 SELECT DISTINCT p.ID_PEDIDO, p.TOTAL_PEDIDO,cli.NOMBRE_CLIENTE,cli.APELLIDO_CLIENTE,p.DISTRIBUIR,p.FECHA_PEDIDO,
p.ESTADO_PEDIDO,cli.CELULAR_CLIENTE,cli.TELEFONO_CLIENTE,cli.DIRECCION_CLIENTE,p.TIPO_PEDIDO,u.USER,
d.RANGO_ENTREGA,d.OBSERVACIONES
FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_detalle_pedido AS d ON d.ID_PEDIDO_FK_2 = p.ID_PEDIDO
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO 
 WHERE  u.ID_USUARIO =".$id_usu." ORDER BY p.FECHA_PEDIDO DESC",$conex);
 
			}
			//BUSQUEDA ESTADO
		else if(empty($fecha) && empty($estado)==false) {
			$consulta =mysql_query("
 SELECT DISTINCT p.ID_PEDIDO, p.TOTAL_PEDIDO,cli.NOMBRE_CLIENTE,cli.APELLIDO_CLIENTE,p.DISTRIBUIR,p.FECHA_PEDIDO,
p.ESTADO_PEDIDO,cli.CELULAR_CLIENTE,cli.TELEFONO_CLIENTE,cli.DIRECCION_CLIENTE,p.TIPO_PEDIDO,u.USER,
d.RANGO_ENTREGA,d.OBSERVACIONES
FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_detalle_pedido AS d ON d.ID_PEDIDO_FK_2 = p.ID_PEDIDO
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO 
 WHERE  u.ID_USUARIO =".$id_usu." AND ESTADO_PEDIDO='$estado'
 ORDER BY p.FECHA_PEDIDO DESC",$conex);
			 
			
			}  
			//BUSQUEDA FECHA 
		 else if(empty($fecha)==false && empty($estado)) {
			$consulta =mysql_query("
 SELECT DISTINCT p.ID_PEDIDO, p.TOTAL_PEDIDO,cli.NOMBRE_CLIENTE,cli.APELLIDO_CLIENTE,p.DISTRIBUIR,p.FECHA_PEDIDO,
p.ESTADO_PEDIDO,cli.CELULAR_CLIENTE,cli.TELEFONO_CLIENTE,cli.DIRECCION_CLIENTE,p.TIPO_PEDIDO,u.USER,
d.RANGO_ENTREGA,d.OBSERVACIONES
FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_detalle_pedido AS d ON d.ID_PEDIDO_FK_2 = p.ID_PEDIDO
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO 
 WHERE  u.ID_USUARIO =".$id_usu." AND DATE(p.FECHA_PEDIDO)='$fecha'
 ORDER BY p.FECHA_PEDIDO DESC",$conex);
	
			}
			//BUSQUEDA ESTADO Y FECHA
			else if(empty($fecha)==false && empty($estado)==false ) {
			$consulta =mysql_query("
 SELECT DISTINCT p.ID_PEDIDO, p.TOTAL_PEDIDO,cli.NOMBRE_CLIENTE,cli.APELLIDO_CLIENTE,p.DISTRIBUIR,p.FECHA_PEDIDO,
p.ESTADO_PEDIDO,cli.CELULAR_CLIENTE,cli.TELEFONO_CLIENTE,cli.DIRECCION_CLIENTE,p.TIPO_PEDIDO,u.USER,
d.RANGO_ENTREGA,d.OBSERVACIONES
FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_detalle_pedido AS d ON d.ID_PEDIDO_FK_2 = p.ID_PEDIDO
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO 
 WHERE  u.ID_USUARIO =".$id_usu." AND ESTADO_PEDIDO='$estado' AND DATE(p.FECHA_PEDIDO)='$fecha'
 ORDER BY p.FECHA_PEDIDO DESC" ,$conex);
			}		
}





						}
			else{
	$consulta=mysql_query("
 SELECT DISTINCT p.ID_PEDIDO, p.TOTAL_PEDIDO,cli.NOMBRE_CLIENTE,cli.APELLIDO_CLIENTE,p.DISTRIBUIR,p.FECHA_PEDIDO,
p.ESTADO_PEDIDO,cli.CELULAR_CLIENTE,cli.TELEFONO_CLIENTE,cli.DIRECCION_CLIENTE,p.TIPO_PEDIDO,u.USER,
d.RANGO_ENTREGA,d.OBSERVACIONES
FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_detalle_pedido AS d ON d.ID_PEDIDO_FK_2 = p.ID_PEDIDO
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO 
 WHERE  u.ID_USUARIO =".$id_usu." ORDER BY p.FECHA_PEDIDO DESC",$conex);
			}
 ?>
<br />
 <table style="width:99%; margin:auto auto;" rules="none" >
   <tr>
		<th class="principal"><center>PEDIDO(S)</center></th>
	</tr>
 </table>
 <br /> 
   <div class="table table-responsive">   
<table id="pedidos" class="table table-hover">
	<thead>
	<tr >
		<th class="TITULO"><center>VALOR PEDIDO</center></th>
		<th class="TITULO"><center>CLIENTE</center></th>
        <th class="TITULO"><center>DISTRIBUIDOR</center></th>
        <th class="TITULO"><center>FECHA PEDIDO</center> </th>
        <th class="TITULO"><center>ESTADO PEDIDO</center> </th>
        <th class="TITULO"><center>TIPO PEDIDO</center></th>
        <th class="TITULO"><center>VER</center></th>
	</tr>
    </thead>
    <tbody>
    <?PHP
    while($dato=mysql_fetch_array($consulta))
	{
	?>
		<tr class="datos">
		<td>$ <?php echo number_format($dato["TOTAL_PEDIDO"],0,',','.');?></td>
            <td><?php echo $dato["NOMBRE_CLIENTE"]?> <?php echo $dato["APELLIDO_CLIENTE"]?></td>
            <td><?php echo $dato["DISTRIBUIR"]?></td>
            <td><?php echo $dato["FECHA_PEDIDO"]?></td>
            <td><?php echo $dato["ESTADO_PEDIDO"]?></td>            
            <td><?php echo $dato["TIPO_PEDIDO"]?></td>
            <th>
                <a  href="javascript:ventanaSecundaria('../presentacion/consulta_pedidos_detalle_visita.php?x=<?php echo base64_encode($dato['ID_PEDIDO'])?>')" >
              <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="DETALLE PEDIDO"/>
                </a>                
            </th>	
		</tr>
	<?php 
	}
	?>
    </tbody>
</table>
</div>
</div>
<br />
<br />


</center>
<?php } ?>
</body>
</html>