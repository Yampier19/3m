<?php
	include("../logica/session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="../presentacion/css/menu_visitas.css" type="text/css" rel="stylesheet" />
<link href="css/menu_visitas.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="../presentacion/fonts.css" />
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
<style>
input
{
	line-height:2%;
	width:90.5%;
}
select
{
	line-height:15%;
	width:99%;
}
.form-control[disabled],.form-control[readonly],fieldset[disabled] .form-control
{
	background-color:#eee;
	opacity:1;
	cursor:not-allowed;
}
.form-control
{
	display:block;
	padding:6px 12px;
	line-height:1.42857143;
	color:#555;
	background-color:#fff;
	background-image:none;
	border:1px solid #ccc;
	border-radius:4px;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	font-size:90%;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
.btn:focus,
.btn.focus {
  color: #fff;
  background-color: #286090;
  border-color: #122b40;
}
.btn:hover {
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active,
.btn.active,
.open > .dropdown-toggle.btn{
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active:hover,
.btn.active:hover,
.open > .dropdown-toggle.btn:hover,
.btn:active:focus,
.btn.active:focus,
.open > .dropdown-toggle.btn:focus,
.btn:active.focus,
.btn.active.focus,
.open > .dropdown-toggle.btn.focus {
  color: #fff;
  background-color: #204d74;
  border-color: #122b40;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
</style>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<script src="../presentacion/js/jquery.js"></script>
</head>

<script>
function nombre_cliente()
{
	var cliente=$('#nombre').val();
	if(cliente!='')
	{
		$.ajax(
		{
			url:'../presentacion/consulta_datos_nombre.php',
			data:
			{
				cliente: cliente
			},
			type: 'post',
			beforeSend: function () 
			{
				$("#cliente_name").attr('disabled', 'disabled');
			},
			success: function(data)
			{
				$('#cliente_name').html(data);
				var i = $('#cliente_name > option').length;
				$('#valor').val(i);
				if(i>1)
				{
					
					$("#cliente_name").removeAttr('disabled');
					$("#link1").css('display','none');
				}
				if(i<=1)
				{
					$("#div_menu").css('display','block');
					$("#cliente_name").attr('disabled', 'disabled');
					$("#link1").css('display','block');
					$("#link1").css('border-bottom-color','#911108');
					$("#link2").css('display','none');
					$("#link2").css('border-bottom-color','#911108');
					$("#link3").css('display','none');
					$("#link3").css('border-bottom-color','#911108');
				}
			}
		})
	}
	else
	{
		$("#div_menu").css('display','none');
		$("#cliente_name").attr('disabled', 'disabled');
		$("#cliente_name").html('');
		$('#cliente_name').attr('size',$('#cliente_name option').length);
	}
}
function datos()
{
	var cliente=$('#cliente_name').val();
	if(cliente!='')
	{
		$.ajax(
		{
			url:'../presentacion/consulta_id_odontologo.php',
			data:
			{
				cliente: cliente
			},
			type: 'post',
			beforeSend: function () 
			{
				$('#resultado_div').css('display','none');
			},
			success: function(data)
			{
				$('#resultado_div').css('display','block');
				$('#id_cliente').val(data);	
			}
		})
	}
	else
	{
		$("#id_cliente").html('');
		$('#cliente_name').attr('size',1);
	}
}
function ingresar_paso()
{
	var latitud=$('#LAT').val();
	var longitud=$('#lon').val();
	$.ajax(
	{
		url:'../presentacion/ingresar_paso.php',
		data:
		{
			latitud: latitud,
			longitud:longitud
		},
		type: 'post',
		success: function(data)
		{
			alert("Bienvenido(a) a 3M")
		}
	})
}

/*function ingresar_gestion()
{
	var latitud=$('#LAT').val();
	var longitud=$('#lon').val();
	var observacion=$('#observacion').val();
	var id_cliente=$('#id_cliente').val();
	$.ajax(
	{
		url:'../presentacion/ingresar_gestion.php',
		data:
		{
			latitud: latitud,
			longitud:longitud,
			observacion:observacion,
			id_cliente:id_cliente
		},
		type: 'post',
		success: function(data)
		{
			$('#visita_no_efectiva').html(data);
			$('#resultado_div').css('display','none');
			$("#resultado option:eq(0)").attr("selected", "selected");
		}
	})
}*/

function consultar_distribuidor()
{
	var id_cliente=$('#id_cliente').val();
	if(id_cliente!='')
	{
		$.ajax(
		{
			url:'../presentacion/consulta_distribuidor_odontologo.php',
			data:
			{
				id_cliente: id_cliente
			},
			type: 'post',
			beforeSend: function () 
			{
				$("#link3").css('display','none');
			},
			success: function(data)
			{
				$('#distribuidor').val(data);
				var distribuidor=$('#distribuidor').val();
				if(distribuidor!='')
				{
					$("#link3").css('display','block');
				}
				if(distribuidor=='')
				{
					$("#link3").css('display','none');
				}
			}
		})
	}
	else
	{
		$("#distribuidor").html('');
		$('#cliente_name').attr('size',1);
	}
}
var height= window.innerHeight-10;/*tamaño ventana*/
var ancho=window.innerWidth;
//alert(ancho);
/*alert(height);*/
$(document).ready(function()
{
	
	$('#nombre').on('keyup', function()
	{
		nombre_cliente();
        $('#cliente_name').attr('size',$('#cliente_name option').length);
	})
	$('#GUARDAR').click(function()
	{
		resulta=$('#resultado').val();
		if(resulta=='NO EFECTIVA')
		{
			observacion=$('#observacion').val();
			if(observacion=='')
			{
				alert('La observacion esta vacia');
				$('#observacion').focus();
				return false;
			}
			if(observacion!='')
			{
				ingresar_gestion();
			}
		}
	});
	$('#link1').click(function()
	{
		$('#nombre').val('');
		$("#nombre").attr('disabled', 'nombre');
	});
	$('.bt-menu').click(function()
	{
		if(dato!='')
		{
			
			$("#div_menu").css('display','block');
			$("#link2").css('display','block');
			if(distribuidor!='')
			{
				$("#link3").css('display','block');
			}
		}
		if(dato=='')
		{
			$("#link1").css('display','none');
			$("#link2").css('display','none');
			$("#link3").css('display','none');
		}
	});
	
	$('#cambiar').click(function()
	{
		alert('cambio');
		$("#resultado_text option:eq(0)").attr("selected", "selected");
		alert('cambio2');
		$('#nombre').val('');
		$('#id_cliente').val('');
		$('#distribuidor').val('');
		$("#nombre").removeAttr('disabled');
		$("#div_menu").css('display','none');
		$("#div_menu").animate({marginLeft:'0'},500);
		$('#info').fadeOut(1000);
		//$('#resultado_div').css('display','none');
	});
	$('#cargar').click(function()
	{
		$("#div_menu").animate({marginLeft:'0'},500);
		$('#info').fadeOut(1000);
		$("#div_menu").css('display','block');
		/*consultar_distribuidor();
		var distribuidor=$('#distribuidor').val();
		if(distribuidor!='')
		{
			$("#link3").css('display','block');
			$("#link2").css('border-bottom-color','#FFFDFD');
		}
		if(distribuidor=='')
		{
			$("#link2").css('border-bottom-color','#911108');
			$("#link3").css('display','none');
		}*/
	});
	$('#cliente_name').change(function()
	{
		datos();
		consultar_distribuidor();
		var dato=$('#cliente_name').val();
		var distribuidor=$('#distribuidor').val();
		$('#nombre').val(dato);
		$("#nombre").attr('disabled', 'nombre');
		$('#cliente_name').html('');
		$('#cliente_name').attr('size',$('#cliente_name option').length);
		$("#cliente_name").attr('disabled', 'disabled');
		if(dato!='')
		{
			
			$("#div_menu").css('display','block');
			$("#link2").css('display','block');
			if(distribuidor!='')
			{
				$("#link3").css('display','block');
			}
		}
		if(dato=='')
		{
			$("#link1").css('display','none');
			$("#link2").css('display','none');
			$("#link3").css('display','none');
		}
	});	
	if(ancho<1001)
	{
		$('#info').css('height',height);
		var tama_menu=$("#div_menu").height();
		var div_buscar=$("#buscar").height();
		tama_menu=tama_menu*(-1);
		//alert(tama_menu);
		$('#info').css('margin-bottom',tama_menu);
		$("#info").animate({marginTop:tama_menu},1);
		//alert(tama_menu);
		
		$('#div_menu li').click(function()
		{
			$("#div_menu").animate({marginLeft:'-120%'},500);
			$('#info').toggle(1000);
			/*$('#div_menu').animate({top: '-100%',left: '-100%'});
			$(".div_menu").css('margin-left','-120%');*/
			/*$(".div_menu").an*/
		});
		$('#menu_bar').click(function()
		{
			/*$("#div_menu").css('display','block');
			$("#div_menu").animate({marginLeft:'0'},500);
			$('#info').fadeOut(100);*/
			
			$("#div_menu").animate({marginLeft:'0'},500);
			$('#info').fadeOut(1000);
			$("#div_menu").css('display','block');
		});
	}
	else
	{
		$('#info').css('height',height);
		$('#info').css('display','block');
	}
});
function obtener()
{
	/*maximumAge: pedir que se cambia a cierto tiempo
	  timeaout:10000, maximumAge:600000
	  
	*/
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.watchPosition(mostrar_posicion,gestion_errores,parametros);
	
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	/*alert(exactitud);*/
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;
	ingresar_paso()
	
}
function gestion_errores(error)
{
	//alert("ha ocurrido un error "+error.code +" "+error.massage);
	if(error.code==1)
	{
		alert("debes permitir el uso de la geolocalizacion en tu navegador");
	}
}
</script>


<?php
if($id_usu!='')
{
?>
<body onload="obtener()">
<center><img src="../presentacion/imagenes/3m.png" width="24%" height="17%" /></center>
	<div class="form-group" style="display:none">
        <input id="LAT" name="LAT" type="text" class="form-control" autocomplete="off"/>
        <input id="lon" name="lon" type="text" class="form-control" autocomplete="off"/>
    </div>
    <br />
    <br />

	<div class="menu_bar" id="menu_bar">
        <a class="bt-menu" style="text-align:left; margin-left:15px;" href="../presentacion/blanco.php" target="info"><span class="icon-list2"></span>    Menu</a>
        <!--<div id="salir" style="float:left; margin-left:3%; margin-top:2%;">
        	<span class="icon-exit" title="CERRAR SESION" style="color:#FFF;"></span>
        </div>
        <div id="salir" style="float:left; margin-top:4.5%;">
        	<strong style="font-size:100%; margin-top:50%; color:#FFF;">SALIR</strong>
        </div>-->
    </div>
    <br />
    <div id="buscar">
        <label for="cliente">Nombre Odontologo</label><span class="asterisco">*</span>
            <div class="form-group">
                <img src="../presentacion/imagenes/lapiz_1.png" width="35" height="34" id="cambiar" align="right" style="margin-right:10px"/>
                <input placeholder="NOMBRE ODONTOLOGO" type="text" name="nombre" id="nombre" class="form-control" autocomplete="off" autofocus="autofocus"/>
            </div>
            <br />
    <select name="cliente_name" id="cliente_name" class="form-control" disabled>
        </select>
        <br />
          <label for="cliente">Distribuidor</label>
            <div class="form-group">
                <img src="../presentacion/imagenes/cargar.png" width="35" height="34" id="cargar" align="right" style="margin-right:10px"/>
                <input name="distribuidor" id="distribuidor" type="text" readonly="readonly" class="form-control"/>
            </div>
        <input name="id_cliente" id="id_cliente" type="text" style="display:none" readonly="readonly"/>
        <br />
        <br />
    </div>
    <div class="form-group" id="resultado_div" style="display:none">
    	<label for="Resultado">Resultado Visita:</label><span class="asterisco">*</span>
        <select name="resultado_text" id="resultado_text" class="form-control">
        	<option value="">Elija...</option>
            <option>EFECTIVA</option>
            <option>NO EFECTIVA</option>
        </select>
        <select id="ado">
        	<option value="aqui" >Elija...</option>
            <option selected="selected">EFECTIVA</option>
            <option>NO EFECTIVA</option>
        </select>
    </div>
    <br />
    <div id="visita_efectiva" style="display:none">
        <div class="div_menu" id="div_menu" style="display:none">
                <ul>
                    <li id="link1">
                        <a href="../presentacion/form_cliente_nuevo.php" target="info" style="width:100%;"><span class="icon-user-plus"></span>&nbsp;&nbsp;&nbsp;NUEVO ODONTOLOGO</a>
                    </li>
                    <li id="link2">
                        <a href="../presentacion/form_encuesta_consumo.php" target="info" style="width:100%;" ><span class="icon-stats-bars"></span>&nbsp;&nbsp;&nbsp;ENCUESTA DE CONSUMO</a>
                    </li>
                    <li style="border-bottom:none;" id="link3">
                        <a href="../presentacion/form_transferencias_med.php" target="info" style="width:100%;"><span class="icon-paste"></span>&nbsp;&nbsp;&nbsp;PEDIDOS Y TRASFERENCIAS</a>
                    </li>
                </ul>
        </div>
    	<iframe style="width:100%;border:1px solid transparent; display:none" name="info" id="info" scrolling="auto"></iframe>
		<!--<iframe style=" padding-top:20px; width:100%;border:1px solid transparent" name="info" id="info" scrolling="auto" height="500px"></iframe>-->
	</div>
<div id="visita_no_efectiva" style="display:none">
	<textarea class="form-control" name="observacion" id="observacion" style="width:95%;" rows="6"></textarea>
    <br />
    <center>
    	<input type="submit" name="GUARDAR" id="GUARDAR" class="form-control btn" value="GUARDAR"/>
    </center>
</div>
</body>
</html>
<?php
}
else
{
	//echo 'sasa';
	header("Location: ../index.php");
}
?>