<?php
error_reporting(0);
include ('../logica/session.php');
header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>CONSULTAR GESTION</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 
<script>
$(document).ready(function() {
    $('#gestion').DataTable();
} );

$(document).ready(function() {
    $('#gestionTodos').DataTable();
} );
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}

</script>
</head>
<?php
include('../datos/conex_copia.php');
	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);
$usua;
?>
<body>
<br /><br />

<form id="rutero" name="rutero" method="post" action="asignar_ruta.php" >
<fieldset style="margin:auto auto; width:90%;">
<legend>Ingresar Rutero</legend>
<div class="col-md-12">
      <div class="col-md-2">
             <label for="cliente">FECHA GESTI&Oacute;N:</label><span class="asterisco">*</span><br />
                <input type="date" class="form-control" name="fecha" id="fecha" required="required"/>
        </div>
        <div class="col-md-4">
             <label for="fecha">USUARIO:</label><span class="asterisco">*</span><br />
              <select class="form-control" name="idUsuario[]" multiple="multiple" required="required"> 
        <option value="" disabled="disabled" selected="selected">Seleccione</option>           
                    <?php 
                        $consulta =mysqli_query($conex,"
         SELECT ID_USUARIO, USER FROM 3m_usuario
		 WHERE PRIVILEGIOS =2 AND ESTADO =1  AND USER <> 'medellin' AND USER <> 'emesa' AND USER <> 'visita' 
		  ORDER BY USER ASC ;");
                 
                 
                  while($dato=mysqli_fetch_array($consulta)) { ?>
                <option value="<?php echo $dato['ID_USUARIO']; ?>"><?php echo $dato['USER']; ?></option>
                <?php } ?>
                    
            </select>
        </div>
        <div class="col-md-2">
        <label for="fecha">N&uacute;mero de gestiones</label><span class="asterisco">*</span><br />
        <input type="number" min="0" name="gestiones" class="form-control" required="required"/>
        </div>
    
        <div class="col-md-3">
           <label for="cliente"></label><br /><br />
    <button class="btn btn-primary" name="btn_ingresar_gestion">Ingresar</button>
    </div>
</div>
     
    </fieldset>
    
</form>
<div class="table table-responsive">
<?php 



if(isset($_POST["btn_ingresar_gestion"])){ 
	$idUsuario		= $_POST["idUsuario"];
	$fecha	    	= $_POST["fecha"];
	$gestiones  	= $_POST["gestiones"];
	
	foreach($idUsuario as $visitador){
		
		$CONSULTAR_ANT_RUTERO = mysqli_query($conex,"SELECT ID_RUTERO FROM 3m_rutero WHERE FECHA_GESTION='".$fecha."' AND ID_USUARIO=".$visitador."");
		$datos_rutero = mysqli_fetch_array($CONSULTAR_ANT_RUTERO);
		$ID_RUTERO = $datos_rutero["ID_RUTERO"];
		
			if(empty($ID_RUTERO)){
				$INGRESAR_RUTERO = mysqli_query($conex,"
				INSERT INTO 3m_rutero(FECHA_GESTION,ID_USUARIO,NUMERO_GESTIONES,FECHA_ASIGNACION) 
				VALUES ('".$fecha."',".$visitador.",".$gestiones.",NOW());");
						
			}else{
				$INGRESAR_RUTERO= mysqli_query($conex,"UPDATE 3m_rutero SET NUMERO_GESTIONES=".$gestiones." WHERE ID_RUTERO=".$ID_RUTERO."");
				
				}
			
	
			echo mysqli_error($INGRESAR_RUTERO);
		}
	?>
    <script>
		alert('Rutero Ingresado');

    </script>
    <?php	
	}

	?>
 
    <br />
<table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
        <tr>
            <th colspan='11' class="principal">RUTERO</th>
        </tr>
        </table>
    <br />
    
    <form id="consultar_rutero" name="consultar_rutero" method="post" action="asignar_ruta.php" >
    	<div class="col-md-12">
        <div class="col-md-2">
                     <label for="mes">MES</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="mes"  >
                        	<option value="">SELECCIONE</option>
                            <option value="1">ENERO</option>
                            <option value="2">FEBRERO</option>
                            <option value="3">MARZO</option>
                            <option value="4">ABRIL</option>
                            <option value="5">MAYO</option>
                            <option value="6">JUNIO</option>
                            <option value="7">JULIO</option>
                            <option value="8">AGOSTO</option>
                            <option value="9">SEPTIEMBRE</option>
                            <option value="10">OCTUBRE</option>
                            <option value="11">NOVIEMBRE</option>
                            <option value="12">DICIEMBRE</option>
                        </select>
                </div>

        <div class="col-md-2">
             <label for="cliente">FECHA GESTI&Oacute;N:</label><span class="asterisco">*</span><br />
                <input type="date" class="form-control" name="fecha_gestion" id="fecha_gestion"/>
        </div>
        <div class="col-md-4">
             <label for="fecha">USUARIO:</label><span class="asterisco">*</span><br />
              <select class="form-control" name="idPromotores[]" multiple="multiple"> 
        <option value="" disabled="disabled" selected="selected">Seleccione</option>           
                    <?php 
                        $consulta_usuarios =mysqli_query($conex,"
         SELECT ID_USUARIO, USER FROM 3m_usuario
		 WHERE PRIVILEGIOS =2 AND ESTADO =1 AND USER <> 'medellin' AND USER <> 'emesa' AND USER <> 'visita' 
		  ORDER BY USER ASC ;");
                 
                 
                  while($dato=mysqli_fetch_array($consulta_usuarios)) { ?>
                <option value="<?php echo $dato['ID_USUARIO']; ?>"><?php echo $dato['USER']; ?></option>
                <?php } ?>
                    
            </select>
        </div>
        
    
        <div class="col-md-3">
           <label for="cliente"></label><br /><br />
    <button title="Consultar" name="consultar">
    <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR">
    </button>

    
    </div>
</div>
    
    </form>
    <?php
    	if(isset($_POST["consultar"])){
			$MES            = $_POST["mes"];
			$FECHA 			= $_POST["fecha_gestion"];
			$ID_PROMOTORES  =$_POST["idPromotores"];
			
			$usuarios_seleccionados= implode(',',$ID_PROMOTORES);
			
			if(empty($MES) && empty($FECHA) && empty($ID_PROMOTORES) ){
				?>
                <script>
                	alert("Seleccione una opcion");
                </script>
                <?php
				}
				//busqueda por mes
			else if(empty($MES)==false && empty($FECHA) && empty($ID_PROMOTORES) ){
				$CONSULTAR_RUTERO = mysqli_query($conex,"
				SELECT B.USER,CONCAT(B.NOMBRES,' ',B.APELLIDOS) AS VISITADOR,A.FECHA_GESTION,A.NUMERO_GESTIONES
				FROM 3m_rutero AS A
				INNER JOIN 3m_usuario AS B ON B.ID_USUARIO = A.ID_USUARIO
				WHERE MONTH(FECHA_GESTION)=".$MES."
				ORDER BY A.FECHA_GESTION DESC");
				
				}
				//busqueda por fecha
			else if(empty($MES) && empty($FECHA)==false && empty($ID_PROMOTORES) ){
				$CONSULTAR_RUTERO = mysqli_query($conex,"
				SELECT B.USER,CONCAT(B.NOMBRES,' ',B.APELLIDOS) AS VISITADOR,A.FECHA_GESTION,A.NUMERO_GESTIONES
				FROM 3m_rutero AS A
				INNER JOIN 3m_usuario AS B ON B.ID_USUARIO = A.ID_USUARIO
				WHERE DATE(FECHA_GESTION)='".$FECHA."'
				ORDER BY A.FECHA_GESTION DESC");
				
				}
				//busqueda por usuario
			 else if(empty($MES) && empty($FECHA) && empty($ID_PROMOTORES)==false ){
				$CONSULTAR_RUTERO = mysqli_query($conex,"
				SELECT B.USER,CONCAT(B.NOMBRES,' ',B.APELLIDOS) AS VISITADOR,A.FECHA_GESTION,A.NUMERO_GESTIONES
				FROM 3m_rutero AS A
				INNER JOIN 3m_usuario AS B ON B.ID_USUARIO = A.ID_USUARIO
				WHERE A.ID_USUARIO IN (".$usuarios_seleccionados.")
				ORDER BY A.FECHA_GESTION DESC");
				
				}
				//busqueda por mes y usuario
			else if(empty($MES)==false && empty($FECHA) && empty($ID_PROMOTORES)==false ){
				$CONSULTAR_RUTERO = mysqli_query($conex,"
				SELECT B.USER,CONCAT(B.NOMBRES,' ',B.APELLIDOS) AS VISITADOR,A.FECHA_GESTION,A.NUMERO_GESTIONES
				FROM 3m_rutero AS A
				INNER JOIN 3m_usuario AS B ON B.ID_USUARIO = A.ID_USUARIO
				WHERE A.ID_USUARIO IN (".$usuarios_seleccionados.") AND MONTH(FECHA_GESTION)=".$MES."
				ORDER BY A.FECHA_GESTION DESC");
				
				}	
				//busqueda por fecha y usuario
			else if(empty($MES)==false && empty($FECHA) && empty($ID_PROMOTORES)==false ){
				$CONSULTAR_RUTERO = mysqli_query($conex,"
				SELECT B.USER,CONCAT(B.NOMBRES,' ',B.APELLIDOS) AS VISITADOR,A.FECHA_GESTION,A.NUMERO_GESTIONES
				FROM 3m_rutero AS A
				INNER JOIN 3m_usuario AS B ON B.ID_USUARIO = A.ID_USUARIO
				WHERE A.ID_USUARIO IN (".$usuarios_seleccionados.") AND DATE(FECHA_GESTION)='".$FECHA."'
				ORDER BY A.FECHA_GESTION DESC");
				
				}	
			}else{
				$CONSULTAR_RUTERO = mysqli_query($conex,"
				SELECT B.USER,CONCAT(B.NOMBRES,' ',B.APELLIDOS) AS VISITADOR,A.FECHA_GESTION,A.NUMERO_GESTIONES
				FROM 3m_rutero AS A
				INNER JOIN 3m_usuario AS B ON B.ID_USUARIO = A.ID_USUARIO
				ORDER BY A.FECHA_GESTION DESC");
				}
	
	
	?>
    
    
         <table style="width:99%; margin:auto auto;"  id="gestion">
        <thead>
        <tr style=" text-align:center;">
    		<th class="TITULO">FECHA</th>
            <th class="TITULO">USUARIO</th>
            <th class="TITULO">VISITADOR</th>
             <th class="TITULO">NUMERO DE GESTIONES</th>
     
        </tr>
        </thead>
        <tbody>
		<?PHP
        while($dato=mysqli_fetch_array($CONSULTAR_RUTERO))
        {
            
         ?>
            <tr class="datos" >
           		<td style="text-align:left;"><?php echo $dato["FECHA_GESTION"]?></td>
                <td style="text-align:left;"><?php echo $dato["USER"]?></td>
                <td style="text-align:left;"><?php echo $dato["VISITADOR"]?></td>
                <td style="text-align:left;"><?php echo $dato["NUMERO_GESTIONES"]?></td>
                
            </tr>
        <?php 
        }
        ?>
  </tbody>
  </table>
  
    </div>
</body>
</html>