<?php
	include ('../logica/session.php');
	error_reporting(0);	
         header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CONSULTAR GESTION</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 
<style>


</style>
<script>
$(document).ready(function() {
    $('#gestion').DataTable();
} );

$(document).ready(function() {
    $('#gestionTodos').DataTable();
} );

function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}


</script>
</head>
<?php
include('../datos/conex.php');
	mysql_query("SET NAMES utf8");
	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);
$usua;
?>
<body>
<br /><br />

<div class="table table-responsive">
<?php 

$CONSULTA_PROMOCIONES =mysql_query("SELECT A.ID_CATEGORIA,A.CATEGORIA,A.SUBCATEGORIA,A.STOCK,A.DESCRIPCION,A.PRECIO_UNIDAD,A.ESTADO
	FROM 3m_categoria AS A  
	ORDER BY DESCRIPCION ASC;",$conex);


?>
<div class="col-md-12">
<div class="col-md-9">
<a  href="reporte_productos.php" >
      <button type="submit" name="editar" id="button" value="Reporte" class="btn btn-default"><img src="imagenes/exportar.png" width="15" height="15" style="background-size:cover" title="CONSULTAR"> EXPORTAR PRODUCTOS</button>

	  </a>
	  
</div>
<div class="col-md-3" style="text-align:right;">
<a  href="javascript:ventanaSecundaria('../presentacion/crear_productos.php?x=<?php echo base64_encode($dato['ID_CATEGORIA'])?>')" >
      <button type="submit" name="editar" id="button" value="Consultar" class="btn btn-default"><img src="imagenes/CHULO1.png" width="15" height="15" style="background-size:cover" title="CONSULTAR">   CREAR PRODUCTOS</button>
  </a>
</div>
</div>
<br /><br />
<table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
	<tr>
		<th colspan='11' class="principal">PRODUCTOS</th>
	</tr>
    </table>
<br />
	 <table style="width:99%; margin:auto auto;"  id="gestion" class="table table-striped">
    <thead>
	<tr style=" text-align:center;">
    <th class="TITULO">CATEGORIA</th>
    <th class="TITULO">SUB CATEGORIA</th>
    <th class="TITULO">STOCK</th>
    <th class="TITULO">DESCRIPCI&Oacute;N</th>
    <th class="TITULO">ESTADO</th>
    <th class="TITULO">PRECIO</th>
    <?php 
	if($privilegios==1){?>
     <th class="TITULO">EDITAR</th>
    <?php } 
	
	?>
   
	</tr>
    </thead>
    <tbody>
    <?PHP
    while($dato=mysql_fetch_array($CONSULTA_PROMOCIONES))
	{	
	?>
		<tr class="datos" >
            <td style="text-align:left;"><?php echo $dato["CATEGORIA"]?></td>
        	<td style="text-align:left;"><?php echo $dato["SUBCATEGORIA"]?></td>
            <td style="text-align:left;"><?php echo $dato["STOCK"]?></td>
            <td style="text-align:left;"><?php echo $dato["DESCRIPCION"]?></td>
            <td style="text-align:left;"><?php if($dato["ESTADO"]==1){ echo "ACTIVO";}else { echo "INACTIVO";}?></td>
            <td style="text-align:left;">$ <?php echo  number_format($dato["PRECIO_UNIDAD"],0,',','.');?></td>
             <?php 
	if($privilegios==1){?>
                <td>
              <a  href="javascript:ventanaSecundaria('../presentacion/editar_promocion.php?x=<?php echo base64_encode($dato['ID_CATEGORIA'])?>&area=2')" >
                 
                    <button type="submit" name="editar" id="button" value="Consultar" class="btn btn-default"><img src="imagenes/lapiz.png" width="15" height="15" style="background-size:cover" title="CONSULTAR"></button>
                    </a>
                </td> 
                <?php } ?> 
        </tr>
	<?php 
	}
	?>
    </tbody>
</table>
	
    </div>
</body>
</html>