<?php
	include("../logica/session.php");
	error_reporting(0);
 header('Content-Type: text/html; charset=UTF-8'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<meta http-equiv="Expires" content="0">

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 

<title>Pedidos</title>
<script>
$(document).ready(function() {
    $('#pedidos').DataTable();
	
	 $('#consultar').click(function()
	{
		var mes = $('#mes').val();
		var year = $('#year').val();
		var distribuidor = $('#distribuidor').val();
		
		if(mes != '' && year != '' && distribuidor != ''){
		  $.ajax(
		{
				url:'busqueda_pedidos_dentales.php',
				data:
				{
						mes: mes,
						year: year,
						distribuidor:distribuidor,
						tipo:1
				},
				type: 'post',
				
				success: function(data)
				{
						$('#resultados').html(data);
						$('#sin_filtro').css("display","none");
                        $('#exportar_tipificaciones').css("display","none");
						$('#resultados').css("display","block");
						$('#FormularioExportacion').css("display","block");
                        
				}
		})  
		}else{
			alert("Seleccion A�o, Mes y Distribuidor para realizar la busqueda");
			
		}
	});
	
	$(".botonExcel").click(function(event) {
		$("#datos_a_enviar").val( $("<div>").append( $("#resultados").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
      });   
	
	
} );
</script>

</head>
<?php 
	include('../datos/conex_copia.php');
	
	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);

?>
<body>

<fieldset style="margin:auto auto; width:90%;">
<div class="col-md-12" style="margin-top:20px;">
		<div class="col-md-2">
			<label for="fecha">AÑO:</label><br />
              <select class="form-control" name="year" id="year" > 
					<option value="" disabled="disabled" selected="selected">Seleccione</option>           
                    <?php 
                        $consultaYear =mysqli_query($conex,"
						 SELECT DISTINCT YEAR(FECHA_PEDIDO) AS YEAR FROM 3m_pedido");
						 while($dato=mysqli_fetch_array($consultaYear)) { ?>
                <option value="<?php echo $dato['YEAR']; ?>"><?php echo $dato['YEAR']; ?></option>
                <?php } ?>
                    
            </select>
		</div>

        <div class="col-md-2">
                     <label for="mes">MES</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="mes" id="mes">
                        	<option value="">SELECCIONE</option>
                            <option value="1">ENERO</option>
                            <option value="2">FEBRERO</option>
                            <option value="3">MARZO</option>
                            <option value="4">ABRIL</option>
                            <option value="5">MAYO</option>
                            <option value="6">JUNIO</option>
                            <option value="7">JULIO</option>
                            <option value="8">AGOSTO</option>
                            <option value="9">SEPTIEMBRE</option>
                            <option value="10">OCTUBRE</option>
                            <option value="11">NOVIEMBRE</option>
                            <option value="12">DICIEMBRE</option>
                        </select>
                </div>
			
        <div class="col-md-5">
             <label for="fecha">DISTRIBUIDOR:</label><span class="asterisco">*</span><br />
              <select class="form-control" name="distribuidor" id="distribuidor" > 
					<option value="" disabled="disabled" selected="selected">Seleccione</option>           
                    <?php 
                        $consultaDistribuidores =mysqli_query($conex,"
						 SELECT ID_DENTAL, NOMBRE_DENTAL FROM 3m_dentales ORDER BY NOMBRE_DENTAL ASC");
						 if(empty($consultaDistribuidores)){
							 echo "esta vacio";
						 }
		 
                  while($dato=mysqli_fetch_array($consultaDistribuidores)) { ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php } ?>
                    
            </select>
        </div>
   
        <div class="col-md-2">
		<br>
           <button title="Consultar" name="consultar" id="consultar"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>
		</div>
		   <div class="col-md-2" >
		   <br>
			 <form action="excel_pedido_dental.php" method="post"  id="FormularioExportacion" style="display: none;">
			<button class="btn btn-default botonExcel"> Exportar</button>

			<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
			</form>
		
                                       
                                    </div>
    
	</div>
     
    </fieldset>
    
<div class="table table-responsive" style="display: none;" id="resultados">

            
</div>

<div class="table table-responsive" style="display: block;" id="sin_filtro">

	<?php 
	$consultaGestionUsuario =mysqli_query($conex,"
			SELECT a.TOTAL_PEDIDO,a.DISTRIBUIR,a.FECHA_PEDIDO,a.FORMA_PAGO,a.TIPO_PEDIDO,b.USER,c.IDENTIFICACION_CLIENTE,CONCAT(c.NOMBRE_CLIENTE,' ',c.APELLIDO_CLIENTE) AS CLIENTE
			FROM 3m_pedido AS a 
			INNER JOIN 3m_usuario AS b ON b.ID_USUARIO = a.ID_USUARIO_FK
			INNER JOIN 3m_cliente AS c ON c.ID_CLIENTE = a.ID_CLIENTE_FK
			WHERE ID_USUARIO NOT IN(1,2,3,4,5,12,13,14,20)");
	?>


<table style="width:99%; margin:auto auto;" rules="none" >
	<tr>
		<th colspan='11' class="principal">DISTRIBUIDOR</th>
	</tr>
    </table>
    <table style="width:99%; margin:auto auto;" rules="none" id="pedidos">
    <thead>
	<tr>

		<th class="TITULO">TOTAL PEDIDO</th>
        <th class="TITULO">DISTRIBUIDOR</th>
        <th class="TITULO">FECHA DE PEDIDO</th>
        <th class="TITULO">FORMA DE PAGO</th>
        <th class="TITULO">TIPO </th>
		<th class="TITULO">VISITADOR</th>
		<th class="TITULO">IDENTIFICACION</th>
		<th class="TITULO">CLIENTE</th>
      
	</tr>
    </thead>
    <tbody>
    <?PHP
	$i=1;
    while($dato=mysqli_fetch_array($consultaGestionUsuario))
	{
		 
	?>
		<tr class="datos">
    
            <td style="text-align:center"><?php echo $dato["TOTAL_PEDIDO"]?></td>
            <td style="text-align:center"><?php echo $dato["DISTRIBUIR"]?></td>
            <td style="text-align:center"><?php echo $dato["FECHA_PEDIDO"]?></td>
            <td style="text-align:center"><?php echo $dato["FORMA_PAGO"]?></td>
            <td style="text-align:center"><?php echo $dato["TIPO_PEDIDO"]?></td>
            <td style="text-align:center"><?php echo $dato["USER"]?></td>	
            <td style="text-align:center"><?php echo $dato["IDENTIFICACION_CLIENTE"]?></td>	
            <td style="text-align:center"><?php echo $dato["CLIENTE"]?></td>
         
		</tr>
	<?php 
	}
	?>
    </tbody>
</table>
</div>
</body>
</html>