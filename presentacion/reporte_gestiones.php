<?php
	include ('../logica/session.php');

 /*$string_intro = getenv("QUERY_STRING"); 
 parse_str($string_intro);
/*	
$servidor="192.168.0.7"; 
$usuariobd="webuser"; 
$password="P4t4d4 4l n3n3!!!";
$base="3M";

$servidor="127.0.0.1"; 
$usuariobd="encontacto"; 
$password="$7x%shK]vlqM"; 
$base="encontac_3m";
	
$conex= mysqli_connect($servidor,$usuariobd,$password,$base)or die("No se Puede conectar al Servidor");

  if (mysqli_connect_errno())
	  {
	 	 echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  }
	mysqli_set_charset($conex,"utf8"); */
	
	include('../datos/conex.php');
	
	$consulta_clientes = mysql_query("SELECT A.ACCION,A.FECHA_GESTION, 
			CONCAT( B.NOMBRE_CLIENTE,' ', B.APELLIDO_CLIENTE )  AS CLIENTE,
			B.TIPO_IDENTIFICACION, B.IDENTIFICACION_CLIENTE,
			B.CIUDAD_CLIENTE,U.USER,B.DIRECCION_CLIENTE,B.CELULAR_CLIENTE,B.TELEFONO_CLIENTE,E.CLASIFICACION_ODONTOLOGO
		    FROM 3m_gestion AS A
			INNER JOIN 3m_cliente AS B ON B.ID_CLIENTE = A.ID_CLIENTE_FK
			INNER JOIN 3m_usuario AS U  ON  U.ID_USUARIO = A.ID_ASESOR_GESTION
			INNER JOIN 3m_encuesta AS E ON E.ID_CLIENTE_FK_5 = B.ID_CLIENTE
			WHERE ACCION ='CREACION PACIENTE' AND  E.CLASIFICACION_ODONTOLOGO <> ''
			AND  E.CLASIFICACION_ODONTOLOGO IS NOT NULL AND  E.CLASIFICACION_ODONTOLOGO <> 'NULL'
			AND A.ID_ASESOR_GESTION=".$id_usu."
			ORDER BY FECHA_GESTION DESC;",$conex);

 if(mysql_num_rows($consulta_clientes)> 0){
	 
	 date_default_timezone_set('America/Mexico_City');
	 
	  require_once("PHPExcel-1.8/Classes/PHPExcel.php");
	 
	 $objPHPExcel = new PHPExcel();
	 $objPHPExcel->getProperties()->setCreator("Codedrinks") // Nombre del autor
    ->setLastModifiedBy("Codedrinks") //Ultimo usuario que lo modificó
    ->setTitle("Reporte Clientes Nuevos") // Titulo
    ->setSubject("Reporte Clientes Nuevos") //Asunto
    ->setDescription("Reporte Clientes Nuevos") //Descripción
    ->setKeywords("Reporte Clientes Nuevoss") //Etiquetas
    ->setCategory("Reporte Clientes Nuevos"); //Categorias
	 
	 $tituloReporte = "CLIENTES NUEVOS ";
$titulosColumnas = array('FECHA GESTION', 'TIPO IDENTIFICACION', 'IDENTIFICACION', 'CLIENTE','DIRECCION','CIUDAD','CELULAR','TELEFONO FIJO','CLASIFICACION','VISITADOR');

	 // Se combinan las celdas A1 hasta D1, para colocar ahí el titulo del reporte
$objPHPExcel->setActiveSheetIndex(0)
    ->mergeCells('A1:J1');
	
	
	// Se agregan los titulos del reporte
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1',$tituloReporte) // Titulo del reporte
    ->setCellValue('A3',  $titulosColumnas[0])  //Titulo de las columnas
    ->setCellValue('B3',  $titulosColumnas[1])
    ->setCellValue('C3',  $titulosColumnas[2])
    ->setCellValue('D3',  $titulosColumnas[3])
	->setCellValue('E3',  $titulosColumnas[4])
	->setCellValue('F3',  $titulosColumnas[5])
	->setCellValue('G3',  $titulosColumnas[6])
	->setCellValue('H3',  $titulosColumnas[7])
	->setCellValue('I3',  $titulosColumnas[8])
	->setCellValue('J3',  $titulosColumnas[9]);
	
	
	//se agregan los datos
	$i = 4; //Numero de fila donde se va a comenzar a rellenar
 while ($fila = mysql_fetch_array($consulta_clientes)) {
     $objPHPExcel->setActiveSheetIndex(0)
         ->setCellValue('A'.$i, $fila['FECHA_GESTION'])
         ->setCellValue('B'.$i, $fila['TIPO_IDENTIFICACION'])
         ->setCellValue('C'.$i, $fila['IDENTIFICACION_CLIENTE'])
         ->setCellValue('D'.$i, $fila['CLIENTE'])
		 ->setCellValue('E'.$i, $fila['DIRECCION_CLIENTE'])
		 ->setCellValue('F'.$i, $fila['CIUDAD_CLIENTE'])
		 ->setCellValue('G'.$i, $fila['CELULAR_CLIENTE'])
		 ->setCellValue('H'.$i, $fila['TELEFONO_CLIENTE'])
		 ->setCellValue('I'.$i, $fila['CLASIFICACION_ODONTOLOGO'])
		 ->setCellValue('J'.$i, $fila['USER']);
     $i++;
 }
 
 //ESTILO DEL TITULO 
 
 $estiloTituloReporte = array(
    'font' => array(
        'name'      => 'Verdana',
        'bold'      => true,
        'italic'    => false,
        'strike'    => false,
        'size' =>14,
        'color'     => array(
            'rgb' => 'FFFFFF'
        )
    ),
  
   'fill' => array(
  'type'  => PHPExcel_Style_Fill::FILL_SOLID,
  'color' => array(
            'argb' => '808a0808')
  ),
    
  
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_NONE
        )
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'rotation' => 0,
        'wrap' => TRUE
    )
);
 
 //ESTILO DE LAS COLUMNAS
 
 $estiloTituloColumnas = array(
    'font' => array(
        'name'  => 'Arial',
        'bold'  => true,
		'color'     => array(
            'rgb' => '8a0808'
        )
        
    ),
    'fill' => array(
        'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
  'rotation'   => 90,
  'color' => array(
            'argb' => '808a0808')

    ),
    
    'alignment' =>  array(
        'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'      => TRUE
    )
);

//ESTILO DE LA INFORMACION
$estiloInformacion = new PHPExcel_Style();
$estiloInformacion->applyFromArray( array(
    'font' => array(
        'name'  => 'Arial'
    )
));



$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($estiloTituloReporte);
$objPHPExcel->getActiveSheet()->getStyle('A3:J3')->applyFromArray($estiloTituloColumnas);
$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:J".($i-1));
	
	
	//ANCHO AUTOMATICO DE LAS CELDAS
	
	for($i = 'A'; $i <= 'J'; $i++){
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
}

// Se asigna el nombre a la hoja
$objPHPExcel->getActiveSheet()->setTitle('Clientes Nuevos');
 
// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
$objPHPExcel->setActiveSheetIndex(0);
 
// Inmovilizar paneles
$objPHPExcel->getActiveSheet(0)->freezePane('A4');
//$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,9);
	
	
	// Se manda el archivo al navegador web, con el nombre que se indica, en formato 2007
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Reporte_Clientes_Nuevos.xls"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
}
else{
    print_r('No hay resultados para mostrar');
}

	 