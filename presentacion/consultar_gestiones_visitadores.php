<?php
	include("../logica/session.php");
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 

<title>CONSULTA PEDIDO</title>
<script>
$(document).ready(function() {
    $('#pedidos').DataTable();
} );
</script>

<script language=javascript> 
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}
</script> 
<style>
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
</head>
<?PHP
include('../datos/conex_copia.php');
if(isset($pedid))
{
	$ID_CLIENTE=base64_decode($pedid);
}
else
{
	$ID_CLIENTE=$ID_CLIENTE;
}


 $consulta=mysqli_query($conex," 
 SELECT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,a.OBSERVACION_GESTION,a.FECHA_PROXIMA_VISITA,
 b.ID_CLIENTE,CONCAT(b.NOMBRE_CLIENTE,' ',b.APELLIDO_CLIENTE) AS NOMBRE,b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,
 b.CELULAR_CLIENTE,b.TELEFONO_CLIENTE,b.TELEFONO_CLIENTE,b.DIRECCION_CLIENTE,
 a.ASESOR_GESTION,a.ID_ASESOR_GESTION,a.OBSERVACION_GESTION
 FROM 3m_gestion AS a
 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
 WHERE ID_ASESOR_GESTION =". $id_usu." 
 ORDER BY a.FECHA_GESTION DESC;");
?>
<body>
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			<center>
					<a href="https://app-peoplemarketing.com/farmadecolombia/"  class="btn_continuar">
						<img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
					</a>
				</center>
			</center>
			</span>
				
			<?php }else{?>
<div class="container-fluid">
<table align="right" style="width:100%;">
    	<tr> 	
          <th colspan="6">Bienvenid@  <?php echo $usua?></th> 
          </tr>
          <tr>
            <input name="usua" style="display:none;" type="text" id="usua" value="<?php echo $usua; ?>" readonly/>      
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" >
        </a></td>
       <!-- <td><a href="../presentacion/kpi_visitador.php">
        <img src="../presentacion/imagenes/kpi.png" width="56" height="57" id="cambiar" title="KPI'S" >
         <center> <font style="font-size:10px;" >KPI'S</font></center>
        </a></td>-->
        <td><a href="../presentacion/consultar_gestiones_visitadores.php">
        <img src="../presentacion/imagenes/gestiones.png" width="56" height="57" id="cambiar" title="GESTIONES" >
         <center> <font style="font-size:10px;" >Gestiones</font></center>
        </a></td>
        <!--<td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Pedidos</font></center>
        </a></td>-->
          <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PR&Oacute;XIMA VISITA" >
         <center> <font style="font-size:10px;" >Pr&oacute;xima <br />Visita</font></center>
        </a></td>
        <td><a href="../presentacion/consulta_productos_visita.php?">
        <img src="../presentacion/imagenes/productos.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Productos</font></center>
        </a></td>
         <td><a href="../presentacion/reporte_clientes_nuevos.php">
        <img src="../presentacion/imagenes/excel.png" width="48" height="51" id="cambiar" title="REPORTE EXCEL" ><br />
       <center> <font style="font-size:10px;" >Clientes <br />Nuevos</font></center>
        </a></td>
        </tr>
    </table>
    </div>
    <br />

  <br />
<br />
<div class="table table-responsive">
    <div class="col-md-6">
        <?php 
        $mes = date("m");
        $year = date("Y");
         /*if($mes ==01){ $nombre_mes ='Enero';}
         else if($mes ==02){$nombre_mes ='Febrero';}
         else if($mes ==03){$nombre_mes ='Marzo';}
         else if($mes ==04){$nombre_mes ='Abril';}
         else if($mes ==05){$nombre_mes ='Mayo';}
         else if($mes ==06){$nombre_mes ='Junio';}
         else if($mes ==07){$nombre_mes ='Julio';}
         else if($mes ==08){$nombre_mes ='Agosto';}
         else if($mes ==09){$nombre_mes ='Septiembre';}
         else if($mes ==10){$nombre_mes ='Octubre';}
         else if($mes ==11){$nombre_mes ='Noviembre';}
         else if($mes ==12){$nombre_mes ='Diciembre';}*/
         
         $consulta_efectivas = mysqli_query($conex,"SELECT COUNT(*) FROM 3m_gestion
            WHERE MONTH(FECHA_GESTION)='".$mes."' AND YEAR(FECHA_GESTION)='".$year."' AND TIPIFICACION_GESTION='EFECTIVA' AND ID_ASESOR_GESTION=".$id_usu);
       while($dato_efectivas=mysqli_fetch_array($consulta_efectivas))
        {
           $conteo_efectivas = $dato_efectivas["COUNT(*)"];
        }
         
         ?>
        <h4>Numero de efectivas<b> <?php echo $nombre_mes; ?>:</b>
             <span class="label label-danger"><?php echo $conteo_efectivas; ?></span></h4>
    </div>
<table style="width:99%; margin:auto auto;" rules="none" >
	<tr>
		<th colspan='11' class="principal">GESTIONES REALIZADAS</th>
	</tr>
    </table>
    <br />
    <table style="width:99%; margin:auto auto;" rules="none" id="pedidos"class="table table-striped">
    <thead>
	<tr>
        <th class="TITULO">FECHA GESTI&Oacute;N</th>
        <th class="TITULO">ACCI&Oacute;N</th>
        <th class="TITULO">TIPIFICACI&Oacute;N</th>
        <th class="TITULO">SUB TIPIFICACI&Oacute;N</th>
        <th class="TITULO">IDENTIFICACI&Oacute;N </th>
        <th class="TITULO">CLIENTE </th>
        <th class="TITULO">CELULAR </th>
		<th class="TITULO">TEL&Eacute;EFONO</th>
		<th class="TITULO">DIRECCI&Oacute;N</th>
		<th class="TITULO">OBSERVACI&Oacute;N</th>
	</tr>
    </thead>
    <tbody>
    <?PHP
    while($dato=mysqli_fetch_array($consulta))
	{
	?>
		<tr class="datos">
          	<td><?php echo $dato["FECHA_GESTION"]?></td>
            <td><?php echo $dato["ACCION"]?></td>
            <td><?php echo $dato["TIPIFICACION_GESTION"]?></td>
            <td><?php echo $dato["SUB_TIPIFICACION"]?></td>
            <td><?php echo $dato["IDENTIFICACION_CLIENTE"]?></td>
            <td><?php echo $dato["NOMBRE"]?></td>
            <td><?php echo $dato["CELULAR_CLIENTE"]?></td>	
            <td><?php echo $dato["TELEFONO_CLIENTE"]?></td>	
            <td><?php echo $dato["DIRECCION_CLIENTE"]?></td>
			<td><?php echo $dato["OBSERVACION_GESTION"]?></td>
            
		</tr>
	<?php 
	}
	?>
    </tbody>
</table>
</div>
<?php } ?>
</body>
</html>