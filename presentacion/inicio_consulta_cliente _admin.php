<?php
	include("../logica/session.php");
	header("Content-Type: text/html;charset=utf-8");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CLIENTE</title>
<link href="css/menu_visitas.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="../presentacion/fonts.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 
</head>

<script language=javascript> 
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}



$(document).ready(function() {
  
	
	
	$("#eliminar_cliente").click(function() {
		 var cliente = $(this).data();
        var id_cliente = cliente.id;
        var identificacion = cliente.identificacion;
		
		
		$.ajax(
				{
					url:'../logica/eliminar_cliente.php',
					data:
					{
						id_cliente: id_cliente,
						identificacion:identificacion,
						tipo:1
					},
					type: 'post',
					success: function(data)
					{
						if(data==3){
							alert("El odontologo no se puede eliminar");
						}
						else{	
						alert("El odontologo eliminado");
						location.href="consultar_nuevos_clientes.php";
						}
					}
				});
	});
	
} );

</script>
<?php
	include('../datos/conex_copia.php');
	

		
	$id_cli=base64_decode($x);

	
 // datos del cliente

		$consulta=mysqli_query($conex,"SELECT * FROM 3m_cliente WHERE ID_CLIENTE='$id_cli'");
		echo mysqli_error($conex);
		while($datos=(mysqli_fetch_array($consulta)))
		{
			$id_cli                 =$datos['ID_CLIENTE'];
			$IDENTIFICACION_CLIENTE =$datos['IDENTIFICACION_CLIENTE'];
			$TELEFONO_CLIENTE       =$datos['TELEFONO_CLIENTE'];
			$CELULAR_CLIENTE        =$datos['CELULAR_CLIENTE'];
			$DIRECCION_CLIENTE      =$datos['DIRECCION_CLIENTE'];
			
			$_SESSION['ID_CLIENTE']= $id_cli;
			$cliente			   = $datos['NOMBRE_CLIENTE'].' '.$datos['APELLIDO_CLIENTE'];
			$nombre 			   = $datos['NOMBRE_CLIENTE'];
			$apellido 			   = $datos['APELLIDO_CLIENTE'];
			$_SESSION['NOMBRE_CLIENTE']=$cliente;
			$FECHA_REGISTRO        = $datos["FECHA_REGISTRO"];
			$AUTOR_REGISTRO_FK     = $datos["AUTOR_REGISTRO_FK"];
		}
	
	// datos de la encuesta
	$select = mysqli_query($conex,"SELECT DISTRIBUIDOR_COMPRA,CLASIFICACION_ODONTOLOGO,TIPO_ODONTOLOGO,ESPECIALIDAD_ODONTOLOGO FROM 3m_encuesta WHERE ID_CLIENTE_FK_5='$id_cli'");
	echo mysqli_error($conex);
	while($fila=(mysqli_fetch_array($select)))
	{
		$TIPO_ODONTOLOGO = $fila["TIPO_ODONTOLOGO"];
		$distribuidor=$fila['DISTRIBUIDOR_COMPRA'];
		$_SESSION['DISTRIBUIDOR_COMPRA']=$distribuidor;
		$CLASIFICACION_ODONTOLOGO=$fila['CLASIFICACION_ODONTOLOGO'];
		$ESPECIALIDAD_ODONTOLOGO=$fila['ESPECIALIDAD_ODONTOLOGO'];
	}
	
	 
	//consultar pedido
	$consulta=mysqli_query($conex," SELECT * FROM 3m_pedido AS p
	 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
	 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO
	 WHERE ID_CLIENTE_FK='$id_cli'");
	 echo mysqli_error($conex);
	 
	 //consultar detalle de la encuesta
	$consulta_encuesta=mysqli_query($conex,"SELECT * FROM 3m_encuesta WHERE ID_CLIENTE_FK_5='$id_cli'");
	 echo mysqli_error($conex);
		$consulta_gestiones = mysqli_query($conex," SELECT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,a.FECHA_PROXIMA_VISITA,
		 b.ID_CLIENTE,
		 a.ASESOR_GESTION,a.ID_ASESOR_GESTION,a.OBSERVACION_GESTION
		 FROM 3m_gestion AS a
		 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
		 WHERE ID_CLIENTE_FK=".$id_cli." 
		 ORDER BY a.FECHA_GESTION DESC;");
	

	//CONSULTAR FECHA DE LA ULTIMA GESTION 
      $consultar_fecha_gestion = mysqli_query($conex,"SELECT MAX(FECHA_GESTION)
	FROM 3m_gestion
	WHERE ID_CLIENTE_FK=".$id_cli);
		while($datos=(mysqli_fetch_array($consultar_fecha_gestion))){
			$fecha_gestion = $datos["MAX(FECHA_GESTION)"];
        }
    
    //CONSULTA PRODUCTOS USADOS DE ENCUESTA 3M
    $select = mysqli_query($conex,"SELECT DISTINCT(p.NOMBRE_PRODUCTO) AS NOMBRE_PRODUCTO, p.CATEGORIA, p.SUBCATEGORIA
	FROM 3m_productos_consumo_historial AS h
	INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO 
	WHERE h.ID_CLIENTE_FK='$id_cli'");
	
    
	
//$fecha_actual = date("Y-m-d H:i:s");

$fecha_actual= date("Y-m-d H:i:s");
	
	$fecha_inicial= new DateTime($fecha_gestion);
	$fecha_final = new DateTime($fecha_actual);

	$resta_fecha = $fecha_inicial->diff($fecha_final);
	$dias= $resta_fecha->format("%d");
	$hora= $resta_fecha->format("%H");
	$minuto= $resta_fecha->format("%I");
	$segundo= $resta_fecha->format("%S");
	
	if($dias  > 1  ){
		$tiempo_transcurrido = "Hace ".$dias." dias";
	}
	else if($dias ==1){
		$tiempo_transcurrido="Ayer";
	}
	
   else if($hora >0){
		$tiempo_transcurrido = "Hace ".$hora. "h ".$minuto."m";
	}
	else if($minuto >0){
		$tiempo_transcurrido = "Hace ".$minuto. " min";
	}
	else{
		$tiempo_transcurrido="Justo ahora";
    }
    

    $consulta_competencia = mysqli_query($conex,"SELECT B.id_competencia AS id_temp, A.id_competencia,A.categoria,A.producto_3m,A.descripcion_competencia
		 FROM 3m_categoria_competencia AS A
		INNER JOIN 3m_productos_competencia_temp AS B ON A.id_competencia = B.ID_PRODUCTO_COMPETENCIA
		WHERE ID_CLIENTE= ".$id_cli.";"); 
?>

 
<body>
<div id="dialog-message" title="Download complete">
	<br />
    <br />
	<fieldset style="border:1px solid #93271b; border-radius:10px; margin:auto auto;width:90%">
        <legend style="color:#93271b; font-weight:bold; text-transform:uppercase"><center>Informaci&Oacute;n Cliente</center>
       
        </legend>
        <table align="center" width="100%"  class="table table-striped">
            <tr style=" text-align: left;">
                <td class="tit">
                    Codigo Interno:
					
                </td>
                <td><?php
				if(empty($ID_CLIENTE)){
					$ID_CLIENTE=$_SESSION['ID_CLIENTE'];
					}
				   ?>
                    <input name="identifi" style="width:65%; height:25px;  border:none;" type="text" id="identifi" value="<?php echo $ID_CLIENTE; ?>" readonly="readonly"/>
<?php
	$consultar_cliente =mysqli_query($conex,"SELECT ID_CLIENTE FROM 3m_cliente WHERE IDENTIFICACION_CLIENTE = '".$IDENTIFICACION_CLIENTE."'");
		$cantidad_cliente = mysqli_num_rows($consultar_cliente);
		
		if($cantidad_cliente >=2){ ?>
			 <button class="btn btn-danger" id="eliminar_cliente" style="float:right;margin-right:10px;" data-id="<?php echo  $ID_CLIENTE?>" data-identificacion="<?php echo $IDENTIFICACION_CLIENTE; ?>">Eliminar Cliente</button>
                 
	<?php	}

 ?>                   
				     </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit" style="width:35%;">
                    Nombre Cliente:
                </td>
                <td>
                    <input name="nombre_negocio" style="width:90%; height:25px; border:none" type="text" id="nombre_negocio" value="<?php echo $cliente; ?>" readonly="readonly"/>
               </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Numero de Identificacion:
                </td>
                <td>
                    <input name="identifi" style="width:90%; height:25px; border:none" type="text" id="identifi" value="<?php echo $IDENTIFICACION_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Telefono:
                </td>
                <td>
                    <input name="telefono" style="width:90%; height:25px; border:none" type="text" id="telefono" value="<?php echo $TELEFONO_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Celular:
                </td>
                <td>
                    <input name="celular" style="width:90%; height:25px; border:none" type="text" id="celular" value="<?php echo $CELULAR_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Direcci&oacute;n:
                </td>
                <td>
                    <input name="direccion" style="width:90%; height:25px; border:none" type="text" id="direccion" value="<?php echo $DIRECCION_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr> 
            <tr style=" text-align: left;">
                <td class="tit">
                    Distribuidor:
                </td>
                <td>
                    <input name="distri" type="text" id="distri" style="width:90%; height:25px; border:none" value="<?php echo $distribuidor; ?>" readonly/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">Clasificaci&oacute;n:</td>
                <td>
                <input name="clasificacion" type="text" id="clasificacion" style="width:90%; height:25px; border:none" value="<?php echo $CLASIFICACION_ODONTOLOGO; ?>" readonly/>
                </td>
            </tr>
          <tr style=" text-align: left;">
                <td class="tit">Especialidad:</td>
                <td>
                <input name="especialidad" type="text" id="especialidad" style="width:90%; height:25px; border:none" value="<?php echo $ESPECIALIDAD_ODONTOLOGO; ?>" readonly/>
                </td>
            </tr>
               <tr style=" text-align: left;">
                <td class="tit">Fecha de Registro:</td>
                <td>
                <input name="fecha_registro" type="text" id="fecha_registro" style="width:90%; height:25px; border:none" value="<?php echo $FECHA_REGISTRO; ?>" readonly/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">Usuario quien registra:</td>
                <td>
                <input name="usuario_registro" type="text" id="usuario_registro" style="width:90%; height:25px; border:none" value="<?php echo $AUTOR_REGISTRO_FK; ?>" readonly/>
                </td>
            </tr>
			<tr>
			<td class="tit">&Uacute;ltima visita</td>
			<td>
			   <input name="dias_visita" type="text" id="dias_visita" style="width:90%; height:25px; border:none" value="<?php echo $tiempo_transcurrido; ?>" readonly/>
             </td>
			</tr>
                              
      </table>
	  </center>
  </fieldset>
    <br />
    <br />
	<?php
while($datos=(mysqli_fetch_array($consulta_encuesta)))
{
		$ID_ENCUESTA = $datos['ID_ENCUESTA'];
	$CANTIDAD_PACIENTES=$datos['CANTIDAD_PACIENTES'];
	$ESPECIALIDAD_ODONTOLOGO=$datos['ESPECIALIDAD_ODONTOLOGO'];
	$CLASIFICACION_ODONTOLOGO=$datos['CLASIFICACION_ODONTOLOGO'];
	$CASOS_NUEVOS_SEMANA= $datos['CASOS_NUEVOS_SEMANA'];
	$FECHA_ACTUALIZACION=$datos['FECHA_ACTUALIZACION'];
	$DIA_VISITA=$datos['DIA_VISITA'];
	$HORA_INI_VISITA=$datos['HORA_INI_VISITA'];
	$HORA_FIN_VISITA=$datos['HORA_FIN_VISITA'];
}


?>

<hr style="width:100%;border-bottom:red;">
<center>

<fieldset style="border:1px solid #93271b; border-radius:10px; margin:auto auto;width:90%">
        <legend style="color:#93271b; font-weight:bold; text-transform:uppercase"><center>Informaci&Oacute;n Ultima Encuesta Cliente</center>
        
        </legend>
        <table align="center" width="100%"  class="table table-striped">
            <tr style=" text-align: left;">
                <td width="54%" class="tit">
                    Especialidad Odontologo:
                </td>
                <td width="46%">
                    <span><?php echo $ESPECIALIDAD_ODONTOLOGO ?></span>
                </td>
            </tr>
            
	
			<tr style=" text-align: left;">
			<?php 
			if($TIPO_ODONTOLOGO==1){ ?>
				<td class="tit" style="width:45%;">
                    Cantidad Paciente Que Atiende:
                </td>
			<?php }
			else if($TIPO_ODONTOLOGO==2){ ?>
				<td class="tit" style="width:45%;">
                   N&uacute;mero de valoraciones mensuales
                </td>
		<?php	}
			?><td>
                    <span><?php echo $CANTIDAD_PACIENTES ?></span>
               </td>
			   <?php 
			   if($TIPO_ODONTOLOGO==2){ ?>
				<tr style=" text-align: left;">
                <td class="tit">
                    Casos nuevos por semana
                </td>
                <td>
                    <span><?php echo $CASOS_NUEVOS_SEMANA ?></span>
                </td>
            </tr>   
			  <?php  }
			   
			   ?>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Clasificaci&Oacute;n Odontologo:
                </td>
                <td>
                    <span><?php echo $CLASIFICACION_ODONTOLOGO ?></span>
                </td>
            </tr>
            
            <tr style=" text-align: left;">
                <td class="tit">
                    Fecha Modificaci&oacute;n:
                </td>
                <td>
                    <span><?php echo $FECHA_ACTUALIZACION ?></span>
                </td>
            </tr>
      </table>
</fieldset>
</center>
</br>
<center>
<input id="id_encuesta" type="hidden" value="<?php echo $ID_ENCUESTA; ?>" >
<div class="table table-responsive" style="width:90%;">
		<br />
	
<table style="width:100%;border:1px solid #000; margin:auto auto;" class="table table-hover" rules="all" id="tablas_usado">
        <tr>
        <th class='principal' colspan='5' style='border-radius: 15px 15px 0 0;'>PRODUCTOS 3M USADOS</th>
        </tr>
        <tr>
        <th class='TITULO'>CATEGORIA</th>
        <th class='TITULO'>SUBCATEGORIA</th>
        <th class='TITULO'>NOMBRE PRODUCTO</th>
    </tr>
    <?php
	
    while($dato=(mysqli_fetch_array($select)))
    {
    ?>
    <tr>
        <td><?PHP echo $dato["CATEGORIA"]?></td>
        <td><?PHP echo $dato['SUBCATEGORIA']?></td>
        <td><?PHP echo $dato["NOMBRE_PRODUCTO"]?> </td>
    </tr>
	<?PHP	
    }
    ?>
</table>
        <br />
	
<table style="width:100%;border:1px solid #000; margin:auto auto;" rules="all" class="table table-hover" id="tablas"  >
    <tr>
        <th class='principal' colspan='5' style='border-radius: 15px 15px 0 0;'>PRODUCTOS COMPETENCIA</th>
        </tr>
        <tr>
        <th class='TITULO'>CATEGORIA</th>
        <th class='TITULO'>SUBCATEGORIA</th>
        <th class='TITULO'>NOMBRE PRODUCTO</th>
    </tr>
    <?php
	
    while($dato=(mysqli_fetch_array($consulta_competencia)))
    {
    ?>
    <tr>
        <td><?PHP echo $dato["categoria"]?></td>
        <td><?PHP echo $dato['producto_3m']?></td>
        <td><?PHP echo $dato["descripcion_competencia"]?> </td>
    </tr>
	<?PHP	
    }
    ?>
</table>	
    </div>
</center>

<hr style="width:100%;border-bottom:red;">	
	
	
	<div class="table table-responsive" >
	<center>
 <table  class="table table-striped" rules="none" id="gestiones" style="width:90%">
 <thead>
	<tr>
		<th colspan='9' class="principal">GESTIONES</th>
	</tr>
	<tr>
		<th class="TITULO">FECHA GESTION</th>
		<th class="TITULO">ACCION</th>
        <th class="TITULO">TIPIFICACION</th>
        <th class="TITULO">SUBTIPIFICACION </th>
        <th class="TITULO">PROXIMA VISITA </th>
        <th class="TITULO">VISITADOR</th>
		<th class="TITULO">OBSERVACION</th>
	</tr>
</thead>
<tbody>	
    <?PHP
    while($dato_gestion=mysqli_fetch_array($consulta_gestiones))
	{
	?>
		<tr class="datos">
            <td><?php echo $dato_gestion["FECHA_GESTION"]?></td>
            <td><?php echo $dato_gestion["ACCION"]?></td>
            <td><?php echo $dato_gestion["TIPIFICACION_GESTION"]?></td>
            <td><?php echo $dato_gestion["SUB_TIPIFICACION"]?></td>
            <td><?php echo $dato_gestion["FECHA_PROXIMA_VISITA"]?></td>	
            <td><?php echo $dato_gestion["ASESOR_GESTION"]?></td>	
            <td><?php echo $dato_gestion["OBSERVACION_GESTION"]?></td>
            
		</tr>
	<?php 
	}
	?>                                                                                                                                                                                                                                                              
	</tbody>
</table>
</div>
<hr style="width:100%;border-bottom:red;">

	<div class="table table-responsive" >
	<center>
 <table  class="table table-striped" rules="none" id="pedidos" style="width:90%">
<thead>
	<tr>
		<th colspan='9' class="principal">PEDIDO(S)</th>
	</tr>
	<tr>
		<th class="TITULO">TOTAL PEDIDO</th>
        <th class="TITULO">DISTRIBUIDOR</th>
        <th class="TITULO">FECHA PEDIDO </th>
        <th class="TITULO">ESTADO PEDIDO </th>
        <th class="TITULO">TIPO PEDIDO</th>
		
        <th class="TITULO">VER</th>
	</tr>
	</thead>
	<tbody>
    <?PHP
    while($dato=mysqli_fetch_array($consulta))
	{
	?>
		<tr class="datos">
            <td><?php echo $dato["TOTAL_PEDIDO"]?></td>
            <td><?php echo $dato["DISTRIBUIR"]?></td>
            <td><?php echo $dato["FECHA_PEDIDO"]?></td>
            <td><?php echo $dato["ESTADO_PEDIDO"]?></td>
            
            <td><?php echo $dato["TIPO_PEDIDO"]?></td>
            <th>
                <a  href="javascript:ventanaSecundaria('../presentacion/consulta_pedidos_detalle_visita.php?x=<?php echo base64_encode($dato['ID_PEDIDO'])?>')" >
              <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="DETALLE PEDIDO"/>
                </a>                
            </th>	
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
</center>
</div>
	
</div>

    
