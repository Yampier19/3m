<?php
	include("../logica/session.php");
	error_reporting(0);
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<script src="https://unpkg.com/sweetalert2@7.1.3/dist/sweetalert2.all.js"></script>
<title>TRANSFERENCIAS</title>
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/transferencia.css" type="text/css" rel="stylesheet" />
<script src="js/jquery.js"></script>

<script>
function categoria()
{
	$.ajax(
	{
		url:'../presentacion/consulta_categoria_pedido.php',
		type: 'post',
		beforeSend: function () 
		{
			$("#categoria").attr('disabled', 'disabled');
			$("#subcategoria").attr('disabled', 'disabled');
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#categoria").removeAttr('disabled');
			$('#categoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}

function subcategoria()
{
	var categoria=$('#categoria').val();
	var usuario=$('#usuario').val();
	$.ajax(
	{
		url:'../presentacion/consulta_subcategoria_pedido.php',
		data:
		{
			categoria: categoria,
			usuario:usuario
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#subcategoria").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#subcategoria").removeAttr('disabled');
			$('#subcategoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function stock()
{
	var subcategoria=$('#subcategoria').val();
	var categoria=$('#categoria').val();
	
	$.ajax(
	{
		url:'../presentacion/consulta_stock.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#stock").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#stock").removeAttr('disabled');
			$('#stock').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function Descripcion()
{
	var subcategoria=$('#subcategoria').val();
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_descripcion_pedido.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#Descripcion").removeAttr('disabled');
			$('#Descripcion').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function Cantidad_forecast()
{
	var Descripcion=$('#Descripcion').val();

	$.ajax(
		{
			//url:'../presentacion/consulta_cantidad_forecast.php',
			data:{Descripcion:Descripcion},
			type: 'post',

			success: function(data)
			{
				$('#cantidad_forecast_inp').val(data);
			}
	});
}
function Precio()
{
	var subcategoria=$('#subcategoria').val();
	var categoria=$('#categoria').val();
	var Descripcion=$('#Descripcion').val();
	$.ajax(
	{
		url:'../presentacion/consulta_precio.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria,
			Descripcion:Descripcion
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#precio").attr('readonly', 'readonly');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			//$("#precio").removeAttr('disabled');
			//alert(data);
			$('#precio').val(data);
			$("#cantidad").removeAttr('disabled');
			$("#cargando").fadeOut([1000]);
		}
	})
}

</script>
<style>
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
<script>
function limpiar()
{
	settimeout(document.tranferencias.reset(),1000);
	return false;
}
$(document).ready(function()
{
	var height= 700;
	$('#cargando').css('height',height);
	var div_caragr=($("#cargando").height()/2);
	var div_cargar_ancho=($("#cargando").width()/2);
	var div_cargar_ancho_img=$("#img_cargando").width();
	var total=div_caragr;
	var total_ancho_img=(div_cargar_ancho-div_cargar_ancho_img)/2;
	$('#img_cargando').css('margin-top',total);
	
	
	categoria();
	
	$('#categoria').change(function()
	{
		subcategoria();
		$('#subcategoria').html('');
		$('#Descripcion').html('');
		$("#Descripcion").attr('disabled', 'disabled');
		$('#precio').val('');
	});
	$('#subcategoria').change(function()
	{
		Descripcion();
		$('#precio').val('');
	});
	$('#Descripcion').change(function()
	{
		Precio();
		Cantidad_forecast();
		$('#cantidad').val('');
		$('#costo_tranferencia').val('');
	});
	$('#cantidad').change(function()
	{
		var cant=$('#cantidad').val();
		var precio=$('#precio').val();
		if(cant!=0&&cant<100)
		{
			$("#cargando").fadeIn();
			total= cant*precio;
			$('#costo_tranferencia').val(total);
			$("#cargando").fadeOut([1000]);
		}
		else
		{
			alert("Cantidad invalida")
			$('#cantidad').val('');
			$('#cantidad').focus();
			return false;
		}
	});
	$('#guardar').click(function()
	{
		$('#pedidos').css('display','inline-block');
	});
	$('#confirmar').submit(function()
	{
	});
});
</script>

</head>
<?PHP
include('../logica/ingresar_pedido.php');
 $id_usuario=$id_usu;
 $ID_CLIENTE=$ID_CLIENTE;
 $NOMBRE_CLIENTE;
 $DISTRIBUIDOR;
include('../datos/conex_copia.php');
if($id_usu!=''&&$id_usu!='0'&&$DISTRIBUIDOR!='')
{
?>
<style>
.col-md-3{
	margin-top:5px;
	}
</style>

<body>
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESI&Oacute;N.
			</p>
			<br />
			<br />
			</center>
			</span>
				
			<?php }else{?>
<form id="tranferencias" name="tranferencias" action="../logica/guradar_pedido_temporal.php" method="post" style="width:100%;" onsubmit="return limpiar()">
	<div class="form-group" style="display:none">
      <input id="LAT1" name="LAT1" type="text" class="form-control"  />
      <input id="lon1" name="lon1" type="text" class="form-control" />
      <input name="id_cliente" id="id_cliente" type="text" style="display:" readonly="readonly"value="<?php echo $ID_CLIENTE ?>"/>
      <input name="distribuidor" id="distribuidor" type="text" readonly="readonly" class="form-control1" value="<?php echo $DISTRIBUIDOR ?>"/>
		  <input name="usuario" id="usuario" type="text" readonly="readonly" class="form-control1" value="<?php echo $id_usuario?>"/>
    </div>

	<header>
        <div class="container" id="div_header">
        	<center><h1>TRANSFERENCIA - PRODUCTOS</h1></center>
        </div>
    </header>    
    <br />
	  <div class="form-group">
          <div class="col-md-3">
              <label for="cliente">Categoria</label><span class="asterisco">*</span>
            </div>
               <div class="col-md-3">
              <select name="categoria" id="categoria" class="form-control" >
              </select>
          </div>
          
           <div class="col-md-3">
              <label for="cliente">Subcategoria</label><span class="asterisco">*</span>
            </div>
             <div class="col-md-3">
              <select name="subcategoria" id="subcategoria" class="form-control" >
              </select>
        	</div>
         </div>
    
     <div class="form-group">
           <div class="col-md-3">
            <label for="cliente">Descripci&oacute;n</label><span class="asterisco">*</span>
            </div>
                 <div class="col-md-3">
            <select name="Descripcion" id="Descripcion" class="form-control" >
            </select>
        </div>
        </div>
        <div class="form-group" style=" display:none">
          <div class="col-md-3">
              <label for="cliente">Stock</label><span class="asterisco">*</span>
           </div>
             <div class="col-md-3">
              <select name="stock" id="stock" class="form-control">
              </select>
        </div>
        </div>
        <div class="form-group">
        <div class="col-md-3">
            <label for="cliente">Precio</label><span class="asterisco">*</span>
        </div>
        <div class="col-md-3">
            <input type="text" name="precio" id="precio" class="form-control" readonly="readonly"/>
        </div>
    </div>
    
        <div class="form-group">
        <div class="col-md-3">
            <label for="cliente">Cantidad</label><span class="asterisco">*</span>
         </div>
         <div class="col-md-3">
            <input type="number" max="99" min="0" name="cantidad" id="cantidad" class="form-control" disabled/>
        </div>
       <div class="col-md-3">
            <label for="cliente">Valor transferencia</label><span class="asterisco">*</span>
            </div>
           <div class="col-md-3">
    <input type="text" name="costo_tranferencia" id="costo_tranferencia" class="form-control" readonly="readonly"/>
        </div>
    </div>
    <div class="seccionformularios" id="cargando">
    	<center>
			<img src="imagenes/cargar_sinfondo2.gif" id="img_cargando">
        </center>
    </div>
    <br />
	
         <div class="form-group">
            <input name="guardar" type="submit" id="guardar"  value="AGREGAR PRODUCTO" class="form-control btn" onClick="return validar(tranferencias);"/>
        </div>  
       
<!--        
<iframe class="ifram" src="../logica/ingresar_pedido.php" name="pedidos" id="pedidos" style="display:none"></iframe>-->
    </div>
  
    	<div class="table table-responsive">
            <table class="table table-striped" style="border:1px solid #666; font-size:10pt;" rules="all">
            <tr>
                <th colspan="4">PEDIDO</th>
                </tr>
                <tr>
                <th>NOMBRE PRODUCTO</th>
                <th>CANTIDAD</th>
                <th>VALOR</th>
                <th>ELIMINAR</th>
            </tr>
            <?php
            $consulta=mysqli_query($conex,"SELECT * FROM 3m_detalle_pedido_temp AS dpt
            INNER JOIN 3m_categoria AS c ON dpt.ID_CATEGORIA_FK_3=c.STOCK
            WHERE ID_CLIENTE_FK_3='$ID_CLIENTE' AND ID_USUARIO_FK_3='$id_usuario'");
			$nreg_deta=mysqli_num_rows($consulta);
            while($dato=mysqli_fetch_array($consulta))
            {
            ?>
            <tr>
                <td><?php echo $dato['DESCRIPCION']?></td>
                <td><?php echo $dato['CANTIDAD_PRODUCTO']?></td>
                <td><?php echo $dato['TOTAL_PEDIDO']?></td>
                <th><a href="../presentacion/form_transferencias_med.php?xcx=<?php echo base64_encode($dato['ID_PEDIDO'])?>"><img src="../presentacion/imagenes/no.png" /></a></th>
            </tr>
            <?php
            }
            ?>
            <tr>
            <?PHP
            $consulta_TOT=mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) AS 'TOTAL' FROM 3m_detalle_pedido_temp AS dpt
            INNER JOIN 3m_categoria AS c ON dpt.ID_CATEGORIA_FK_3=c.STOCK
            WHERE ID_CLIENTE_FK_3='$ID_CLIENTE' AND ID_USUARIO_FK_3='$id_usuario'");
			$nreg=mysqli_num_rows($consulta_TOT);
			?>
			<th colspan="2">TOTAL COMPRA:</th>
			<?php
            	while($consulta_total=mysqli_fetch_array($consulta_TOT))
				{
					$total=$consulta_total['TOTAL'];
				}
				if($total!='')
				{
					?>
						<td colspan="2">
							<?php echo $total ?>
                            <input value="<?php echo $total ?>" name="total_pedido" id="total_pedido" type="hidden"/>
                        </td>
					<?php
				}
				else
				{
				?>
					<td colspan="2">0</td>
				<?php
				}
				?>
            </tr>
            <tr>
            	<th colspan="4" style="text-align:left;">
            	<?php
				if($DISTRIBUIDOR!='')
				{
					if($DISTRIBUIDOR=='Yina Moncayo-Dental Palermo')
					{
						echo "PAGO DE CONTADO -5%: ".($total-($total*5)/100);
						echo "<br />";
						echo "PAGO DE A 30 DIAS -2%: ".($total-($total*2)/100);
					}
					if($DISTRIBUIDOR=='Carlos Henao-Dental Nader')
					{
						echo "PAGO DE CONTADO -4%: ".($total-($total*4)/100);
						echo "<br />";
						echo "PAGO DE A 30 DIAS -2%: ".($total-($total*2)/100);
					}
					if($DISTRIBUIDOR=='Paola Linares-Dental 83 Portafolio 3M ESPE')
					{
						echo "PAGO DE CONTADO -4%: ".($total-($total*4)/100);
					}
				}
				?>
                </td>
            </tr>
            </table>
        </div>
        
        
        
    
            <div class="form-group">
              <div class="col-md-3">
                <label for="cliente">Forma de Pago</label><span class="asterisco">*</span>
                </div>
                  <div class="col-md-3">
                <select name="forma_pago" id="forma_pago" class="form-control" >
                	<option value="">Elija...</option>
                    <option>Efectivo</option>
                    <option>Credito</option>
                </select>
            </div>
            
             <div class="col-md-3">
                <label for="cliente">Datafono</label><span class="asterisco">*</span>
                </div>
                  <div class="col-md-3">
              <select name="datafono" id="datafono" class="form-control">
                	<option value="">Elija...</option>
                    <option>SI</option>
                    <option>NO</option>
                </select>
            </div>
            </div>
          <div class="form-group">
            <div class="col-md-3">
                <label for="cliente">Tipo Pedido</label><span class="asterisco">*</span>
                </div>
                  <div class="col-md-3">
                <select name="tipo_pedido" id="tipo_pedido" class="form-control" >
                	<option value="">Elija...</option>
                    <option>Sugerido</option>
                    <option>Transferencia</option>
					<?php 
						$consulta =mysqli_query($conex,"
							 SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
					FROM 3m_dentales AS d
					INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
					WHERE ID_USUARIO='".$id_usu."' AND ID_DENTAL <> 5 ORDER BY NOMBRE_DENTAL ASC;");
					while($datos_ciudad =mysqli_fetch_array($consulta)){
						$ciudad = $datos_ciudad["CIUDAD"];			
					}?>
					
					<option>Transferencia Compartida</option>
					
                </select>
            </div>
            <div class="col-md-3">
                <label for="horario">Rango de Horario para la entrega</label><span class="asterisco">*</span>
                </div>
                  <div class="col-md-3">
                <select name="rango" id="rango" class="form-control" >
                	<option value="">Elija...</option>
                    <option>AM</option>
                    <option>PM</option>
                    <option>Todo el dia</option>
                </select>
            </div>
            
        </div>  
         <div class="form-group">
            <div class="col-md-3">
             <label for="observaciones">Observaciones / Recomendaciones </label><span class="asterisco">*</span>
            </div>
                 <div class="col-md-9">
                 <textarea class="form-control" name="observaciones" id="observaciones"></textarea>
                 </div>
          </div>
          <br />
          <div class="form-group">
        <label for="cliente">Distribuidor de compra</label><span class="asterisco">*</span>
        <br>
        <select name="distribuidor" id="distribuidor" class="form-control" required>
		     <option><?php echo $DISTRIBUIDOR;?></option>
         <?php 
                        $consulta =mysqli_query($conex,"
         SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
			FROM 3m_dentales AS d
			INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
			WHERE ID_USUARIO='".$id_usu."' AND ID_DENTAL <> 5 ORDER BY NOMBRE_DENTAL ASC;");
                 
            if($id_usu!=2){     
                  while($dato=mysqli_fetch_array($consulta)) { ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php } 
			   if($ciudad=='PASTO' || $ciudad=='BOGOTA' || $ciudad=='ANTIOQUIA' || $ciudad=='CALI' || $ciudad=='BARRANQUILLA' || $ciudad=='CARTAGENA' || $ciudad=='BUCARAMANGA' || $ciudad=='TUNJA' || $ciudad=='MANIZALES'){ ?>
				<option>Dental ALDENTAL</option>
			<?php }
			if ($id_usu==19 || $id_usu==27 || $id_usu==9){?>
            <option>Bracket Ltda-Andres Beltran</option>
			<?php }
                if ($id_usu==7){ ?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>
            <option>Bracket Ltda-Andres Beltran</option>
			<option>Luz Elena Sanchez-Casa Dental Sede Norte</option>
			<?php 
			}
			if($id_usu==19 ){//AINFANTES QUE MANEJA BOGOTA, VILLLAVICENCIO Y FLORENCIA CAQUETA?>
			<option>Dental Nader Villavicencio-Jhovanny Parra</option>	
			<option>Jose Alejo Rodriguez Barragan - Dental Jar</option>
            <option>Luz Karime Zanabria-Vertice S.A.S</option>
			<?php }
			if($id_usu==33 ){//dlozano ?>
            <option>ODONTOSOLUCIONES-MARIA ISABEL VILLABONA</option>
            <option>Luz Karime Zanabria-Vertice S.A.S</option>
            <?php }
			if($id_usu==31 ){//ALGARCIA MANEJA CIUDAD ARMENIA?>
            <option>Equidentales</option>  
            <?php }
            if($id_usu==23 ){//jneira ?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>  
            <?php }
            if($id_usu==9 ){//achacon ?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>  
            <?php }

			if($id_usu==30 ){//SACASTELLANOS QUE MANEJA BOGOTA Y VILLLAVICENCIO?>
			<option>Dental Nader Villavicencio-Jhovanny Parra</option>	
			<option>Luz Karime Zanabria-Vertice S.A.S</option>	
			<?php }

			if($id_usu==29){//fvanstrahlen ?>
			<option>Luz Karime Zanabria-Vertice S.A.S</option>	
			<?php }

////////////////////////
			if($id_usu==7){//SGONZALES ?>
			<option>Dental Nader Bogota</option>	
			<?php }
////////////////////////

			if($id_usu==26){ // usuario visita ?>
			<option>Faride Maestre-Alfa Representaciones dentales</option>					
			<option>Ana Bueno-Elegir Soluciones</option>					
			<?php }
			if($id_usu==16){?> 
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
			<?php }
			if($id_usu==21){?> 
			<option>Luz Elena Sanchez-Casa Dental Sede Norte</option>
			<option>Dental Nader Villavicencio-Jhovanny Parra</option> 
			<?php }
			if($id_usu==31){?> 
			<option>Yina Moncayo-Dental Palermo</option>
			<?php }
			if($id_usu==36){ // lguevara ?> 
            <option>Faiber Laverde-Dentales Y Dentales</option>
            <?php }
			if($id_usu==9){?> 
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
			<option>Luz Karime Zanabria-Vertice S.A.S</option>
			<?php }
						if($id_usu>0){ // arestrepo  $id_usu==37 ?> 
            <option>Bracket Ltda-Andres Beltran</option>
            <?php }
			if($id_usu==30){// SACASTELLANOS
				$consulta_dentales= mysqli_query($conex,"SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
				FROM 3m_dentales AS d
				INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
				WHERE u.CIUDAD in ('BOGOTA') GROUP BY ID_DENTAL ORDER BY NOMBRE_DENTAL ASC");
				 while($dato=mysqli_fetch_array($consulta_dentales)) { ?>
                                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                         <?php } 
			}
			}
			else if($id_usu==2){
				$consulta_dentales= mysqli_query($conex,"SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
				FROM 3m_dentales AS d
				INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
				GROUP BY ID_DENTAL ORDER BY NOMBRE_DENTAL ASC");
				 while($dato=mysqli_fetch_array($consulta_dentales)) { ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
			<?php } ?>
				<option>ODONTOSOLUCIONES-MARIA ISABEL VILLABONA</option>
			<?php
			}

			 //} ?>

		</select>
    </div>
          <br />


        <?php
		if($nreg_deta>0)
		{
			?>
            <div class="form-group">
            <input name="confirmar" type="submit" id="confirmar" formaction="../logica/confirmar_pedido.php" value="SOLICITAR PEDIDO" class="form-control btn" onClick="return validar2(tranferencias)"/>
        </div>
            <?php
		}
		?>
    </div>
    <br />
    <br />    
</form>
</div>
<?php } ?>
</body>
</html>
<script>
// JavaScript Document
function validar(tuformulario)
{
	var categoria=$("#categoria").val();
	if(categoria=='')
	{
		swal(
		  {title: 'La categoria esta vacia',
		  confirmButtonColor: '#BC3228'});
		$('#categoria').focus();
		return false;
	}
	var subcategoria=$("#subcategoria").val();
	if(subcategoria=='')
	{
		swal(
		  {title: 'La subcategoria esta vacia',
		  confirmButtonColor: '#BC3228'});
		$('#subcategoria').focus();
		return false;
	}
	var Descripcion=$("#Descripcion").val();
	if(Descripcion=='')
	{
		swal(
		  {title: 'La descripcion esta vacia',
		  confirmButtonColor: '#BC3228'});
		$('#Descripcion').focus();
		return false;
	}
	var precio=$("#precio").val();
	if(precio=='')
	{
		swal(
		  {title: 'El precio esta vacio',
		  confirmButtonColor: '#BC3228'});
		$('#precio').focus();
		return false;
	}
	var cantidad=$("#cantidad").val();
	if(cantidad=='')
	{
		swal(
		  {title: 'la cantidad esta vacia',
		  confirmButtonColor: '#BC3228'});
		$('#cantidad').focus();
		return false;
	}
	var costo_tranferencia=$("#costo_tranferencia").val();
	if(costo_tranferencia=='')
	{
		swal(
		  {title: 'El costo tranferencia esta vacia',
		  confirmButtonColor: '#BC3228'});
		$('#cantidad').focus();
		return false;
	}
		swal({
		  title: 'ESTA SEGURO(A) DE AGRGAR ESTE PRODUCTO',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar'
		}).then((result) => {
		  if (result.value) {
			document.getElementById("tranferencias").submit();
			//$('#tranferencias').submit();
			return (true);
		  }
		
		  
		})
	
	return (false);
}

function validar2(tuformulario)
{
	var forma_pago=$("#forma_pago").val(); 
	if(forma_pago=='')
	{
		swal(
		  {title: 'La forma de pago esta vacia',
		  confirmButtonColor: '#BC3228'})
		$('#forma_pago').focus();
		return false;
	}
	
	var datafono=$("#datafono").val();
	if(datafono=='')
	{
		swal(
		  {title: 'Datafono esta vacio',
		  confirmButtonColor: '#BC3228'});
		$('#datafono').focus();
		return false;
	}
	
	var tipo_pedido=$("#tipo_pedido").val();
	if(tipo_pedido=='')
	{
		swal(
		  {title: 'El tipo de pedido esta vacio',
		  confirmButtonColor: '#BC3228'});
		$('#tipo_pedido').focus();
		return false;
	}
	
	var rango=$("#rango").val();
	if(rango=='')
	{
		swal(
		  {title: 'El rango de horario esta vacio',
		  confirmButtonColor: '#BC3228'});
		$('#rango').focus();
		return false;
	}

}

</script>
<?php 
}
else
{
}
?>