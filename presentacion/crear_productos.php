<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/tablas.css" rel="stylesheet" />
<link href="css/bootstrap.css" rel="stylesheet" />
<title>Crear Producto</title>
<link rel="shortcut icon" href="imagenes/3m.png" />
<script src="js/jquery.js"></script>

<script>
function categoria()
{
	$.ajax(
	{
		url:'../presentacion/crear_categoria.php',
		type: 'post',
		beforeSend: function () 
		{
			$("#categoria").attr('disabled', 'disabled');
			$("#subcategoria").attr('disabled', 'disabled');
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#categoria").removeAttr('disabled');
			$('#categoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}

function subcategoria()
{
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/crea_sub_categoria.php',
		data:
		{
			categoria: categoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#subcategoria").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#subcategoria").removeAttr('disabled');
			$('#subcategoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
</script>

<script>
$(document).ready(function()
{
categoria();
	
	$('#categoria').change(function()
	{
		subcategoria();
		$('#subcategoria').html('');
	});
	});
</script>
</head>
<?PHP
include('../datos/conex.php');

?>
<body>
<form action="../logica/cambio_estado_pedido.php" method="post">

<table style="width:99%; margin:auto auto;" rules="none" class="table table-striped" >
	<tr>
		<th colspan='6' class="principal">NUEVOS PRODUCTOS</th>
	</tr>
    	
	<tr>
    <th>FAMILIA PRODUCTO</th>
    <td><div class="col-md-3">
    <select class="form-control" name="FAMILIA" required="true">
    		<option value="">ELIJA...</option>
          <?PHP
		  $SELECT = mysql_query("SELECT * FROM 3m_familia_productos ORDER BY DESCRIPCION_FAMILIA ASC",$conex);
          while($filas=(mysql_fetch_array($SELECT)))
		{
			echo "<option value=\"".$filas['ID_FAMILIA']."\">".utf8_encode($filas['DESCRIPCION_FAMILIA'])."</option>";
		} 
		?> 	    
    </select>
    </div></td>
    	</tr>
        
		<tr>
        	<th>CATEGORIA</th>
            	<td><div class="col-md-3">
              <select name="categoria" id="categoria" class="form-control" required="true">
              <option value="">ELIJA...</option>
              </select>
          </div></td>
    	</tr>
       	<tr> 
        	<th>SUB CATEGORIA </th>
            	<td><div class="col-md-3">
              <select name="subcategoria" id="subcategoria" class="form-control" required="true">
              <option value="">ELIJA...</option>
              </select>
        	</div></td>
     	</tr>
        <tr> 
            <th>STOCK</th>
            	<td><input name="stock" type="text"  required="true" class="form-control"/></td>
       </tr>
       <tr>
       		<th>DESCRIPCI&Oacute;N</th>
            <td><input name="descripcion" type="text"  required="true" class="form-control"/></td>
            </tr>
            <tr>
            <th>PRECIO</th>
            <td><input name="precio" type="number"  required="true" class="form-control" min="0"/></td>
          </tr>
          <tr>  
            <th>ESTADO</th>
            <td>
        
			<select class="form-control" name="estado" required="true">
            	<option value="">ELIJA...</option>
                <option value="0">Inactivo</option>
				<option value="1">Activo</option> 
			
            
            </select>
			</td>
       </tr> 
 
</table>
<br /><br />
<center>

<button class="btn btn-group-lg" name="crear_producto">Crear Productos</button>
</center>
<br />

<br />
   
</form>
</body>
</html>