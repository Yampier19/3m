<?php
	include("../logica/session.php");
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="css/tablas.css" rel="stylesheet" />
<link rel="stylesheet" href="../presentacion/css/tablas.css" />
<link rel="stylesheet" href="../presentacion/css/bootstrap.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>

	// PRODUCTOS
	function categoria()
{
	$.ajax(
	{
		url:'../presentacion/consulta_categoria_pedido.php',
		type: 'post',
		beforeSend: function () 
		{
			$("#categoria").attr('disabled', 'disabled');
			$("#subcategoria").attr('disabled', 'disabled');
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#categoria").removeAttr('disabled');
			$('#categoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}

function subcategoria()
{
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_subcategoria_pedido.php',
		data:
		{
			categoria: categoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#subcategoria").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#subcategoria").removeAttr('disabled');
			$('#subcategoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function stock()
{
	var subcategoria=$('#subcategoria').val();
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_stock.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#stock").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#stock").removeAttr('disabled');
			$('#stock').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function Descripcion()
{
	var subcategoria=$('#subcategoria').val();
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_descripcion_pedido.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#Descripcion").removeAttr('disabled');
			$('#Descripcion').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function Precio()
{
	var subcategoria=$('#subcategoria').val();
	var categoria=$('#categoria').val();
	var Descripcion=$('#Descripcion').val();
	$.ajax(
	{
		url:'../presentacion/consulta_precio.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria,
			Descripcion:Descripcion
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#precio").attr('readonly', 'readonly');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			//$("#precio").removeAttr('disabled');
			//alert(data);
			$('#precio').val(data);
			$("#cargando").fadeOut([1000]);
			$("#cantidad").removeAttr('disabled');
			
			console.log(data);
			console.log("success");
		},
		
                error: function (err) {
                    console.log(err);
                },
	})
}
		
	$(document).ready(function()
{
	var height= 700;
	$('#cargando').css('height',height);
	var div_caragr=($("#cargando").height()/2);
	var div_cargar_ancho=($("#cargando").width()/2);
	var div_cargar_ancho_img=$("#img_cargando").width();
	var total=div_caragr;
	var total_ancho_img=(div_cargar_ancho-div_cargar_ancho_img)/2;
	$('#img_cargando').css('margin-top',total);
	
	categoria();
	
	$('#categoria').change(function()
	{
		subcategoria();
		$('#subcategoria').html('');
		$('#Descripcion').html('');
		$("#Descripcion").attr('disabled', 'disabled');
		$('#precio').val('');
	});
	$('#subcategoria').change(function()
	{
		Descripcion();
		$('#precio').val('');
	});
	$('#Descripcion').change(function()
	{
		Precio();
		$('#cantidad').val('');
		$('#costo_tranferencia').val('');
	});
	$('#cantidad').change(function()
	{
		var cant=$('#cantidad').val();
		var precio=$('#precio').val();
		if(cant!=0&&cant<100)
		{
			$("#cargando").fadeIn();
			total= cant*precio;
			$('#costo_tranferencia').val(total);
			$("#cargando").fadeOut([1000]);
			$("#guardar").show();
		}
		else
		{
			alert("Cantidad invalida")
			$('#cantidad').val('');
			$('#cantidad').focus();
			return false;
		}
	});
	
});
</script>

</head>
<?PHP
include('../datos/conex.php');
//$ID_CLIENTE=base64_decode($x);
$ID_PEDIDO=base64_decode($x);
echo $ID_PEDIDO;
$consulta_pedido=mysql_query("SELECT * FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO
 WHERE ID_PEDIDO='$ID_PEDIDO'",$conex);
?>
<body>
<div class="container-fluid">

<table style="width:99%; margin:auto auto;" rules="none">
	<tr>
		<th colspan='6' class="principal">INFORMACION PEDIDO</th>
	</tr>
    <?PHP
    while($dato_pedido=mysql_fetch_array($consulta_pedido))
	{
		$CLIENTE = $dato_pedido["NOMBRE_CLIENTE"]." ".$dato_pedido["APELLIDO_CLIENTE"]; 
		$DISTRIBUIDOR 		=  $dato_pedido["DISTRIBUIR"];
		$FECHA_PEDIDO 		= $dato_pedido["FECHA_PEDIDO"];
		$ID_CLIENTE  		= $dato_pedido["ID_CLIENTE"];
		$ID_USUARIO_FK_2	= $dato_pedido["ID_USUARIO_FK"];
		$TIPO_PEDIDO 		= $dato_pedido["TIPO_PEDIDO"];
	?>
    
		<tr class="datos" style="border:1px solid #000;">
        	<th colspan="2">
            	NOMBRE CLIENTE                
            </th>
            <th colspan="4"><?php echo $CLIENTE?></th>
       </tr>
        <tr class="datos" style="border:1px solid #000;">
        	<th>
            	TOTAL PEDIDO
                <input type="hidden" name="id" id="id" value="<?php echo $dato_pedido["ID_PEDIDO"] ?>" />
                <input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $ID_CLIENTE ?>" />
            </th>
            <td><?php echo $dato_pedido["TOTAL_PEDIDO"]?></td>
            
            <th>FECHA PEDIDO </th>
            <td><?php echo $dato_pedido["FECHA_PEDIDO"]?></td>
            
            <th>DISTRIBUIDOR</th>
            <td><?php echo $dato_pedido["DISTRIBUIR"]?></td>
       </tr>
       <tr class="datos" style="border:1px solid #000;">
       		<th>CELULAR CLIENTE</th>
            <td><?php echo $dato_pedido["CELULAR_CLIENTE"]?></td>
            
            <th>TELEFONO FIJO</th>
            <td><?php echo $dato_pedido["TELEFONO_CLIENTE"]?></td>
            
            <th>DIRECCION CLIENTE</th>
            <td><?php echo $dato_pedido["DIRECCION_CLIENTE"]?></td>
       </tr> 
       <tr class="datos" style="border:1px solid #000;">     
            <th>USUARIO</th>
            <td style="text-align:center"><?php echo $dato_pedido["USER"]?></td>
            
            <th>ESTADO ACTUAL PEDIDO </th>
            <td>
				<?php echo $dato_pedido["ESTADO_PEDIDO"]?>
                <input type="hidden" value="<?php echo $dato_pedido["ESTADO_PEDIDO"] ?>" name="estado_producto_act" id="estado_producto_act" />
            </td>
            
            
            
		</tr>
	<?php 
		$total=$dato_pedido["TOTAL_PEDIDO"];
	}
	?>
</table>
<br />

<form action="../logica/cambio_estado_pedido.php" method="post">
<input name="id_pedido" 		type="hidden"  value="<?php echo $ID_PEDIDO  ?>"  />
<table style='width:99%;border:1px solid transparent; margin:auto auto;' rules='all'>
	<tr>
		<th colspan='4' class="principal" style="border-radius:0px;">DETALLES ENTREGA PEDIDO</th>
	</tr>
    <?php
	 $consultaEntrega=mysql_query(" 
 SELECT DISTINCT
d.RANGO_ENTREGA,d.OBSERVACIONES
FROM 3m_pedido AS p
INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON d.ID_PEDIDO_FK_2 = p.ID_PEDIDO
WHERE p.ID_PEDIDO= '$ID_PEDIDO' ",$conex);
 
	while($dato_entrega=mysql_fetch_array($consultaEntrega))
	{ 
	$ENTREGA = $dato_entrega["RANGO_ENTREGA"];
	$OBSERVACIONES = $dato_entrega["OBSERVACIONES"];
	?>
		 <tr class="datos" style="border:1px solid #000;">
       		<th width="40%">HORARIO ENTREGA</th>
            <td width="60%">
             <select name="rango" id="rango" class="form-control" >
                	<optgroup label="Actual">
                    <option value="<?php echo $dato_entrega["RANGO_ENTREGA"]?>"><?php echo $dato_entrega["RANGO_ENTREGA"]?></option>
                    </optgroup>
                    <optgroup label="Seleccione">
                         <option value="AM">AM</option>
                        <option value="PM">PM</option>
                        <option value="Todo el dia">Todo el d&iacute;a</option>
                    </optgroup>
                </select>
            
            </td>
          </tr>
          <tr class="datos" style="border:1px solid #000;">  
            <th>OBSERVACIONES / RECOMENDACIONES</th>
            <td>
			<textarea class="form-control" name="observacion"  ><?php echo $dato_entrega["OBSERVACIONES"]?>
            </textarea>
            </td>
            
       </tr> 
		<?php }
	 ?>
    </table>
    
<div class="col-md-12" style="text-align:right; padding:10px;">
  <button class="btn-primary" name="actualizar_detalle" id="actualizar_detalle">
  Actualizar Detalle
  </button>
  </div>
</form>
<br />
<table style='width:99%;border:1px solid transparent; margin:auto auto;text-align:left;' >
	<tr>
		<th colspan='5' class="principal" style="border-radius:0px;">DETALLES PEDIDO</th>
	</tr>
	<tr>
		<th class="TITULO">NOMBRE PRODUCTO</th>
		<th class="TITULO">CANTIDAD</th>
		<th class="TITULO">VALOR</th>
        <th class="TITULO" id="esta_titu">EDITAR</th>
        <th class="TITULO" id="esta_titu">ELIMINAR</th>
	</tr>
    <?php
	$ID_PEDIDO;
    $consulta=mysql_query("SELECT dp.ID_PEDIDO,c.ID_CATEGORIA, c.PRECIO_UNIDAD,c.STOCK,c.DESCRIPCION,dp.CANTIDAD_PRODUCTO,dp.TOTAL_PEDIDO
	FROM 3m_detalle_pedido AS dp
	INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=dp.ID_CLIENTE_FK_2
	INNER JOIN 3m_categoria AS c ON dp.ID_CATEGORIA_FK_2=c.STOCK
	INNER JOIN 3m_usuario AS u ON dp.ID_USUARIO_FK_2=u.ID_USUARIO
    WHERE ID_PEDIDO_FK_2='$ID_PEDIDO'",$conex);
	$CONT=0;
	while($dato=mysql_fetch_array($consulta))
	{
		$CONT=$CONT+1;
	?>
		<tr id="pedido_<?php echo $dato["ID_PEDIDO"]; ?>" >
            <td><?php echo utf8_encode($dato["DESCRIPCION"]);?></td>
            <td>
	<input value="<?php echo $dato["CANTIDAD_PRODUCTO"]?>" name="pedido_cantidad" id="pedido_cantidad_<?php echo $dato["ID_PEDIDO"]; ?>" type="number" min="1" class="form-control" style="width:100px;"/>
    </td>
            <td><?php echo  number_format($dato["TOTAL_PEDIDO"],0,",",".") ?></td>	
        
            <td>
                
            <button name="editar"  class="btn btn-default editar" style="background-color:transparent;"  id="pedido_<?php echo $dato["ID_PEDIDO"]; ?>" data-pedido="<?php echo $dato["ID_PEDIDO"]; ?>" data-principal =<?php echo $ID_PEDIDO; ?> data-precio="<?php echo $dato["PRECIO_UNIDAD"]; ?>" data-total="<?php  echo $total; ?>" data-totalproducto=<?php echo $dato["TOTAL_PEDIDO"]; ?> >
			 <img src="imagenes/lapiz.png" width="18" height="19" style="background-size:cover" title="EDITAR CANTIDAD PRODUCTO"/>
             </button>
             </td>
             <td>
                <button name="eliminar"  class="btn btn-default eliminar" style="background-color:transparent;"  id="pedido_<?php echo $dato["ID_PEDIDO"]; ?>" data-pedido="<?php echo $dato["ID_PEDIDO"]; ?>"  data-principal =<?php echo $ID_PEDIDO; ?> data-total="<?php  echo $total; ?>" data-totalproducto=<?php echo $dato["TOTAL_PEDIDO"]; ?>>
			 <img src="imagenes/no.png" width="18" height="19" style="background-size:cover" title="ELIMINAR PRODUCTO"/>
             </button>
             </td>
     
          		
		</tr>
	<?php 
	}
	?>
    
 
    <tr >
    	
		<?php
        if($total!='')
		{
			?>
            <th colspan="2" class="TITULO">VALOR TOTAL PEDIDO:
            </th>
            <th class="TITULO">
				<center><?php echo number_format($total,0,",",".");?></center>
			</th>
            <th colspan="2" class="TITULO"></th>
			<?php
		}
		else
		{
		?>
			<th colspan="4" class="TITULO">VALOR TOTAL PEDIDO: 0</th>
		<?php
		}
		?>
    </tr>
</table>
<br />
<br />
  

<div class="col-md-6" style="text-align:left;">
  
  </div>

<div class="col-md-6" style="text-align:right;">
  <button class="btn-primary agregar_producto" id="agregar_producto" >
  Agregar Producto
  </button>
  <form method="post" action="../logica/cambio_estado_pedido.php">
    <input name="id_pedido"    type="hidden"  value="<?php echo $ID_PEDIDO  ?>"  />
    <input name="ID_CLIENTE"   type="hidden"  value="<?php echo $ID_CLIENTE ?>" /> 
    <input name="distribuidor" type="hidden"  value="<?php echo $DISTRIBUIDOR; ?>"   />
    
    <input name="ID_USUARIO_FK_2"   type="hidden" value="<?php echo $ID_USUARIO_FK_2;?>"/>

  <button class="btn-danger" id="finalizar_edicion"  name="finalizar_edicion">
Finalizar edici&oacute;n
  </button>
  </form>
  </div>
  <br />
  <hr  style="width:100%;"/>
  <script>
  	jQuery('#agregar_producto').on('click', function(){ 
			
				jQuery("#tranferencias").show();
			});
  </script>


<form id="tranferencias" name="tranferencias" action="../logica/cambio_estado_pedido.php" method="post" hidden="hidden" style="width:100%; height:300px;" onsubmit="return limpiar()">


    <input name="id_pedido" 		type="hidden"  value="<?php echo $ID_PEDIDO  ?>"  />
    <input name="distribuidor"  	type="hidden"  value="<?php echo $DISTRIBUIDOR; ?>"   />
    <input name="FECHA_PEDIDO"  	type="hidden"  value="<?php  echo $FECHA_PEDIDO;?>"/>
    <input name="ID_CLIENTE"    	type="hidden"  value="<?php echo $ID_CLIENTE ?>" /> 
    <input name="TIPO_PEDIDO"   	type="hidden"  value="<?php echo $TIPO_PEDIDO; ?>">
    <input name="ENTREGA"       	type="hidden"  value="<?php  echo $ENTREGA?>" />
    <input name="OBSERVACIONES" 	type="hidden"  value="<?php  echo $OBSERVACIONES;?>" />
    <input name="ID_USUARIO_FK_2"   type="hidden" value="<?php echo $ID_USUARIO_FK_2;?>"/>
    <table style='width:99%;border:1px solid transparent; margin:auto auto;' rules='all'>
	<tr>
		<th colspan='4' class="principal" style="border-radius:0px;">INGRESAR PRODUCTO</th>
	</tr>
    </table>
 <div class="form-group" >
          <div class="col-md-4" style="padding:5px;">
              <label for="cliente">Categoria</label><span class="asterisco">*</span>
			<br />
              <select name="categoria" id="categoria" class="form-control" >
              </select>
          </div>
          
          
           <div class="col-md-2" style="padding:5px;">
              <label for="cliente">Subcategoria</label><span class="asterisco">*</span>
     <br />
              <select name="subcategoria" id="subcategoria" class="form-control" >
              </select>
        	</div>
            
 
           <div class="col-md-6"   style="padding:5px;" >
            <label for="cliente">Descripci&oacute;n</label><span class="asterisco">*</span>
          <br />
            <select name="Descripcion" id="Descripcion" class="form-control" >
            </select>
        </div>
        </div>
       
        <div class="form-group" >
        <div class="col-md-4" style="padding:5px;">
            <label for="cliente">Precio</label><span class="asterisco">*</span>
       <br />
            <input type="text" name="precio" id="precio" class="form-control" readonly="readonly"/>
         
        </div>
    </div>
    
        <div class="col-md-2"  style="padding:5px;">
            <label for="cliente">Cantidad</label><span class="asterisco">*</span>
        <br />
            <input type="number" max="99" min="0" name="cantidad" id="cantidad" class="form-control" disabled/>
        </div>
       <div class="col-md-6" style="padding:5px;">
            <label for="cliente">Valor tranferencia</label><span class="asterisco">*</span>
         <br />
    <input type="text" name="costo_tranferencia" id="costo_tranferencia" class="form-control" readonly="readonly"/>
    
        </div>
  
    <div class="seccionformularios" id="cargando">
    	<center>
			<img src="imagenes/cargar_sinfondo2.gif" id="img_cargando">
        </center>
    </div>
    <br />
      
         <div class="form-group">
         <div class="col-md-12" style="padding:5px;">
            <input name="guardar_producto" type="submit" id="guardar" value="AGREGAR PRODUCTO" class="btn-primary" style="display:none;width:100%;" />
        </div>
        </div>  
  
       </form>
  

</div>

</body>


<script>
jQuery(".eliminar").on("click", function(){
		var pedidodata = $(this).data();
		var id_pedido = pedidodata.pedido;
		var principal = pedidodata.principal;
		var total     = pedidodata.total;
		var total_producto = pedidodata.totalproducto;
		
		var mensaje = confirm("Desea eliminar el producto del pedido?");
	
		if (mensaje) {
				var dato = new FormData();
				dato.append('id_pedido', id_pedido);
				dato.append('principal', principal);
				dato.append('tipo', 1);
				dato.append('total', total);
				dato.append('total_producto', total_producto);
			
				jQuery.ajax({
					
					url			: '../logica/ajax.php', 
					type		: 'POST', 
					contentType	: false, 
					data		: dato, 
					processData	: false, 
					cache		: false,
					
				}).done(function(msg){			
			
						location.reload(true);	
				}); 
		}
		
		else {
	
		}

});

jQuery(".editar").on("click", function(){
		var pedidodata 	   = $(this).data();
		var id_pedido 	   = pedidodata.pedido;
		var principal 	   = pedidodata.principal;
		var precio_unidad  = pedidodata.precio;
		var total          = pedidodata.total;
		var total_producto = pedidodata.totalproducto;
		
		var pedido_cantidad = jQuery('#pedido_cantidad_'+id_pedido).val();
		
		if(pedido_cantidad==0){
			alert("Ingrese un valor mayor a 0 ");
			jQuery('#pedido_cantidad_'+id_pedido).focus();
		}else{
		
		var mensaje = confirm("Desea cambiar la cantidad del producto?");
		
		if(mensaje){
				var dato = new FormData();
				dato.append('id_pedido', id_pedido);
				dato.append('principal', principal);
				dato.append('precio_unidad', precio_unidad);
				dato.append('pedido_cantidad', pedido_cantidad);
				dato.append('total', total);
				dato.append('total_producto', total_producto);
				dato.append('tipo', 2);
			
				jQuery.ajax({
					
					url			: '../logica/ajax.php', 
					type		: 'POST', 
					contentType	: false, 
					data		: dato, 
					processData	: false, 
					cache		: false,
					
				}).done(function(msg){			
				
				location.reload(true);	
				}); 
			}
		}
	});
	</script>	
</html>