<?php
$ID_CLIENTE;
$id_usuario;
$DISTRIBUIDOR;
require_once('../datos/conex_copia.php');
header('Content-Type: text/html; charset=UTF-8');
mysqli_set_charset($conex,"utf8");
$fecha= date("Y-m-d");

$body1 = "";
$body2 = "";
$body3 = "";
$body4 = "";

$consulta_TOT=mysqli_query($conex,"SELECT SUM(dp.TOTAL_PEDIDO) AS 'TOTAL' 
FROM 3m_detalle_pedido AS dp
INNER JOIN 3m_pedido AS p ON p.ID_PEDIDO=dp.ID_PEDIDO_FK_2
WHERE dp.ID_PEDIDO_FK_2='$ID_PEDIDO'");
	
	$nreg=mysqli_num_rows($consulta_TOT);
	while($consulta_total=mysqli_fetch_array($consulta_TOT))
	{
		$total=$consulta_total['TOTAL'];
	}

$consulta_cliente = mysqli_query($conex,"SELECT cli.NOMBRE_CLIENTE, cli.APELLIDO_CLIENTE, cli.TIPO_IDENTIFICACION, cli.IDENTIFICACION_CLIENTE, cli.CELULAR_CLIENTE, cli.TELEFONO_CLIENTE, cli.DIRECCION_CLIENTE, cli.CIUDAD_CLIENTE,cli.EMAIL_CLIENTE, p.FORMA_PAGO, p.TIPO_PEDIDO, dp.RANGO_ENTREGA, dp.OBSERVACIONES, u.USER
 FROM 3m_detalle_pedido AS dp
INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=dp.ID_CLIENTE_FK_2
INNER JOIN 3m_categoria AS c ON dp.ID_CATEGORIA_FK_2=c.STOCK
INNER JOIN 3m_pedido AS p ON p.ID_PEDIDO=dp.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS u ON dp.ID_USUARIO_FK_2=u.ID_USUARIO
WHERE dp.ID_PEDIDO_FK_2='$ID_PEDIDO'");

echo mysqli_error($conex);
$nrego=mysqli_num_rows($consulta_cliente);

if($nrego > 0){
while ($fila1 = mysqli_fetch_array($consulta_cliente))
  { 
    	$CLIENTE       = $fila1['NOMBRE_CLIENTE'].' '.$fila1['APELLIDO_CLIENTE'];
  		$TIPO_IDENTIFICACION= $fila1['TIPO_IDENTIFICACION'].' '.$fila1['IDENTIFICACION_CLIENTE'];
		$CELULAR_CLIENTE 	= $fila1['CELULAR_CLIENTE'];
		$TELEFONO_CLIENTE 	= $fila1['TELEFONO_CLIENTE'];
		$DIRECCION_CLIENTE  = $fila1['DIRECCION_CLIENTE'];
		$CIUDAD_CLIENTE 	= $fila1['CIUDAD_CLIENTE'];
		$FORMA_PAGO 		= $fila1['FORMA_PAGO'];
		$tipo_pedido        = $fila1['TIPO_PEDIDO'];
		$RANGO_ENTREGA 		= $fila1['RANGO_ENTREGA'];
		$OBSERVACIONES		= $fila1['OBSERVACIONES'];	
		$USER 		  		= $fila1['USER'];
                $CORREO_CLIENTE     = $fila1["EMAIL_CLIENTE"];
		
  }
 
 
$body1 = "
<b>Distribuidor:</b> ".$DISTRIBUIDOR."

<h3>Datos Del Cliente</h3>
<table width='100%'>
  <tr>
  <th width='24%' style='text-align:left'>
  DOCUMENTO DE IDENTIFICACI&Oacute;N:
  </th>
        <td width='30%' >
       	".$TIPO_IDENTIFICACION."
        </td>
  <th width='14%' style='text-align:left'>
  NOMBRE CLIENTE:
  </th>
        <td width='32%' >
      	".$CLIENTE."
        </td>
  </tr>
     <tr>
     <th style='text-align:left'>
     TELEFONO CELULAR:
     </th>
        <td>
		".$CELULAR_CLIENTE."
        </td>
         <th style='text-align:left'>TELEFONO FIJO:</th>
        <td>
      	".$TELEFONO_CLIENTE."
        </td>
  </tr>
  <tr>
        <th style='text-align:left'>CORREO ELECTRONICO:</th>
        <td colspan='3'>".$CORREO_CLIENTE."</td>
</tr>
         <tr>
        <th style='text-align:left'>DIRECCION CLIENTE:</th>
 <td>
     	".$DIRECCION_CLIENTE."
		</td>
 
        <th style='text-align:left'>CIUDAD:</th>
        <td>
		".$CIUDAD_CLIENTE."
		</td>
        </tr>
        <tr>
           <th style='text-align:left'>FORMA DE PAGO:</th>
      <td>
      ".$FORMA_PAGO."
      </td>
      <th style='text-align:left'>TIPO PEDIDO:</th>
      <td>
      ".$tipo_pedido."
      </td>
    </tr>
    <tr>
           <th style='text-align:left'>HORARIO ENTREGA:</th>
      <td>
      ".$RANGO_ENTREGA."
      </td>
      <th style='text-align:left'>USUARIO:</th>
      <td>
      ".$USER."
      </td>
    </tr>
    <tr>
      <th colspan='1' style='text-align:left'>OBSERVACIONES:</th>
      <td colspan='3'>
      ".$OBSERVACIONES."
	  </td>
	  </tr>
   </table><br /><br />";
    
	
	
$body2 = "<h3>Detalle Del Pedido</h3>
<table style='width:99%;border:1px solid #000; margin:auto auto;' rules='all'>
	<tr>
		<th>NOMBRE PRODUCTO</th>
		<th>CANTIDAD</th>
		<th>VALOR</th>

	</tr>";
	
}


	$consulta_pedi=mysqli_query($conex,"SELECT C.ID_CATEGORIA, B.ID_CATEGORIA_FK_2, B.TOTAL_PEDIDO, B.CANTIDAD_PRODUCTO,C.DESCRIPCION
 FROM 3m_detalle_pedido AS B
INNER JOIN 3m_categoria AS C ON B.ID_CATEGORIA_FK_2=C.STOCK
INNER JOIN 3m_pedido AS p ON p.ID_PEDIDO=B.ID_PEDIDO_FK_2
WHERE B.ID_PEDIDO_FK_2='$ID_PEDIDO'");


if (empty($consulta_pedi)){
	echo "El pedido esta vacio";
	}else{				

				while($dato=mysqli_fetch_array($consulta_pedi))
				{
	
	$body3.= "	
					<tr>
					<td>".$dato['DESCRIPCION']."</td>
					<td>".$dato['CANTIDAD_PRODUCTO']."</td>
					<td>".$dato['TOTAL_PEDIDO']."</td>
		
					</tr>"; 
				
				}

	$body4 = "
	<tr>
			<th colspan='2'>TOTAL COMPRA:</th>
					<td colspan='4'>
						".$total."
					</td>
  </tr>
            
</table>";
        $body5="<br> <b>POR FAVOR CONFIRMAR CORREO RECIBIDO A:  programacion@peoplecontact.cc</b>";
	}




$BODY_TODO = $body1.$body2.$body3.$body4.$body5;
echo $BODY_TODO;


if($tipo_pedido=='Transferencia')
{
	
	if($DISTRIBUIDOR=='Paola Linares-Dental 83 Portafolio 3M ESPE')
	{//1
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc,jenny.ramirez@dental83.com,paola.linares@dental83.com", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Claudia Esperanza Beltran-Dentales Padilla')
	{//2
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "ventas1@dentalespadilla.com,ventasalmacen@dentalespadilla.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Yina Moncayo-Dental Palermo')
	{// 3
	  $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "dentalespalermo@outlook.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	  echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Dental Nader Bogota')
	{//4
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "comercial@dentalnader.com,facturacion@dentalnader.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Bracket Ltda-Andres Beltran')
	{//5
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "gerencia@bracket.com.co,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Dental Nader Villavicencio-Jhovanny Parra')
	{//6
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "villavicencio@dentalnader.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Sandra Patricia Lopez-Casa Dental Sede Sur')
	{//7
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "sandrap.lopez@casadentalltda.com,patricia.velasquez@casadentalltda.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Angie Franco-Casa Dental Sede Norte')
	{//8
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "d.ramirez@casadentalltda.com,n.argote@casadentalltda.com,patricia.velasquez@casadentalltda.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Adriana Meneses-Casa Odontologica Importadora Y Comercializadora SAS')
	{//9
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "asesoradrico@hotmail.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Alex Basurto-Dental ORBIDENTAL')
	{//10
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "orbidental@yahoo.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Marleny Pinto-Dental MEGADENTALES')
	{//11
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "ventas2@megadentales.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Juan Pablo Bustamante-Dental ALDENTAL')
	{//12
		//dental1ventas2@aldentalsa.com,
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "juanlazaro842@hotmail.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Paula Andrea Restrepo-Dentales Maria T')
	{//13
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "dentalesmariate@gmail.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Maryory Pareja-Dentales HV SAS')
	{//14
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "dentaleshv@hotmail.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Juan Carlos Sosa-Dental DENTALES ANTIOQUIA')
	{//15
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "info@dentalesantioquia.com.co,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Juan David Gonzales-Dental Market')
	{//16
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "gerencia@dentalesmarket.com,info@dentalesmarket.com,dcomercial@dentalesmarket.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Alicia Torres-Dental Alfor')
	{//18
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "compras@dentalalfor.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Faiber Laverde-Dentales Y Dentales')
	{//19
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "adm.dentalesydentales@gmail.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	
	else if($DISTRIBUIDOR=='Angela Munioz-Dental ALDENTAL')
	{//20
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "dental1ventas2@aldentalsa.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	
	else if($DISTRIBUIDOR=='Jhovany Manrique-Dental MEGADENTALES')
	{//21
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "ventas2@megadentales.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	
	else if($DISTRIBUIDOR=='Carlos Henao-Dental Nader')
	{//22
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "comercial@dentalnader.com,facturacion@dentalnader.com,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
		echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	
	
	else if($DISTRIBUIDOR=='Faride Maestre-Alfa Representaciones dentales')
	{//23
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "faride@adental.com.co,coordinador3m@peoplecontact.cc,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else if($DISTRIBUIDOR=='Luz Karime Zanabria-Vertice S.A.S')
	{//24
	 $mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "?vertice.luzkarime@gmail.com,vertice.milena@gmail.com,seguimiento3m@peoplecontact.cc,programacion@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO,$headers);
	 echo "<b> Enviado a: ".$DISTRIBUIDOR."</b><br>";
	}
	else{
		$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "programacion@peoplecontact.cc", "Solicitud pedido Falla Distribuidor- ".$fecha,$BODY_TODO,$headers);
	}
	
	if($mailer->send()){
		echo "<b>Correo enviado</b>";
	}else{
		echo "<b>Ha ocurrido un error al momento de enviar el correo</b>";
	}
	
}

if($tipo_pedido=='Sugerido')
{

	$mailer = new AttachMailer("pedidos_3m@encontactopeoplemarketing.com", "seguimiento3m@peoplecontact.cc,coordinador3m@peoplecontact.cc,programacion@peoplecontact.cc", "Pedido Sugerido- ".$fecha,$BODY_TODO,$headers);
	
	if($mailer->send()){
		echo "<b>Correo enviado</b>";
	}else{
		echo "<b>Ha ocurrido un error al momento de enviar el correo</b>";
	}
	
}

	
?>