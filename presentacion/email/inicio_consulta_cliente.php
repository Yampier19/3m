<?php
	include("../logica/session.php");
	error_reporting(0);
	header("Content-Type: text/html;charset=utf-8");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CLIENTE</title>
<link href="../presentacion/css/menu_visitas.css" type="text/css" rel="stylesheet" />
<link href="css/menu_visitas.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="../presentacion/fonts.css" />
<link href="css/bootstrap.css" rel="stylesheet" />

<link href="../presentacion/css/encuenta.css" type="text/css" rel="stylesheet" />
<script src="https://unpkg.com/sweetalert2@7.1.3/dist/sweetalert2.all.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script src="../presentacion/js/jquery.js"></script>
</head>
<style>
input
{
	line-height:2%;
	width:90.5%;
}
select
{
	line-height:15%;
	width:99%;
}
.form-control[disabled],.form-control[readonly],fieldset[disabled] .form-control
{
	background-color:#eee;
	opacity:1;
	cursor:not-allowed;
}
.form-control
{
	display:block;
	padding:6px 12px;
	line-height:1.42857143;
	color:#555;
	background-color:#fff;
	background-image:none;
	border:1px solid #ccc;
	border-radius:4px;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	font-size:90%;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
.btn:focus,
.btn.focus {
  color: #fff;
  background-color: #286090;
  border-color: #122b40;
}
.btn:hover {
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active,
.btn.active,
.open > .dropdown-toggle.btn{
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active:hover,
.btn.active:hover,
.open > .dropdown-toggle.btn:hover,
.btn:active:focus,
.btn.active:focus,
.open > .dropdown-toggle.btn:focus,
.btn:active.focus,
.btn.active.focus,
.open > .dropdown-toggle.btn.focus {
  color: #fff;
  background-color: #204d74;
  border-color: #122b40;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
.icon_salir
{
	background-image:url(imagenes/atras.png);
	background-size:cover;
	background-repeat:no-repeat;
}
.aviso3 
{
	font-size: 130%;
	font-weight: bold;
	color: #11a9e3;
	text-transform:uppercase;
	/*font-family: "Trebuchet MS";
	font-family:"Gill Sans MT";
	border-radius:10px;
	background: #11a9e3;*/
	background-color:transparent;
	text-align: center;
	padding:10px;
}
.form-control1 {	background-color:#eee;
	opacity:1;
	cursor:not-allowed;
}
.form-control1 {	background-color:#eee;
	opacity:1;
	cursor:not-allowed;
}
.form-control1 {	display:block;
	padding:6px 12px;
	line-height:1.42857143;
	color:#555;
	background-color:#fff;
	background-image:none;
	border:1px solid #ccc;
	border-radius:4px;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	font-size:90%;
}
table a{
	color:black;
	}
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>



<script>
/*
function ingresar_gestion()
{
	var latitud=$('#LAT').val();
	var longitud=$('#lon').val();
	var id_cliente=$('#id_cliente').val();
	var resultado=$('#resultado_text').val();
	var sub_tipificacion=$('#sub_tipificacion').val();
	var visita_no_efectiva=$('.no_efectiva_subtipificacion').val();
	var observacion=$('#observacion').val();
	var proxima_visita=$('#proxima_visita').val();
	
	
	
	
	
	if(proxima_visita==''){
		alert('Seleccione la fecha de proxima visita');
		return false;
	}
	if(observacion==''){
		alert('Ingrese una observacion');
		return false;
	}
	
	var url = '../presentacion/ingresar_gestion.php';
	$.ajax(
	{
		url:url,
		data:
		{
			observacion:observacion,
			latitud: latitud,
			longitud:longitud,
			id_cliente:id_cliente,
			resultado:resultado,
			sub_tipificacion:sub_tipificacion,
			proxima_visita:proxima_visita
		},
		type: 'post',
		success: function(data)
		{
			//alert(data);
			ingresar_imagenes_gestion();
			var resultado=$('#resultado_text').val();
			if(resultado=='EFECTIVA')
			{
				$("#div_menu").css('display','block');
				$('.visita_no_efectiva').css('display','none');
				$('#mas').css('display','none');
				$("#resultado_text option:eq(0)").attr("selected", "selected");
				$('#compro').val('1');
				$('#resultado_div').css('display','none');
				$('#proxima_visita').css('display','none');
			}
			if(resultado=='NO EFECTIVA')
			{
				$('#resultado_div').html(data);
				$('#visita_no_efectiva').css('display','block');
				$('#mas').css('display','none');
				$('#GUARDAR').css('display','none');
				$('#proxima_visita').css('display','none');
				$('.proxima_visita').css('display','none');
			
			}
			if(resultado=='TELEVENTA')
			{
				alert(resultado+'este es el resultado arriba');
				$('#resultado_div').html(data);
				$('.visita_no_efectiva').css('display','none');
				$('#mas').css('display','none');
				$('#GUARDAR').css('display','none');
				$('#proxima_visita').css('display','none');
				$('.proxima_visita').css('display','none');
			}
		}
	})
}*/
function ingresar_imagenes_gestion()
{
	var inputFileImage = document.getElementById("fachada");
	var file = inputFileImage.files[0];
	var data = new FormData();
	data.append('archivo',file);
	var url = '../presentacion/ingresar_imagen.php';
	$.ajax
	({
		url:url,
		type:"POST",
		contentType:false,
		data:data,
		processData:false,
		cache:false
	});
}
var height= window.innerHeight-10;/*tamaño ventana*/
var ancho=window.innerWidth;

$(document).ready(function()
{
	/*$('#GUARDAR').click(function()
	{
		//ingresar_gestion();
	});*/
	
	function tipificacion_efectiva()
	{
	
			$("#encuesta_consumo").css('display','block');
	}
	
	
	function resultado()
	{
		var resultado=$('#resultado_text').val();
		var encuesta=$('#encuesta').val();
		
		
		var copia_resultado=$('#copia_resultado').val(resultado);
		
		if(resultado=='EFECTIVA')
		{
			
			var distribuidor=$('#distribuidor').val();
			if(distribuidor!='')
			{
			$("#mas").css('display','block');
			$('.visita_efectiva').css('display','block');
			$("#encuesta_consumo").css('display','none');
			$('.fecha_proxima_visita').css('display','block');
			$(".visita_observaciones").css('display','block');
			$('.visita_no_efectiva').css('display','none');
			$("#televenta").css('display','none');
			$('#boton_no_efectiva').css('display','none');
			}
			else if(distribuidor=='')
			{
			swal(
  				{title: 'Ingrese el Distribuidor',
  				confirmButtonColor: '#BC3228'});
			$("#mas").css('display','none');
			$('#visita_no_efectiva').css('display','none');
			$('.tipificacion_no_efectiva').css('display','none');
			$('.fecha_proxima_visita').css('display','none');
			$('#boton_no_efectiva').css('display','none');
			$(".visita_observaciones").css('display','none');
			}
			
			//$('#boton_encuesta').css('display','block');
			obtener();
		}
		
		
		else if(resultado=='NO EFECTIVA'){
			
			$("#mas").css('display','block');
			$("#televenta").css('display','none');
			$('.visita_efectiva').css('display','none');
			$('#visita_no_efectiva').css('display','block');
			$('.tipificacion_no_efectiva').css('display','block');
			$('.fecha_proxima_visita').css('display','block');
			$('#boton_no_efectiva').css('display','block');
			$('#boton_encuesta').css('display','none');
			$("#encuesta_consumo").css('display','none');
			$(".visita_observaciones").css('display','block');
			$("#efectiva_sin_consumo").css('display','none');
			$("#boton_evento").css('display','none');
			
			
			obtener();
		}
		else if(resultado =='VENTA EN EVENTO'){
				
				var distribuidor=$('#distribuidor').val();
			if(distribuidor!='')
			{
				$("#televenta").css('display','none');
				$('.fecha_proxima_visita').css('display','block');
				$(".visita_observaciones").css('display','block');
				$('#visita_no_efectiva').css('display','none');
				$('#boton_no_efectiva').css('display','none');
				$("#efectiva_sin_consumo").css('display','none');
				$('.visita_efectiva').css('display','none');
				$("#mas").css('display','none');
				if(encuesta==1){
					$("#encuesta_consumo").css('display','block');
					$('#boton_encuesta').css('display','block');
				}
				else{
					$('#boton_encuesta').css('display','none');
					$("#encuesta_consumo").css('display','none');
					$('#boton_evento').css('display','block'); 
					}
			}
			else {
				swal(
  					{title: 'Ingrese el Distribuidor',
  					confirmButtonColor: '#BC3228'});
				$("#mas").css('display','none');
				$('#visita_no_efectiva').css('display','none');
				$('.tipificacion_no_efectiva').css('display','none');
				$('.fecha_proxima_visita').css('display','none');
				$('#boton_no_efectiva').css('display','none');
				$(".visita_observaciones").css('display','none'); 
				$("#televenta").css('display','none');
				}
			}
		
			
			
		 else if(resultado!='EFECTIVA'&&resultado!='NO EFECTIVA'&&resultado!='TELEVENTA'){
		
			$("#mas").css('display','none');
			$('.visita_efectiva').css('display','none');
			$('.visita_no_efectiva').css('display','none');
			$('.fecha_proxima_visita').css('display','none');
			$("#encuesta_consumo").css('display','none');
			$("#televenta").css('display','none');
			$("#efectiva_sin_consumo").css('display','none');
			$("#boton_evento").css('display','none');
		}
		

		
	else if(resultado=='TELEVENTA'){
		
			var distribuidor=$('#distribuidor').val();
			if(distribuidor!='')
			{
					$("#mas").css('display','none');
				
				$("#boton_evento").css('display','none');			
				
				if(encuesta==1){
					$("#encuesta_consumo").css('display','block');
					$("#televenta").css('display','none');
					$('#boton_encuesta').css('display','block');
					}
				
				
				else {
					$("#televenta").css('display','block');
					$('.visita_efectiva').css('display','none');
					$('#boton_no_efectiva').css('display','block');
					$('#boton_encuesta').css('display','none');
					$("#encuesta_consumo").css('display','none');
					}
				$("#boton_evento").css('display','none');	
				$('.visita_efectiva').css('display','none');
				$('.visita_observaciones').css('display','block');
				$('.tipificacion_no_efectiva').css('display','none');
				$('.fecha_proxima_visita').css('display','block');
				$('#boton_no_efectiva').css('display','none');
			}
			else if(distribuidor=='')
				{
				swal(
  					{title: 'Ingrese el Distribuidor',
  					confirmButtonColor: '#BC3228'});
				$("#boton_evento").css('display','none');
				$("#mas").css('display','none');
				 $('#visita_no_efectiva').css('display','none');
				$('.tipificacion_no_efectiva').css('display','none');
				$('.fecha_proxima_visita').css('display','none');
				$('#boton_no_efectiva').css('display','none');
				$(".visita_observaciones").css('display','none'); 
				$("#televenta").css('display','none');
			}
	
			obtener();
			
			
			}		
		
	}

	
	function tipificacion_efectiva(){
		var encuesta=$('#encuesta').val();
		var sub_tipificacion=$('.sub_tipificacion_efectiva').val();
		
		 if(sub_tipificacion=='ENCUESTA DE CONSUMO' || sub_tipificacion=='ENCUESTA DE CONSUMO Y TRANSFERENCIA'){
			 	$('#boton_encuesta').css('display','block');
				$("#encuesta_consumo").css('display','block');
				$(".visita_observaciones").css('display','block');
				$("#televenta").css('display','none');
				$("#boton_evento").css('display','none');
				$("#efectiva_sin_consumo").css('display','none');
		}
		else if(sub_tipificacion=='DEMOSTRACION' || sub_tipificacion=='LANZAMIENTO DE PRODUCTO NUEVO'|| sub_tipificacion=='VENTA DE MANTENIMIENTO' || sub_tipificacion=='SEGUIMIENTO'){
				if(encuesta==1){
					$('#boton_encuesta').css('display','block');
				$("#encuesta_consumo").css('display','block');
				$(".visita_observaciones").css('display','block');
				$("#boton_evento").css('display','none');
				$("#televenta").css('display','none');				
				}else{
					$('#boton_encuesta').css('display','none');
					$("#encuesta_consumo").css('display','none');
					$("#efectiva_sin_consumo").css('display','block');
				    $(".visita_observaciones").css('display','block');
					$("#televenta").css('display','none');
					$("#boton_evento").css('display','none');
			    }
			}
		 
		}
	
	$('#resultado_text').change(function()
	{
		resultado();
	});
	
	$('.sub_tipificacion_efectiva').change(function()
	{
		tipificacion_efectiva();
	});
	
	
	
	if(ancho<1001)
	{
		$('#info').css('height',height);
		var tama_menu=$("#div_menu").height();
		tama_menu=tama_menu*(-1);
		$('#info').css('margin-bottom',tama_menu);
		$("#info").animate({marginTop:tama_menu},1);
		
		$('#div_menu li').click(function()
		{
			$("#div_menu").animate({marginLeft:'-120%'},500);
			var tama_menu=$("#div_menu").height();
			tama_menu=tama_menu*(-1);
			$("#info").animate({marginTop:tama_menu},10);
			$('#info').toggle(1000);
			
		});
		$('#menu_bar').click(function()
		{
			$("#div_menu").animate({marginLeft:'0'},500);
			$('#info').fadeOut(1000);
			$("#div_menu").css('display','block');
			var compro=$('#compro').val();
			if(compro=='1')
			{
				$("#div_menu").css('display','block');
				$("#link2").css('display','block');
				if(distribuidor!='')
				{
					$("#link3").css('display','block');
				}
				else
				{
					$("#link3").css('display','none');
				}
			}
			if(compro!='1')
			{
				resultado();
			}
			
		});
	}
	else
	{
		$('#info').css('height',height);
		$('#info').css('display','block');
	}
});

function obtener()
{
	/*maximumAge: pedir que se cambia a cierto tiempo
	  timeaout:10000, maximumAge:600000 
	*/
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.getCurrentPosition(mostrar_posicion,gestion_errores,parametros);
	
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	/*alert(exactitud);*/
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;
	document.getElementById("LAT1").value=latitud;
	document.getElementById("lon1").value=longitud;
	
}
function gestion_errores(error)
{
	//alert("ha ocurrido un error "+error.code +" "+error.massage);
	if(error.code==1)
	{
		//alert("debes permitir el uso de la geolocalizacion en tu navegador");
		//window.onload = window.top.location.href = "https://encontactopeoplemarketing.com/3M/";
	}
}

	
</script>
<?php
include('../datos/conex.php');
$string_intro = getenv("QUERY_STRING"); 
parse_str($string_intro);
	$transferencia = $_REQUEST['transferencia'];
	if(empty($x)){
		$x=$_REQUEST["x"];
		}
		
$id_cli=base64_decode($x);


 if(empty($id_cli)){
	$id_cli=$ID_CLIENTE;
	 }

$consulta=mysql_query("SELECT * FROM 3m_cliente WHERE ID_CLIENTE='$id_cli'",$conex);
echo mysql_error($conex);
while($datos=(mysql_fetch_array($consulta)))
{
	$id_cli                 =$datos['ID_CLIENTE'];
	$IDENTIFICACION_CLIENTE =$datos['IDENTIFICACION_CLIENTE'];
	$TELEFONO_CLIENTE       =$datos['TELEFONO_CLIENTE'];
	$CELULAR_CLIENTE        =$datos['CELULAR_CLIENTE'];
	$DIRECCION_CLIENTE      =$datos['DIRECCION_CLIENTE'];
	
	$_SESSION['ID_CLIENTE']= $id_cli;
	$cliente			   = $datos['NOMBRE_CLIENTE'].' '.$datos['APELLIDO_CLIENTE'];
	$nombre 			   = $datos['NOMBRE_CLIENTE'];
	$apellido 			   = $datos['APELLIDO_CLIENTE'];
	$_SESSION['NOMBRE_CLIENTE']=$cliente;
	$FECHA_REGISTRO        = $datos["FECHA_REGISTRO"];
	$AUTOR_REGISTRO_FK     = $datos["AUTOR_REGISTRO_FK"];
}

$select = mysql_query("SELECT DISTRIBUIDOR_COMPRA,CLASIFICACION_ODONTOLOGO,TIPO_ODONTOLOGO,ESPECIALIDAD_ODONTOLOGO FROM 3m_encuesta WHERE ID_CLIENTE_FK_5='$id_cli'",$conex);
echo mysql_error($conex);
while($fila=(mysql_fetch_array($select)))
{
        $TIPO_ODONTOLOGO = $fila["TIPO_ODONTOLOGO"];
	$distribuidor=$fila['DISTRIBUIDOR_COMPRA'];
	$_SESSION['DISTRIBUIDOR_COMPRA']=$distribuidor;
	$CLASIFICACION_ODONTOLOGO=$fila['CLASIFICACION_ODONTOLOGO'];
	$ESPECIALIDAD_ODONTOLOGO=$fila['ESPECIALIDAD_ODONTOLOGO'];
}

//CONSULTAR FECHA DE LA ULTIMA GESTION 
$consultar_fecha_gestion = mysql_query("SELECT MAX(FECHA_GESTION)
FROM 3m_gestion
WHERE ID_CLIENTE_FK=".$id_cli,$conex);
	while($datos=(mysql_fetch_array($consultar_fecha_gestion))){
		$fecha_gestion = $datos["MAX(FECHA_GESTION)"];
	}
	
$fecha_actual = date("Y-m-d H:i:s");
$nuevafecha = strtotime ( '-1 hour' , strtotime ( $fecha_actual ) ) ;
$nuevafecha = date ( 'Y-m-d H:i:s' , $nuevafecha );
echo "esta es la fecha actual H:i:s ".$nuevafecha;
	
	$fecha_inicial= new DateTime($fecha_gestion);
	$fecha_final = new DateTime($fecha_actual);

	$resta_fecha = $fecha_inicial->diff($fecha_final);
	$dias= $resta_fecha->format("%d");
	$hora= $resta_fecha->format("%H");
	$minuto= $resta_fecha->format("%I");
	$segundo= $resta_fecha->format("%S");
	
	if($dias  > 1  ){
                                    $tiempo_transcurrido = "Hace ".$dias." dias";
                                }
                                else if($dias ==1){
                                    $tiempo_transcurrido="Ayer";
                                }
                                
                               else if($hora >0){
                                    $tiempo_transcurrido = "Hace ".$hora. "h ".$minuto."m";
                                }
                                else if($minuto >0){
                                    $tiempo_transcurrido = "Hace ".$minuto. " min";
                                }
                                else{
                                    $tiempo_transcurrido="Justo ahora";
                                }
		echo "tiempo ".$dias.' '.$hora.' '.$minuto.' '.$segundo; 
           
	

if(empty($ESPECIALIDAD_ODONTOLOGO)){
	//echo 'esta vacia es necesario ingresar encuesta';
	//realizar por primera vez la encuesta consumo
	$realizar_encuesta = 1;
}
else {
	// ya existe encuesta de consumo
	$realizar_encuesta = 0;
	}


if($id_usu!=''&&$id_usu!='0')
{
	$id_cli=$ID_CLIENTE;
	
?>
<script>
	obtener()
    </script>
 
<body>
<div id="dialog-message" title="Download complete">
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			
			<center>
			</center>
			</center>
			</span>
				
			<?php }else{?>
    
  
    
    
 <input name="distribuidor" id="distribuidor" value="<?php echo $distribuidor; ?>" type="hidden"/>
  <form id="tranferencias" name="tranferencias" action="../logica/guardar_encuesta_alert.php" method="post" style="width:100%;">
       <input id="LAT" name="LAT" type="hidden" />
        <input id="lon" name="lon"  type="hidden"/>
    <input name="idusuario" value="<?php echo $id_usu; ?>"   type="hidden"/>
    <input name="name_usuario" value="<?php echo $usua; ?>"  type="hidden" />
  	<input name="encuesta" id="encuesta" value="<?php echo $realizar_encuesta; ?>" type="hidden" />
	<input  name="nombre" value="<?php echo $nombre; ?>" type="hidden"/>
	<input  name="apellido" value="<?php echo $apellido; ?>" type="hidden"/>
   
	<div class="form-group" >
	<table align="right" style="width:100%;text-align:center;">
    	<tr> 	
          <th colspan="6"><center>Bienvenid@  <?php echo $usua?></center></th> 
          </tr>
          <tr>
            <input name="usua" style="display:none;" type="text" id="usua" value="<?php echo $usua; ?>" readonly/>      
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" >
        </a></td>
        <td><a href="../presentacion/consultar_gestiones_visitadores.php">
        <img src="../presentacion/imagenes/gestiones.png" width="56" height="57" id="cambiar" title="GESTIONES" >
         <center> <font style="font-size:10px;" >Gestiones</font></center>
        </a></td>
        
        <td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Pedidos</font></center>
        </a></td>
          <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PR&Oacute;XIMA VISITA" >
         <center> <font style="font-size:10px;" >Pr&oacute;xima <br />Visita</font></center>
        </a></td>
         <td><a href="../presentacion/reporte_clientes_nuevos.php">
        <img src="../presentacion/imagenes/excel.png" width="48" height="51" id="cambiar" title="REPORTE EXCEL" ><br />
       <center> <font style="font-size:10px;" >Clientes <br />Nuevos</font></center>
        </a></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
<center><img src="../presentacion/imagenes/3m.png" width="24%" height="17%" /></center>
	
    
    <br />
    <br />
	<fieldset style="border:1px solid #93271b; border-radius:10px;">
      <legend style="color:#93271b; font-weight:bold;">Informaci&oacute;n Cliente
        
        </legend>
        <table align="center" width="80%" class="table table-condensed">
            <tr style=" text-align: left;">
                <td class="tit">
                    Codigo Interno:
                </td>
                <td><?php
				if(empty($ID_CLIENTE)){
					$ID_CLIENTE=$_SESSION['ID_CLIENTE'];
					}
				   ?>
                    <input name="identifi" style="width:65%; height:25px;  border:none;" type="text" id="identifi" value="<?php echo $ID_CLIENTE; ?>" readonly="readonly"/>
                    
                    
                    <a href="consulta_pedidos_call.php"><img src="../presentacion/imagenes/pregunta.png" width="20%" height="30px" align="right" style="margin-right:5%;"/></a></td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit" style="width:35%;">
                    Nombre Cliente:
                </td>
                <td>
                    <input name="nombre_negocio" style="width:90%; height:25px; border:none" type="text" id="nombre_negocio" value="<?php echo $cliente; ?>" readonly="readonly"/>
               </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Numero de Identificacion:
                </td>
                <td>
                    <input name="identifi" style="width:90%; height:25px; border:none" type="text" id="identifi" value="<?php echo $IDENTIFICACION_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Telefono:
                </td>
                <td>
                    <input name="telefono" style="width:90%; height:25px; border:none" type="text" id="telefono" value="<?php echo $TELEFONO_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Celular:
                </td>
                <td>
                    <input name="celular" style="width:90%; height:25px; border:none" type="text" id="celular" value="<?php echo $CELULAR_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Direcci&oacute;n:
                </td>
                <td>
                    <input name="direccion" style="width:90%; height:25px; border:none" type="text" id="direccion" value="<?php echo $DIRECCION_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr> 
            <tr style=" text-align: left;">
                <td class="tit">
                    Distribuidor:
                </td>
                <td>
                    <input name="distri" type="text" id="distri" style="width:90%; height:25px; border:none" value="<?php echo $distribuidor; ?>" readonly/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">Clasificaci&oacute;n:</td>
                <td>
                <input name="clasificacion" type="text" id="clasificacion" style="width:90%; height:25px; border:none" value="<?php echo $CLASIFICACION_ODONTOLOGO; ?>" readonly/>
                </td>
            </tr>
          <tr style=" text-align: left;">
                <td class="tit">Especialidad:</td>
                <td>
                <input name="especialidad" type="text" id="especialidad" style="width:90%; height:25px; border:none" value="<?php echo $ESPECIALIDAD_ODONTOLOGO; ?>" readonly/>
                </td>
            </tr>
               <tr style=" text-align: left;">
                <td class="tit">Fecha de Registro:</td>
                <td>
                <input name="fecha_registro" type="text" id="fecha_registro" style="width:90%; height:25px; border:none" value="<?php echo $FECHA_REGISTRO; ?>" readonly/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">Usuario quien registra:</td>
                <td>
                <input name="usuario_registro" type="text" id="usuario_registro" style="width:90%; height:25px; border:none" value="<?php echo $AUTOR_REGISTRO_FK; ?>" readonly/>
                </td>
            </tr>
			<tr>
			<td class="tit">&Uacute;ltima visita</td>
			<td>
			   <input name="dias_visita" type="text" id="dias_visita" style="width:90%; height:25px; border:none" value="<?php echo $tiempo_transcurrido; ?>" readonly/>
             </td>
			</tr>
            <tr style="text-align: center">
                <td colspan="2">
                <a href="form_cliente_actualizar.php"><img src="imagenes/lapiz.png" width="40" height="40" title="Editar Cliente"/></a>
                </td>
          </tr>                        
      </table>
  </fieldset>
    <br />
    <br />

    <div class="form-group" style="display:none" >
   
        <input name="id_cliente" id="id_cliente" type="text"  readonly="readonly"value="<?php echo $id_cli ?>"/>
        <input name="distribuidor" id="distribuidor" type="text" readonly="readonly" class="form-control1" value="<?php echo $distribuidor ?>"/>
        <input type="text" id="compro" name="compro" readonly="readonly"/>
    </div>
           
    <?php 
	 if($transferencia == 1){
	?> 
    
	<iframe style=" width:100%;border:1px solid transparent; height:900px;" src="../presentacion/form_transferencias_med.php" id="pag" name="pag"></iframe>
        <?php }  ?>
    
  <?php if($transferencia !=1){ ?>  
    <div class="form-group" id="resultado_div" style="display:">
    	<br />
         <div class="col-md-3">
    	<label for="Resultado">Resultado Visita:</label><span class="asterisco">*</span>
        </div>
         <div class="col-md-3">
        <select name="resultado_text" id="resultado_text" class="form-control" required="true" >
        	<option selected="selected" disabled="disabled">Elija...</option>
           
            <option>EFECTIVA</option>
            <option>NO EFECTIVA</option>
            <option>TELEVENTA</option>
            <option>VENTA EN EVENTO</option>
        </select>
    	</div>
      </div>
   <?php  } ?>     
    </div>
    
     <!-- Visita no efectiva subtipificacion-->
	<div id="visita_no_efectiva"  class="visita_no_efectiva" style="display:none">
    <div class="tipificacion_no_efectiva" style="display:none;">
         <div class="col-md-3">
            <label for="Resultado">Sub tipificacion:</label><span class="asterisco">*</span>
            </div>
         <div class="col-md-3">
        <select name="sub_tipificacion_no_efectiva" id="sub_tipificacion" class="form-control no_efectiva_subtipificacion">
                <option selected="selected" disabled="disabled" value="">Elija...</option>
                <option>CONSULTORIO CERRADO</option>
                <option>DIRECCION ERRADA</option>
                <option>DIRECCION NO EXISTE</option>
                <option>NO EXISTE CONSULTORIO</option>
                <option>MEDICO RETIRADO</option>
                <option>ODONTOLOGO NO ATIENDE </option>
                <option>ODONTOLOGO NO ESTA</option>
        </select>
        </div>
        </div>
      </div>
      
       <!-- Visita efectiva subtipificacion-->
    <div class="visita_efectiva" style="display:none">
         <div class="col-md-3">
        <label for="Resultado">Sub tipificacion:</label><span class="asterisco">*</span>
        </div>
         <div class="col-md-3">
            <select name="sub_tipificacion" id="sub_tipificacion" class="form-control sub_tipificacion_efectiva">
                    <option selected="selected" disabled="disabled">Elija...</option>
                    <option>ENCUESTA DE CONSUMO</option>
                    <option>ENCUESTA DE CONSUMO Y TRANSFERENCIA</option>
                    <option>DEMOSTRACION</option>
                    <option>LANZAMIENTO DE PRODUCTO NUEVO</option>
                    <option>VENTA DE MANTENIMIENTO</option>
					<option>SEGUIMIENTO</option>
            </select>
          </div>  
      </div>
      <!-- fecha proxima visita -->
      <div class="fecha_proxima_visita" style="display:none" id="fecha_proxima_visita">
      <div class="col-md-3">
        <label class="proxima_visita">Fecha Pr&oacute;xima Visita</label>
        </div>
        <div class="col-md-3">
        <input type="date" name="proxima_visita" id="proxima_visita" class="form-control" step="1" required="true"  min="<?php echo date("Y-m-d");?>">
        </div>
      </div>
      
      
      
      <div  class="visita_observaciones" style="display:none">
      <div class="col-md-3">
        <label for="Resultado">Observaciones:</label><span class="asterisco">*</span>
        </div>
        <div class="col-md-3">
        <textarea class="form-control" style="width:98%" rows="4" name="observacion" id="observacion"></textarea>
      </div>
	</div>
    
      <div style="display:none"  class="form-group" id="boton_no_efectiva">
   		 <div class="col-md-12">
   		 <fieldset>
    	<legend>EVIDENCIA</legend>
       
            <label for="cliente">Imagen Fachada</label><span class="asterisco">*</span>
           
            <input class="form-control" name="fachada" id="fachada" type="file" data-preview-file-type="any" multiple=true/>
   			 <div >
            <center>
                <input formaction='../logica/guardar_encuesta.php' type="submit" name="GUARDAR" id="GUARDAR" class="form-control btn" value="GUARDAR" title="Guardar No Efectiva" />
            </center>
		</div>
   		</fieldset>
   		 </div>
     </div> 
     
     <div style="display:none" id="televenta" class="form-group">
   		 <div class="col-md-12">
   	
            <center>
                <input type="submit" name="BTN_TELEVENTA" id="BTN_TELEVENTA" class="form-control btn" value="GUARDAR" title="Guardar Televenta" />
            </center>
			</div>
   	  </div>
     </div> 
     <div style="display:none" id="boton_evento" class="form-group">
   		 <div class="col-md-12">
   	
            <center>
                <input type="submit" name="BTN_EVENTO" id="BTN_EVENTO" class="form-control btn" value="GUARDAR" title="Guardar venta en evento" />
            </center>
			</div>
   	  </div>
     </div> 
     
     <div style="display:none" id="efectiva_sin_consumo" class="form-group">
   		 <div class="col-md-12">
   	
            <center>
                <input type="submit" name="BTN_EFECTIVA_SIN_ENCUESTA" id="BTN_EFECTIVA_SIN_ENCUESTA" class="form-control btn" value="GUARDAR" title="Guardar Efectiva"/>
            </center>
			</div>
   	  </div>
     </div> 
     
     
     <div class="col-md-12" id="encuesta_consumo" style="display:none">
     <div>
     
      <?php
	   include '../presentacion/form_encuesta_consumo.php';
	 
	   ?>
       
       <div class="form-group">
            <center>
            	<input name="confirmar" type="submit" id="confirmar"  class="btn btn-primary" value="Actualizar" onclick="return validar(tranferencias)" style="height:40px" title="Guardar Efectiva con Encuesta de Consumo" />
            </center>
    </div>
       </div>
    </div>
    
</form>
<?php }
		?>           
  
</body>
</html>
<?php
}
else
{
	?>
		<script type="text/javascript">
		/*	window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";*/
		</script>
	<?php
}
?>
