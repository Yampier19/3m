<?php
	include("../logica/session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/tablas.css" rel="stylesheet" />
<link href="../presentacion/css/tablas.css" rel="stylesheet" />
<link href="css/bootstrap.css" rel="stylesheet" />
<title>Detalle pedido</title>
<link rel="shortcut icon" href="imagenes/3m.png" />
<link rel="shortcut icon" href="../presentacion/imagenes/3m.png" />

<script src="js/jquery.js"></script>
<script src="../presentacion/js/jquery.js"></script>
<script>
$(document).ready(function()
{
	var estado_producto_act=$('#estado_producto_act').val();
	if(estado_producto_act=='ENTREGA PARCIAL')
	{
		$('#esta_titu').css('display','block');
		
		$('#esta_titu').css('border','#403f3d');
		
		var cantidad=$('#cantidad').val();
		for(i=1;i<=cantidad;i++)
		{
			$('#esta'+i).css('display','block');
			$("#estado_producto"+i+ "option:eq(0)").attr("selected", "selected");
		}
	}
});

</script>
<style>
td
{
	padding:10px;
}

.div
{
	width:97%;
	padding-right:8px;
	padding-left:8px;
	/*border:#F00 1px solid;*/
}
input[type=submit]
{
	height:30px;
	padding:5px;
	width:30%;
	border-radius:5px;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
.btn:focus,
.btn.focus {
  color: #fff;
  background-color: #286090;
  border-color: #122b40;
}
.btn:hover {
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active,
.btn.active,
.open > .dropdown-toggle.btn{
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active:hover,
.btn.active:hover,
.open > .dropdown-toggle.btn:hover,
.btn:active:focus,
.btn.active:focus,
.open > .dropdown-toggle.btn:focus,
.btn:active.focus,
.btn.active.focus,
.open > .dropdown-toggle.btn.focus {
  color: #fff;
  background-color: #204d74;
  border-color: #122b40;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
</style>
</head>
<?PHP
include('../datos/conex.php');

?>
<body>
<form action="../logica/cambio_estado_pedido.php" method="post">

<table style="width:99%; margin:auto auto;" rules="none" class="table table-striped" >
	<tr>
		<th colspan='6' class="principal">INFORMACI&Oacute;N PROMOCI&Oacute;N</th>
	</tr>
    <?PHP
    while($dato=mysql_fetch_array($CONSULTA_CATEGORIA))
	{
		
	?>
    
		<tr>
        	<th>
            	CATEGORIA               
            </th>
            <td >PROMOCI&Oacute;N</td>
           </tr>
           <tr> 
            <th>SUB CATEGORIA </th>
            <td>PROMOCI&Oacute;N</td>
          </tr>
          <tr> 
            <th>STOCK</th>
            <td><input name="stock" type="text"  required="required" class="form-control"/></td>
       </tr>
       <tr>
       		<th>DESCRIPCI&Oacute;N</th>
            <td><input name="descripcion" type="text"  required="required" class="form-control"/></td>
            </tr>
            <tr>
            <th>PRECIO</th>
            <td><input name="precio" type="number"  required="required" class="form-control"/></td>
          </tr>
          <tr>  
            <th>ESTADO</th>
            <td>
        
			<select class="form-control" name="estado" required="true">
                <option value="0">Inactivo</option>
				<option value="1">Activo</option> 
			
            
            </select>
			</td>
       </tr> 
 
     <?PHP } ?> 
</table>
<br /><br />
<center>

<button class="btn btn-group-lg" name="crear_promocion">Crear Promoci&oacute;n</button>
</center>
<br />

<br />
   
</form>
</body>
</html>