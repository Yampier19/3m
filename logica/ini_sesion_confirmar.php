<?php
session_start();


//cambiamos la duración a la cookie de la sesión 
session_set_cookie_params(0, "/", $HTTP_SERVER_VARS["HTTP_HOST"], 0); 

    $fechaGuardada = $_SESSION["ultimoAcceso"]; 
    $ahora = date("Y-m-d H:i:s"); 
    $tiempo_transcurrido = $ahora-$fechaGuardada; 

    //comparamos el tiempo transcurrido 
     if($tiempo_transcurrido >= 900) { 
     //900 si pasaron 30 minutos o más 
      session_destroy(); // destruyo la sesión 
      header('Location: ../index.php');
	  //envío al usuario a la pag. de autenticación 
      //sino, actualizo la fecha de la sesión 
    }else { 
    $_SESSION["ultimoAcceso"] = $ahora; 
   } 
 echo 'fecha guardada '. $fechaGuardada.' ahora: '.$ahora;
?>