<?php
include("session.php");
?>
<style>
    .aviso3 
    {
        font-size: 130%;
        font-weight: bold;
        color: #11a9e3;
        text-transform:uppercase;
        /*font-family: "Trebuchet MS";
        font-family:"Gill Sans MT";
        border-radius:10px;
        background: #11a9e3;*/
        background-color:transparent;
        text-align: center;
        padding:10px;
    }
    .error
    {
        font-size: 130%;
        font-weight: bold;
        color: #fb8305;
        text-transform:uppercase;
        background-color:transparent;
        text-align: center;
        padding:10px;
    }
</style>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Confirmar pedido</title>
    </head>
    <body>
        <br />
        <br />

        <?php
        header("Content-Type: text/html;charset=utf-8");
        include('../datos/conex_copia.php');
        $string_intro = getenv("QUERY_STRING");
        parse_str($string_intro);
        if (isset($_POST['confirmar'])) {
            $latitud = $_POST['LAT1'];
            $longitud = $_POST['lon1'];
            $ID_CLIENTE = $ID_CLIENTE;
            $id_usuario = $id_usu;
            $DISTRIBUIDOR = $_POST['distribuidor'];
            $total_pedido = $_POST['total_pedido'];
            $FORMA_PAGO = $_POST['forma_pago'];
            $tipo_pedido = $_POST['tipo_pedido'];
            $datafono = $_POST['datafono'];
            $rango = $_POST['rango'];
            $observaciones = utf8_decode($_POST['observaciones']);

            $id_cliente_campo = $_POST['id_cliente'];
            $resultado = "INGRESO SOLICITUD PEDIDO";


            $fecha_actual = date("Y-m-d");

            $UPDATE_DISTRIBUIDOR = mysqli_query($conex, "UPDATE 3m_encuesta
			SET DISTRIBUIDOR_COMPRA='" . $DISTRIBUIDOR . "' 
			WHERE ID_CLIENTE_FK_5='" . $ID_CLIENTE . "'");

            $CONSULTAR_ULTIMA_GESTION = mysqli_query($conex, "SELECT ID_GESTION,TIPIFICACION_GESTION,SUB_TIPIFICACION 
		FROM 3m_gestion 
		WHERE ID_CLIENTE_FK =" . $id_cliente_campo . " AND DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ACCION='REGISTRO VISITA'  AND ID_ASESOR_GESTION=" . $id_usuario . "  ORDER BY FECHA_GESTION DESC LIMIT 1;");

            $dato_subtipificacion = mysqli_fetch_array($CONSULTAR_ULTIMA_GESTION);
            $SUB_TIPIFICACION = $dato_subtipificacion["SUB_TIPIFICACION"];
            $id_gestion = $dato_subtipificacion["ID_GESTION"];
            $TIPIFICACION_GESTION = $dato_subtipificacion["TIPIFICACION_GESTION"];


            //REALIZAR VALIDACIO SI CORREO ES 0
            $CONSULTA_CORREO = mysqli_query($conex, "SELECT CORREO FROM 3m_gestion
		WHERE ID_GESTION='" . $id_gestion . "'");

            $dato_de_corre = mysqli_fetch_array($CONSULTA_CORREO);
            $CORREO = $dato_de_corre["CORREO"];

            if ($CORREO == 0) {

                $INGRESAR_PEDIDO = mysqli_query($conex, "INSERT INTO 3m_pedido (TOTAL_PEDIDO,FORMA_PAGO,DISTRIBUIR,FECHA_PEDIDO,ID_CLIENTE_FK,ID_USUARIO_FK,TIPO_PEDIDO ,DATAFONO)
VALUES('$total_pedido','$FORMA_PAGO','$DISTRIBUIDOR',NOW(),'$ID_CLIENTE','$id_usuario','$tipo_pedido' ,'$datafono')");
                echo mysqli_error($conex);

                //select con id del pedido ingresado limit 1


                if ($TIPIFICACION_GESTION != 'VENTA EN EVENTO' && $TIPIFICACION_GESTION != 'TELEVENTA') {
                    $TIPIFICACION_GESTION = 'EFECTIVA';

                    if (empty($SUB_TIPIFICACION)) {
                        $SUB_TIPIFICACION = 'ENCUESTA DE CONSUMO Y TRANSFERENCIA';
                    } else if ($SUB_TIPIFICACION == '') {
                        $SUB_TIPIFICACION = 'ENCUESTA DE CONSUMO Y TRANSFERENCIA';
                    }
                } else if ($TIPIFICACION_GESTION == 'TELEVENTA') {
                    $TIPIFICACION_GESTION = 'TELEVENTA';
                    $SUB_TIPIFICACION = $SUB_TIPIFICACION;
                } else {
                    $TIPIFICACION_GESTION = 'VENTA EN EVENTO';
                    $SUB_TIPIFICACION = 'VENTA EN EVENTO';
                }



                if ($INGRESAR_PEDIDO) {
                    $fecha_actual = date("Y-m-d");

                    $ACTUALIZAR_REGISTRO_GESTION = mysqli_query($conex, "
			UPDATE 3m_gestion SET  ACCION ='INGRESO SOLICITUD PEDIDO' ,TIPIFICACION_GESTION='" . $TIPIFICACION_GESTION . "',SUB_TIPIFICACION='" . $SUB_TIPIFICACION . "' ,  ASESOR_GESTION='$usua', ID_ASESOR_GESTION='$id_usuario',FECHA_GESTION=NOW()
			WHERE ID_GESTION='$id_gestion'
			");
                    echo mysqli_error($conex);

                    $consulta_id_pedido = mysqli_query($conex, "SELECT ID_PEDIDO FROM 3m_pedido ORDER BY ID_PEDIDO DESC LIMIT 1 ");
                    while ($dato = mysqli_fetch_array($consulta_id_pedido)) {
                        $ID_PEDIDO = $dato['ID_PEDIDO'];
                    }


                    $CONSULTAR_ID_GESTION = mysqli_query($conex, "SELECT ID_GESTION FROM 3m_gestion WHERE ID_CLIENTE_FK = '$ID_CLIENTE' ORDER BY FECHA_GESTION DESC LIMIT 1");

                    while ($dato_gestion = mysqli_fetch_array($CONSULTAR_ID_GESTION)) {
                        $ID_GESTION = $dato_gestion['ID_GESTION'];
                    }

                    $CONSULTAR_CONSOLIDADO_PREVIO = mysqli_query($conex, "SELECT ID_CONSOLIDADO FROM 3m_consolidado_pedido  WHERE ID_GESTION ='$ID_GESTION' AND ID_CLIENTE='$ID_CLIENTE'");

                    $NUMERO_REGISTRO_CONSOLIDADO = mysqli_num_rows($CONSULTAR_CONSOLIDADO_PREVIO);

                    if ($NUMERO_REGISTRO_CONSOLIDADO > 0) {

                        while ($dato_consolidado = mysqli_fetch_array($CONSULTAR_CONSOLIDADO_PREVIO)) {
                            $ID_CONSOLIDADO = $dato_consolidado['ID_CONSOLIDADO'];
                        }
                        $ACTUALIZAR_REGISTRO_CONSOLIDADO = mysqli_query($conex, "UPDATE 3m_consolidado_pedido SET ID_PEDIDO= '$ID_PEDIDO' WHERE ID_CONSOLIDADO= '$ID_CONSOLIDADO'");
                    } else {

                        $INSERT_CONSOLIDADO = mysqli_query($conex, "INSERT INTO 3m_consolidado_pedido (ID_GESTION,ID_PEDIDO,ID_CLIENTE) VALUES('$ID_GESTION','$ID_PEDIDO','$ID_CLIENTE');");
                        echo mysqli_error($conex);
                    }


                    $consulta = mysqli_query($conex, "SELECT * FROM 3m_detalle_pedido_temp AS dpt
            INNER JOIN 3m_categoria AS c ON dpt.ID_CATEGORIA_FK_3=c.STOCK
            WHERE ID_CLIENTE_FK_3='$ID_CLIENTE' AND ID_USUARIO_FK_3='$id_usuario'");
                    echo mysqli_error($conex);
                    $nreg_deta = mysqli_num_rows($consulta);
                    while ($dato = mysqli_fetch_array($consulta)) {
                        $ID = $dato['ID_PEDIDO'];
                        $cantidad = $dato['CANTIDAD_PRODUCTO'];
                        $DESCRIPCION = $dato['DESCRIPCION'];
                        $ID_CATEGORIA = $dato['ID_CATEGORIA_FK_3'];
                        $TOTAL_PEDIDO = $dato['TOTAL_PEDIDO'];

                        $ID_CLIENTE_FK_3 = $dato['ID_CLIENTE_FK_3'];


                        $ingresar = mysqli_query($conex, "INSERT INTO 3m_detalle_pedido 
(ID_PEDIDO_FK_2,CANTIDAD_PRODUCTO,TOTAL_PEDIDO,DISTRIBUIR,ID_CATEGORIA_FK_2,ID_CLIENTE_FK_2,ID_USUARIO_FK_2,TIPO_PEDIDO,RANGO_ENTREGA,OBSERVACIONES)
	VALUES('$ID_PEDIDO','$cantidad','$TOTAL_PEDIDO','$DISTRIBUIDOR','$ID_CATEGORIA','$ID_CLIENTE_FK_3','$id_usuario','$tipo_pedido','$rango','$observaciones')");
                        echo mysqli_error($conex);

                        $eliminar = mysqli_query($conex, "DELETE FROM 3m_detalle_pedido_temp WHERE ID_PEDIDO='$ID'");
                        echo mysqli_error($conex);
                    }



                    //CONVERSION APLICADA A SOLO A TRANSFERENCIAS


                    if ($tipo_pedido == 'Transferencia' || $tipo_pedido == 'Transferencia Compartida') {


                        //CONSULTAR_TABLA_CONVERSION 
                        $CONVERSION_TABLA = mysqli_query($conex, "SELECT ID_CONVERSION, CONVERSION FROM 3m_conversion");

                        while ($datos_posible_conversion = mysqli_fetch_array($CONVERSION_TABLA)) {


                            //CONSULTAR PRODUCTOS PEDIDO TRANSFERENCIA CON POSIBLE CONVERSION
                            $CONSULTAR_PRODUCTOS_TRANSFERENCIA = mysqli_query($conex, "SELECT B.DESCRIPCION
					FROM 3m_detalle_pedido AS A
					INNER JOIN 3m_categoria AS B ON B.STOCK = A.ID_CATEGORIA_FK_2
					WHERE ID_PEDIDO_FK_2=" . $ID_PEDIDO . " AND B.DESCRIPCION LIKE '%" . $datos_posible_conversion["CONVERSION"] . "%' ");


                            while ($datos_transferencia_conversion = mysqli_fetch_array($CONSULTAR_PRODUCTOS_TRANSFERENCIA)) {
                                //CONSULTA LOS PRODUCTOS DE CONSUMO



                                $CONSULTAR_PRODUCTOS_CONSUMO = mysqli_query($conex, "SELECT A.NOMBRE_PRODUCTO, B.ID_CLIENTE_FK
							FROM 3m_detalle_producto_consumo  AS A
							INNER JOIN 3m_productos_consumo_historial AS B ON B.ID_PRODUCTOS_CONSUMO = A.ID_PRODUCTOS_CONSUMO_FK
							WHERE ID_CLIENTE_FK=" . $ID_CLIENTE . ";");

                                while ($datos_productos_consumo = mysqli_fetch_array($CONSULTAR_PRODUCTOS_CONSUMO)) {

                                    //CONSULTAR SI EXISTE CONVERSION


                                    $CONSULTAR_CONVERSION_REALIZADA = mysqli_query($conex, "SELECT ID_CONVERSION 
									FROM 3m_conversion 
									WHERE BASE_CONVERSION='" . $datos_productos_consumo["NOMBRE_PRODUCTO"] . "'  AND CONVERSION='" . $datos_posible_conversion["CONVERSION"] . "'");
                                    while ($datos_conversion = mysqli_fetch_array($CONSULTAR_CONVERSION_REALIZADA)) {

                                        //CONSULTAR SI EXISTE REGISTRE PREVIO



                                        $CONVERSION_PREVIO = mysqli_query($conex, "SELECT COUNT(*) FROM 3m_consolidado_conversion 
										WHERE ID_CONVERSION =" . $datos_conversion["ID_CONVERSION"] . "  AND ID_PEDIDO=" . $ID_PEDIDO . " AND ID_CLIENTE=" . $ID_CLIENTE . " ;");

                                        $REGISTROS = mysqli_fetch_array($CONVERSION_PREVIO);
                                        $NUMERO_REGISTRO_PREVIO = $REGISTROS["COUNT(*)"];


                                        if ($NUMERO_REGISTRO_PREVIO == 0) {

                                            //INGRESAR CONSOLIDADO DE CONVERSION
                                            $INGRESAR_CONSOLIDADA_CONVERSION = mysqli_query($conex, "INSERT INTO 3m_consolidado_conversion 
										(`ID_CONVERSION`, `ID_PEDIDO`, `ID_CLIENTE`,VALOR_CONVERSION)
										VALUES(" . $datos_conversion["ID_CONVERSION"] . ", " . $ID_PEDIDO . "," . $ID_CLIENTE . "," . $TOTAL_PEDIDO . ");");
                                        }
                                    }
                                }
                            }
                        }

                        //ID ENCUESTA

                        $CONSULTA_ENCUESTA = mysqli_query($conex, "SELECT ID_ENCUESTA,TIPO_ODONTOLOGO,CASOS_NUEVOS_SEMANA,CANTIDAD_PACIENTES  FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $ID_CLIENTE);
                        $DATOS_ENCUESTA = mysqli_fetch_array($CONSULTA_ENCUESTA);

                        $ID_ENCUESTA = $DATOS_ENCUESTA["ID_ENCUESTA"];
                        $TIPO_ODONTOLOGO = $DATOS_ENCUESTA["TIPO_ODONTOLOGO"];
                        $CASOS_NUEVOS_SEMANA = $DATOS_ENCUESTA["CASOS_NUEVOS_SEMANA"];
                        $CANTIDAD_PACIENTES = $DATOS_ENCUESTA["CANTIDAD_PACIENTES"];
                        //INGRESAR PRODUCTOS A HISTORIAL DE CONSUMO

                        $INGRESAR_CONSUMO_HISTORIAL = mysqli_query($conex, "INSERT INTO 3m_productos_consumo_historial (ID_CLIENTE_FK,ID_ENCUESTA_FK,FECHA_MODIFICACION,ID_USUARIO_FK,TIPO_INGRESO)
						VALUES('$ID_CLIENTE','$ID_ENCUESTA',NOW(),'$id_usuario','TRANSFERENCIA')");

                        $consulta_id_historial = mysqli_query($conex, "SELECT ID_PRODUCTOS_CONSUMO FROM 3m_productos_consumo_historial ORDER BY ID_PRODUCTOS_CONSUMO DESC LIMIT 1 ");
                        while ($dato = mysqli_fetch_array($consulta_id_historial)) {
                            $ID_PRODUCTOS_CONSUMO = $dato['ID_PRODUCTOS_CONSUMO'];
                        }

                        //INGRESA PRODUCTOS DE DETALLE A HISTORIAL
                        //CONSULTAR PRODUCTOS DEL PEDIDO 
                        $consulta_productos = mysqli_query($conex, "SELECT B.CATEGORIA,B.SUBCATEGORIA,B.DESCRIPCION,B.TIPO_PRODUCTO
							FROM 3m_detalle_pedido AS A
							INNER JOIN  3m_categoria AS B ON B.STOCK = A.ID_CATEGORIA_FK_2
							WHERE A.ID_PEDIDO_FK_2=" . $ID_PEDIDO);
                        echo mysqli_error($conex);

                        while ($dato_productos = mysqli_fetch_array($consulta_productos)) {
                            $ingresar = mysqli_query($conex, "INSERT INTO 3m_detalle_producto_consumo (CATEGORIA,SUBCATEGORIA,NOMBRE_PRODUCTO,ID_PRODUCTOS_CONSUMO_FK,TIPO)
						 VALUES('" . $dato_productos["CATEGORIA"] . "','" . $dato_productos["SUBCATEGORIA"] . "','" . $dato_productos["DESCRIPCION"] . "','$ID_PRODUCTOS_CONSUMO','" . $dato_productos["TIPO_PRODUCTO"] . "')");
                        }

                        //ACTUALIZAR CALIFICACION

                        if ($TIPO_ODONTOLOGO == 1) {
                            $select_categoria_dental = mysqli_query($conex, "SELECT h.ID_CLIENTE_FK,h.ID_ENCUESTA_FK,p.CATEGORIA,p.SUBCATEGORIA,p.NOMBRE_PRODUCTO,
                                                    h.FECHA_MODIFICACION,h.ID_USUARIO_FK 
                                                    FROM 3m_productos_consumo_historial AS h
                                                    INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
                                                    WHERE ID_CLIENTE_FK=" . $ID_CLIENTE . " AND TIPO=1 AND p.SUBCATEGORIA IN ('Resinas','Adhesivos','CEMENTO','Terminado y pulido') 
                                                    GROUP BY SUBCATEGORIA");

                            echo mysqli_error($conex);
                            $nreg_deta = mysqli_num_rows($select_categoria_dental);

                            if ($nreg_deta >= 4 && $CANTIDAD_PACIENTES >= 30) {
                               
                                $clasificacion_odontologo = "A";
                            }
                            else if ($nreg_deta < 4 && $CANTIDAD_PACIENTES >= 30) {
                                $clasificacion_odontologo = "A1G";
                            }
                            else if ($nreg_deta >= 4 && $CANTIDAD_PACIENTES < 30) {
                                $clasificacion_odontologo = "B";
                            }
                            else if ($nreg_deta < 4 && $CANTIDAD_PACIENTES < 30) {
                                $clasificacion_odontologo = "C";
                            } 
                            else {
                                $clasificacion_odontologo = 'C';
                            }
                        } else if ($TIPO_ODONTOLOGO == 2) {
                            $CONSULTAR_CATEGORIA_UNO = mysqli_query($conex, "SELECT p.SUBCATEGORIA
                                                    FROM 3m_productos_consumo_historial AS h
                                                    INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
                                                    WHERE ID_CLIENTE_FK=" . $ID_CLIENTE . " and TIPO=2 AND  SUBCATEGORIA in('Adhesivos','Resinas')");
                            $conteo_categoria_uno = mysqli_num_rows($CONSULTAR_CATEGORIA_UNO);

                            $CONSULTAR_CATEGORIA_DOS = mysqli_query($conex, "SELECT p.SUBCATEGORIA
                                                    FROM 3m_productos_consumo_historial AS h
                                                    INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
                                                    WHERE ID_CLIENTE_FK=" . $ID_CLIENTE . " and TIPO=2 AND  SUBCATEGORIA in('Brackets metalicos convencionales')");
                            $conteo_categoria_dos = mysqli_num_rows($CONSULTAR_CATEGORIA_DOS);

                            $CONSULTAR_CATEGORIA_TRES = mysqli_query($conex, "SELECT p.SUBCATEGORIA
                                                    FROM 3m_productos_consumo_historial AS h
                                                    INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
                                                    WHERE ID_CLIENTE_FK=" . $ID_CLIENTE . " and TIPO=2 AND  SUBCATEGORIA in('Brackets ceramicos convencionales','Brackets ceramicos de autoligado')");
                            $conteo_categoria_tres = mysqli_num_rows($CONSULTAR_CATEGORIA_TRES);


                            $conteo_categoria = $conteo_categoria_uno + $conteo_categoria_dos + $conteo_categoria_tres;
                           
                            //categoria A.
                            if ($CANTIDAD_PACIENTES >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria >= 3) {
                                $clasificacion_odontologo = 'A';
                            }
                            //calificacion A1G
                            else if ($CANTIDAD_PACIENTES >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria < 3) {
                                $clasificacion_odontologo = 'A1G';
                            }
                            //calificacion B
                            else if ($CANTIDAD_PACIENTES < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria >= 3) {
                                $clasificacion_odontologo = 'B';
                            }
                            //calificacion C
                            else if ($CANTIDAD_PACIENTES < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria < 3) {
                                $clasificacion_odontologo = 'C';
                            } 
                            else {
                                $clasificacion_odontologo = 'C';
                            }
                        }
                        $CONSULTAR_CALIFICACION = mysqli_query($conex, "SELECT CLASIFICACION_ODONTOLOGO FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $ID_CLIENTE);

                        while ($dato_calificacion = mysqli_fetch_array($CONSULTAR_CALIFICACION)) {
                            $CLASIFICAION_ANTERIOR = $dato_calificacion['CLASIFICACION_ODONTOLOGO'];
                        }

                        

                        $consultar_historia_calificacion = mysqli_query($conex, "SELECT * FROM 3m_historia_clasificacion WHERE ID_CLIENTE_FK_7=" . $ID_CLIENTE);

                        $historia = mysqli_num_rows($consultar_historia_calificacion);

                        if ($historia == 0) {
                            
                            $INSERT_HISTORIA = mysqli_query($conex, "INSERT INTO 3m_historia_clasificacion(CLASIFICAION_ANTERIOR,CLASIFICAION_ACTUAL,USUARIO_REGISTRO_ID,
                                USUARIO_REGISTRO,ID_CLIENTE_FK_7)
                                VALUES ('" . $CLASIFICAION_ANTERIOR . "','" . $clasificacion_odontologo . "','" . $id_usuario . "','" . $usua . "','" . $ID_CLIENTE . "')");
                        } else {
                            
                            //actualizar califacion
                            $ACTUALIZAR_CALIFICACION = mysqli_query($conex, "
                                        UPDATE 3m_historia_clasificacion SET CLASIFICAION_ACTUAL='" . $clasificacion_odontologo . "' , CLASIFICAION_ANTERIOR='" . $CLASIFICAION_ANTERIOR . "' WHERE ID_CLIENTE_FK_7=" . $ID_CLIENTE);
                        }

                        // actualizar clasificacion del odontologo u ortodoncista en encuesta

                        $actualizar = mysqli_query($conex, "UPDATE 3m_encuesta SET
                                        CLASIFICAION_ODONTOLOGO_ANTERIOR='$CLASIFICAION_ANTERIOR',
                                        CLASIFICACION_ODONTOLOGO='$clasificacion_odontologo'
                                        WHERE
                                        ID_CLIENTE_FK_5='$ID_CLIENTE'");
                    }



                    //consultar si es una prueba...
                    $CONSULTAR_PRUEBA = mysqli_query($conex, "SELECT ID_PEDIDO FROM 3m_pedido WHERE ID_PEDIDO =" . $ID_PEDIDO . " AND ID_USUARIO_FK=2");
                    $datos_prueba = mysqli_fetch_array($CONSULTAR_PRUEBA);

                    $PRUEBA = $datos_prueba["ID_PEDIDO"];

                    //if(empty($PRUEBA)){ 
                    //validar si el id de pedido tiene detalles del pedido es decir: consulta de la tabla detalle pedido con id del pedido 
                    //si esta vacio no envie correo y elimine el pedido  
                    //se esta vacia la consulta no envie correo y no muestre mensaje

                    $CONSULTA_ID_PEDIDO = mysqli_query($conex, "SELECT * FROM 3m_detalle_pedido WHERE ID_PEDIDO_FK_2='" . $ID_PEDIDO . "'");
                    $conteo_de_datos = mysqli_num_rows($CONSULTA_ID_PEDIDO);
                    if ($conteo_de_datos == 0) {
                        $eliminar_pedido = mysqli_query($conex, "DELETE FROM 3m_pedido WHERE ID_PEDIDO='" . $ID_PEDIDO . "'");
                    } else {
                        if ($tipo_pedido == 'Transferencia Compartida') {
                            echo "es una transferencia compartida";
                            include('../presentacion/email/email_transferencia_compartida.php');
                        }
			else if($tipo_pedido=='Sugerido'){
				 include('../presentacion/email/mail_pedido_sugerido.php');
						}
                            else {
                                    if($DISTRIBUIDOR=='Angie Hernandez-Asedis LTDA' || $DISTRIBUIDOR=='Sandra Patricia Lopez-Casa Dental Sede Sur' || $DISTRIBUIDOR=='Angie Franco-Casa Dental Sede Norte'){
                                            include('../presentacion/email/mail_pedido_respaldo.php');
                                    }else{
							
                            include('../presentacion/email/mail_pedido_mail.php');
							}
                            
                        }
                  
						
						  
                    }
                    //}else{
                    //echo "ACCESO CON USUARIO PRUEBA NO SE ENVIA CORREO";
                    //}
                    // UPDATE CORREO CAMBIA DE 0 A 1
                    $ACTUALIZAR_CORREO = mysqli_query($conex, "UPDATE 3m_gestion SET CORREO='1'
					WHERE ID_GESTION= '" . $id_gestion . "'");
                    ?> 
                    <span style="margin-top:5%;">
                        <center>
                            <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/> 
                            <p class="aviso3" style=" width:68.9%; margin:auto auto;">
                                EL PEDIDO HA SIDO INGRESADO SATISFACTORIAMENTE.
                            </p>
                        </center>
                        <br/>
                    <?php
                }
                
                    
                } else {
                    ?>
                    <span style="margin-top:5%;">
                        <center>
                            <img src="imagenes/advertencia.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                        </center>
                    </span>
                    <p class="error" style=" width:68.9%; margin:auto auto;">
                        EL PEDIDO YA HA SIDO GUARDADO.
                    </p>           
        <?php
    }
}
?>	
    </body>
</html>